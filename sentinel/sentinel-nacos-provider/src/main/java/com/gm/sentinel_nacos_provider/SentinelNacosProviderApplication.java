package com.gm.sentinel_nacos_provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication
@EnableDiscoveryClient
public class SentinelNacosProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SentinelNacosProviderApplication.class, args);
    }

}
