package com.gm.sentineldemo;

import com.alibaba.csp.sentinel.Entry;
        import com.alibaba.csp.sentinel.SphU;
        import com.alibaba.csp.sentinel.slots.block.BlockException;
        import com.alibaba.csp.sentinel.slots.block.RuleConstant;
        import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
        import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
        import java.util.ArrayList;
        import java.util.List;
        import java.util.concurrent.CountDownLatch;
        import java.util.concurrent.TimeUnit;
        import java.util.concurrent.atomic.AtomicInteger;

public class FlowThreadDemo1 {

    private static AtomicInteger pass = new AtomicInteger();
    private static AtomicInteger block = new AtomicInteger();
    private static final int threadCount = 10;
    private static final CountDownLatch countDownLatch = new CountDownLatch(threadCount);
    private static volatile int sleepTime = 10000;

    public static void main(String[] args) throws Exception {
        initFlowRule();

        for (int i = 1; i <= threadCount; i++) {
            Thread entryThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Entry methodA = null;
                    try {
                        methodA = SphU.entry("methodA");
                        pass.addAndGet(1);
                        TimeUnit.MILLISECONDS.sleep(sleepTime);
                        System.out.println(Thread.currentThread().getName() + " 业务正常通过");
                    } catch (BlockException e1) {
                        block.incrementAndGet();
                        System.err.println(Thread.currentThread().getName() + " 业务被限流");
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    } finally {
                        countDownLatch.countDown();
                        if (methodA != null) {
                            methodA.exit();
                        }
                    }
                }
            });
            // 控制线程数量缓慢增加
            TimeUnit.MILLISECONDS.sleep(10);
            entryThread.setName("working thread" + i);
            entryThread.start();
        }
        countDownLatch.await();
        System.out.println("pass:" + pass.get() + ", block:" + block.get());
    }

    private static void initFlowRule() {
        List<FlowRule> rules = new ArrayList<FlowRule>();
        FlowRule rule1 = new FlowRule();
        rule1.setResource("methodA");
        // set limit concurrent thread for 'methodA' to 20
        rule1.setCount(4);
        rule1.setGrade(RuleConstant.FLOW_GRADE_QPS);
        rule1.setLimitApp("default");

        rules.add(rule1);
        FlowRuleManager.loadRules(rules);
    }
}
