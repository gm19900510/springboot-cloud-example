package com.gm.sentineldemo;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.EntryType;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.Tracer;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;
import com.alibaba.csp.sentinel.slots.block.degrade.circuitbreaker.CircuitBreakerStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class DegradeDemo1 {

    private static final int threadCount = 100;
    private static final CountDownLatch countDownLatch = new CountDownLatch(threadCount);

    public static void main(String[] args) throws Exception {
        initDegradeRule();

        for (int i = 1; i <= threadCount; i++) {
            Thread entryThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    methodA();
                }
            });
            // 控制线程数量缓慢增加
            TimeUnit.MILLISECONDS.sleep(10);
            entryThread.setName("working thread" + i);
            entryThread.start();
        }
        countDownLatch.await();
    }

    private static void methodA() {
        Entry entry = null;
        try {
            entry = SphU.entry("methodA", EntryType.IN);
            TimeUnit.MILLISECONDS.sleep(500);
            throw new Exception("自定义异常");

        } catch (Throwable t) {
            if (!BlockException.isBlockException(t)) {
                Tracer.trace(t);
            }
            System.err.println(Thread.currentThread().getName() + t.getMessage());
        } finally {
            if (entry != null) {
                entry.exit();
            }
            countDownLatch.countDown();
        }
    }

    private static void initDegradeRule() {
        List<DegradeRule> rules = new ArrayList<>();
        DegradeRule rule = new DegradeRule("methodA").setGrade(CircuitBreakerStrategy.ERROR_COUNT.getType())
                // Max allowed response time
                .setCount(5)
                // Retry timeout (in second)
                .setTimeWindow(1)
                // Circuit breaker opens when slow request ratio > 60%
                .setMinRequestAmount(5).setStatIntervalMs(10000);
        rules.add(rule);

        DegradeRuleManager.loadRules(rules);
        System.out.println("Degrade rule loaded: " + rules);
    }
}
