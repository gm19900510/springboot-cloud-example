package com.gm.sentinel_nacos_consumer.service.impl;

import com.gm.sentinel_nacos_consumer.service.ProviderServiceFeign;

public class ProviserServiceFallback implements ProviderServiceFeign {
    @Override
    public String sayHello(String world) {
        return "fallback";
    }
}
