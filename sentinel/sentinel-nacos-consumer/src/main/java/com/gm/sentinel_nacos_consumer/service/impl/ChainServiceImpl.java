package com.gm.sentinel_nacos_consumer.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.gm.sentinel_nacos_consumer.service.ChainService;
import org.springframework.stereotype.Service;

@Service
public class ChainServiceImpl implements ChainService {

    @SentinelResource(value = "message")
    public String message() {
        return "hello world";
    }
}