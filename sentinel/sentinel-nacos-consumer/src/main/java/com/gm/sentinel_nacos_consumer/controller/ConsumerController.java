package com.gm.sentinel_nacos_consumer.controller;

import com.gm.sentinel_nacos_consumer.service.ChainService;
import com.gm.sentinel_nacos_consumer.service.ProviderServiceFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ConsumerController {

    @Autowired
    ProviderServiceFeign providerServiceFeign;
    @Autowired
    ChainService chainService;
    @Autowired
    RestTemplate restTemplate;

    /**
     * 用于测试Feign支持
     *
     * @return
     */
    @RequestMapping(value = "sayHello", method = RequestMethod.GET)
    public String sayHello() {
        return providerServiceFeign.sayHello("hello world");
    }

    /**
     * 用于测试STRATEGY_DIRECT模式，根据调用方进行限流。
     *
     * @return
     */
    @RequestMapping(value = "sayHelloDirect", method = RequestMethod.GET)
    public String sayHelloDirect() {
        return chainService.message();
    }

    /**
     * 用于测试STRATEGY_CHAIN模式，根据调用链路入口限流。
     *
     * @return
     */
    @RequestMapping(value = "sayHelloChain", method = RequestMethod.GET)
    public String sayHelloChain() {
        return chainService.message();
    }

    /**
     * 用于restTemplate限流测试
     *
     * @return
     */
    @RequestMapping(value = "sayHelloRest", method = RequestMethod.GET)
    public String sayHelloRestTemplate() {
        String url = String.format("http://sentinel-nacos-provider/sayHello?world=hello world");
        String result = restTemplate.getForObject(url, String.class);
        return result;
    }

    /**
     * 测试UrlCleaner自定义实现URL归类
     *
     * @param value
     * @return
     */
    @RequestMapping(value = "say/{value}", method = RequestMethod.GET)
    public String say(@PathVariable String value) {
        return value;
    }

    /**
     * 测试UrlCleaner自定义实现URL归类
     *
     * @param value
     * @return
     */
    @RequestMapping(value = "clear/{value}", method = RequestMethod.GET)
    public String clear(@PathVariable String value) {
        return value;
    }

}
