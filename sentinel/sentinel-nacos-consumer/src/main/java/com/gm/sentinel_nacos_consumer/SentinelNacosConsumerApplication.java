package com.gm.sentinel_nacos_consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.gm.sentinel_nacos_consumer")
public class SentinelNacosConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SentinelNacosConsumerApplication.class, args);
    }
}
