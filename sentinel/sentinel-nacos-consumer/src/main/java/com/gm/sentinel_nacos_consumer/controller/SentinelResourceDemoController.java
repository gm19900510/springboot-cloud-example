package com.gm.sentinel_nacos_consumer.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class SentinelResourceDemoController {

    private static AtomicInteger pass = new AtomicInteger();

    /**
     * 配置blockHandler，配置流控规则，发生流控时进入blockHandler逻辑
     *
     * @return
     */
    @RequestMapping("/blockHandler")
    @SentinelResource(value = "blockHandler", blockHandler = "blockHandlerErr")
    public String blockHandler() {
        return "请求通过";
    }

    public String blockHandlerErr(BlockException be) {
        return "已被限流";
    }


    /**
     * 配置流控、熔断规则，发生流控或熔断时进入系统默认异常处理
     *
     * 在方法本身定义 throws BlockException，在流控发生时，可被全局异常捕获，而不是UndeclaredThrowableException
     *
     * @return
     */
    @RequestMapping("/fallback")
    @SentinelResource(value = "fallback")
    public String fallback() throws BlockException {
        int i = pass.addAndGet(1);
        if (i % 2 == 0) {
            i = 1 / 0;
        }
        return "请求通过";
    }

    /**
     * 同时配置fallback、blockHandler，配置流控规则、熔断规则，发生流控时进入blockHandler，未发生流控而是熔断时进入fallback
     *
     * @return
     */
    @RequestMapping("/fallback2")
    @SentinelResource(value = "fallback2", fallback = "fallbackErr", blockHandler = "blockHandlerErr")
    public String fallback2() {
        int i = pass.addAndGet(1);
        if (i % 2 == 0) {
            i = 1 / 0;
        }
        return "请求通过";
    }

    public String fallbackErr(Throwable e) {
        e.printStackTrace();
        return "已被熔断";
    }

    /**
     * 同时配置fallback、defaultFallback，配置流控、熔断规则，发生熔断时进入fallback，限流时进入fallback
     *
     * @return
     */
    @RequestMapping("/fallback3")
    @SentinelResource(value = "fallback3", fallback = "fallbackErr", defaultFallback = "defaultFallbackErr")
    public String fallback3() {
        int i = pass.addAndGet(1);
        if (i % 2 == 0) {
            i = 1 / 0;
        }
        return "请求通过";
    }

    public String defaultFallbackErr(Throwable e) {
        return "默认异常处理";
    }


    /**
     * 配置exceptionsToIgnore，配置熔断规则，发生ArrayIndexOutOfBoundsException.class异常不计入熔断统计，
     * 也不会进入exceptionsToIgnoreFallbackErr，进入系统默认异常处理
     *
     * @return
     */
    @RequestMapping("/exceptionsToIgnore")
    @SentinelResource(value = "exceptionsToIgnore", exceptionsToIgnore = {ArrayIndexOutOfBoundsException.class},
            fallback = "exceptionsToIgnoreFallbackErr")
    public String exceptionsToIgnore() {
        int i = pass.addAndGet(1);
        if (i % 2 == 1) {
            int[] ints = {1, 2, 3};
            System.out.println(ints[10]);
        } else if (i % 3 == 1) {
            i = Integer.valueOf("s");
        }
        return "请求通过";
    }

    public String exceptionsToIgnoreFallbackErr(Throwable e) {
        return "已被熔断";
    }

    /**
     * 配置exceptionsToTrace，配置熔断规则，只有发生ArrayIndexOutOfBoundsException.class异常计入熔断统计，
     * 并进入exceptionsToIgnoreFallbackErr，其他异常进入系统默认处理
     *
     * @return
     */
    @RequestMapping("/exceptionsToTrace")
    @SentinelResource(value = "exceptionsToTrace", exceptionsToTrace = {ArrayIndexOutOfBoundsException.class},
            fallback = "exceptionsToTraceFallbackErr")
    public String exceptionsToTrace() {
        int i = pass.addAndGet(1);
        if (i % 2 == 1) {
            int[] ints = {1, 2, 3};
            System.out.println(ints[10]);
        } else if (i % 3 == 1) {
            i = Integer.valueOf("s");
        }
        return "";
    }

    public String exceptionsToTraceFallbackErr(Throwable e) {
        return "已被熔断";
    }

}
