package com.gm.sentinel_nacos_consumer.service;

import com.gm.sentinel_nacos_consumer.config.FeignConfiguration;
import com.gm.sentinel_nacos_consumer.service.impl.ProviserServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "sentinel-nacos-provider", fallback = ProviserServiceFallback.class, configuration = FeignConfiguration.class)
public interface ProviderServiceFeign {

    @RequestMapping(value = "sayHello", method = RequestMethod.GET)
    String sayHello(@RequestParam("world") String world);

}
