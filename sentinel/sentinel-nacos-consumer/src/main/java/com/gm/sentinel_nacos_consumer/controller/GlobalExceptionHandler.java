package com.gm.sentinel_nacos_consumer.controller;


import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(BlockException.class)
    public String BlockException(BlockException e) {
        return "全局异常捕获：限流熔断";
    }

    @ExceptionHandler(Exception.class)
    public String ExceptionHandler(Exception exception) {
        return "全局异常处理：" + exception.getClass() + "  message:" + exception.getMessage();
    }
}

