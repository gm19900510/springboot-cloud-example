package com.gm.sentinel_nacos_consumer.component;

import com.alibaba.cloud.commons.io.IOUtils;
import com.alibaba.cloud.sentinel.rest.SentinelClientHttpResponse;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpResponse;

public class ExceptionUtil {

    public static ClientHttpResponse handleException(HttpRequest request, byte[] body, ClientHttpRequestExecution execution, BlockException exception) throws Exception {
        ClientHttpResponse response = new SentinelClientHttpResponse("请求被限流！");
        return response;
    }

}
