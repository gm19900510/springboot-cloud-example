package com.gm.sentinel_nacos_consumer.component;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.UrlCleaner;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.util.List;

@Slf4j
@Component
public class CustomUrlCleaner implements UrlCleaner {

    @Value("#{'${url-cleaner.list}'.split(',')}")
    private List<String> list;

    @Override
    public String clean(String s) {
        log.info(s);
        if (StringUtils.isBlank(s)) {
            return s;
        }
        for (String url : list) {
            if (s.startsWith(url)) {
                return url + "*";
            }
        }
        return s;
    }
}
