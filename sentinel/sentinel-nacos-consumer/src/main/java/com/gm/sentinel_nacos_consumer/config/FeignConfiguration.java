package com.gm.sentinel_nacos_consumer.config;

import com.gm.sentinel_nacos_consumer.service.impl.ProviserServiceFallback;
import org.springframework.context.annotation.Bean;

public class FeignConfiguration {

    @Bean
    public ProviserServiceFallback proviserServiceFallback() {
        return new ProviserServiceFallback();
    }
}
