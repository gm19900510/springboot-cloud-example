package com.gm.demo.nacos_config.component;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ConfigListener {

    /**
     * Nacos group.
     */
    public static final String GROUP = "DEFAULT_GROUP";

    @Autowired
    private NacosConfigManager nacosConfigManager;

    @Value("${spring.cloud.nacos.config.prefix}")
    private String prefix;

    //@Value("${spring.cloud.nacos.config.file-extension}")
    private String fileExtension = "yaml";

    @PostConstruct
    public void init() throws NacosException {

        String dataId = prefix + "." + fileExtension;
        log.error("add listener [dataId]:" + dataId);
        ConfigService configService = nacosConfigManager.getConfigService();

        configService.addListener(dataId, GROUP, new Listener() {
            @Override
            public Executor getExecutor() {
                return Executors.newSingleThreadExecutor();
            }

            @Override
            public void receiveConfigInfo(String content) {
                log.info("[dataId]:[" + dataId + "],Configuration changed to:");
                log.error("\n" + content);
            }
        });
    }
}
