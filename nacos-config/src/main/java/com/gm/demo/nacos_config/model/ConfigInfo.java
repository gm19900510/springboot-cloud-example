package com.gm.demo.nacos_config.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Data
@Component
@ConfigurationProperties(prefix = "config")
public class ConfigInfo {
    private String name;
    private Map<String, String> dbs;
}
