package com.gm.demo.nacos_config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.TimeUnit;

/**
 * nacos 配置获取 启动类
 *
 * @author 高明
 */
@SpringBootApplication
public class NacosConfigApplication {

    public static void main(String[] args) throws InterruptedException {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(NacosConfigApplication.class, args);
        while (true) {
            //当动态配置刷新时，会更新到 Enviroment中，因此这里每隔5秒中从Enviroment中获取配置
            String name = applicationContext.getEnvironment().getProperty("config.name");
            System.err.println("=========>>>>>>>>>>名字 :" + name);
            TimeUnit.SECONDS.sleep(5);
        }
    }
}
