package com.gm.demo.nacos_config.model;

import java.util.List;
import java.util.Map;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "microservice.baseyaml")
public class BaseYamlConfig {
    private String strVaule;
    private int intValue;
    private float floatValue;
    private boolean booleanValue;
    private List<String> listStrValue;
    private Map<String, String> mapStrValue;
    private Map<String, Credential> users;

    @Data
    public static class Credential {
        private String username;
        private String password;
    }
}
