package com.gm.demo.nacos_config.controller;

import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import com.gm.demo.nacos_config.model.BaseYamlConfig;
import com.gm.demo.nacos_config.model.ConfigInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
@RefreshScope
@RestController
@RequestMapping("/config")
public class ConfigController {

    @Autowired
    private ConfigInfo configInfo;

    @Autowired
    private BaseYamlConfig baseYamlConfig;

    @Value("${config.name:}")
    private String name;

    @Autowired
    private NacosConfigManager nacosConfigManager;

    @Value("${spring.cloud.nacos.config.prefix}")
    private String prefix;

    //@Value("${spring.cloud.nacos.config.file-extension}")
    private String fileExtension = "yaml";

    public static final String GROUP = "DEFAULT_GROUP";

    @Value("#{'${microservice.valueyaml.listStrValue}'.split(',')}")
    private List<String> listStrValue;

    @Value("#{${microservice.valueyaml.mapStrValue}}")
    private Map<String,String>  mapStrValue;


    @RequestMapping("/demo")
    public String demo() {
        log.error(String.valueOf(listStrValue));
        log.error(String.valueOf(mapStrValue));
        log.error(String.valueOf(baseYamlConfig));
        return "";
    }


    /**
     * 通过将配置信息配置为bean，支持配置变自动刷新
     *
     * @return
     */
    @RequestMapping("/model")
    public ConfigInfo model() {
        return configInfo;
    }

    /**
     * 通过 @Value 注解进行配置信息获取
     *
     * @return
     */
    @RequestMapping("/value")
    public String value() {
        return name;
    }

    /**
     * 对接 nacos 接口，通过接口完成对配置信息新增/替换单独属性
     *
     * @param name
     * @return
     * @throws NacosException
     */
    @RequestMapping("/publish")
    public boolean publishConfig(
            @RequestParam("name") String name) throws NacosException {
        String dataId = prefix + "." + fileExtension;
        String content = "config:\n" +
                "  name: " + name;
        ConfigService configService = nacosConfigManager.getConfigService();
        return configService.publishConfig(dataId, GROUP, content);
    }

    @RequestMapping("/getConfig")
    public String getConfig()
            throws NacosException {
        String dataId = prefix + "." + fileExtension;
        ConfigService configService = nacosConfigManager.getConfigService();
        return configService.getConfig(dataId, GROUP, 2000);
    }
}
