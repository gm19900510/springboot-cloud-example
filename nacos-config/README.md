# nacos-config

##  简介
nacos配置中心使用示例

## 说明

Nacos中分为三层隔离，分别是命名空间（namespace）隔离、组(group)隔离、集群（cluster-name）隔离。

- 命名空间是绝对隔离，配置和服务都不可调用。
- 组隔离是同一命名空间下，不同组配置可以共享和继承，但是服务不能跨组调用。
- 集群隔离是可以让同一命名空间，同一个分组下，同一集群内服务隔离或不隔离。



