# 一、专栏说明
`Spring Cloud`是一系列框架的有序集合。它利用`Spring Boot`的开发便利性巧妙地简化了分布式系统基础设施的开发，如:服务发现/注册、配置中心、消息总线、负载均衡、断路器、数据监控等，都可以用`Spring Boot`的开发风格做到一键启动和部署。

本文主要介绍`Spring Cloud`的基本使用,以实战为线索，逐步深入`Spring Boot`/`Spring Cloud`开发各个环节，体验掌握后端常用三方组件及性能优化思路，打造完整后端架构，提升工程化编码能力和思维能力。
# 二、版本说明
- Spring Boot 使用`2.7.9`版本
- Spring Cloud 使用`2021.0.5`版本
- Spring Cloud Alibaba 使用`2021.0.4.0`版本
>后续补充其他集成组件版本
> 
# 三、章节系列

[Spring Cloud入门篇 Hello World | Spring Cloud 1](https://gaoming.blog.csdn.net/article/details/129203542)

[mysql8 数据库安装及主从配置 | Spring Cloud 2](https://gaoming.blog.csdn.net/article/details/129215192)

[nacos 单机集群搭建及常用生产环境配置 | Spring Cloud 3](https://gaoming.blog.csdn.net/article/details/129217210)

[Spring Cloud融合Nacos实现服务的注册与发现 | Spring Cloud 4](https://blog.csdn.net/ctwy291314/article/details/129236554)
