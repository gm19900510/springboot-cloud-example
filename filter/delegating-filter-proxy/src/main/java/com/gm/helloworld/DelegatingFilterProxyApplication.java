package com.gm.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * filter定义方式一：DelegatingFilterProxyRegistrationBean + Filter
 * filter定义方式二：FilterRegistrationBean + Filter
 * filter定义方式三：@ServletComponentScan + @WebFilter
 */
@ServletComponentScan
@SpringBootApplication
public class DelegatingFilterProxyApplication {

    public static void main(String[] args) {
        SpringApplication.run(DelegatingFilterProxyApplication.class, args);
    }

}
