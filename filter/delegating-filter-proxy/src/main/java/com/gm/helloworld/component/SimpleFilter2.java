package com.gm.helloworld.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;


@Component("simpleFilter2")
@Slf4j
public class SimpleFilter2 implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("初始化");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("业务处理开始");
        filterChain.doFilter(servletRequest, servletResponse);
        log.info("业务处理结束");
    }

    public void destroy() {
        log.info("销毁");
    }
}
