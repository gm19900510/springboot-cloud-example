package com.gm.helloworld.component;

import org.springframework.boot.web.servlet.DelegatingFilterProxyRegistrationBean;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class SimpleConfig {

    @Bean
    public DelegatingFilterProxyRegistrationBean delegatingFilterProxyRegistrationBean() {
        DelegatingFilterProxyRegistrationBean delegatingFilterProxy = new DelegatingFilterProxyRegistrationBean("simpleFilter");
        delegatingFilterProxy.addUrlPatterns("/*");
        delegatingFilterProxy.setOrder(-5);
        Map<String, String> initParameters = new HashMap<>();
        initParameters.put("targetFilterLifecycle", "true");
        delegatingFilterProxy.setInitParameters(initParameters);
        return delegatingFilterProxy;
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new DelegatingFilterProxy());
        registrationBean.addUrlPatterns("/*");
        //被代理filter
        registrationBean.addInitParameter("targetBeanName", "simpleFilter2");
        //指明作用于filter的所有生命周期
        registrationBean.addInitParameter("targetFilterLifecycle", "true");
        registrationBean.setName("SimpleFilter");
        registrationBean.setOrder(1);
        registrationBean.setEnabled(false);
        return registrationBean;
    }


}
