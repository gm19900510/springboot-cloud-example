package com.gm.local.cache;

import java.util.ArrayList;
import java.util.List;

public class Coupon {
    private String name;
    private String type;
    private double discount;
    private double threshold;

    public Coupon(String name, String type, double discount, double threshold) {
        this.name = name;
        this.type = type;
        this.discount = discount;
        this.threshold = threshold;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public double getDiscount() {
        return discount;
    }

    public double getThreshold() {
        return threshold;
    }

    public double calculateDiscount(double totalPrice) {
        if (totalPrice >= threshold) {
            if (type.equals("discount")) {
                return totalPrice * discount;
            } else if (type.equals("reduce")) {
                return discount;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        List<Coupon> coupons = new ArrayList<>();
        coupons.add(new Coupon("新人优惠券", "reduce", 10, 100));
        coupons.add(new Coupon("满100减20", "reduce", 20, 100));
        coupons.add(new Coupon("满200减50", "reduce", 50, 200));
        coupons.add(new Coupon("满300打8折", "discount", 0.8, 300));

        double totalPrice = 250;
        double discount = 0;
        for (Coupon coupon : coupons) {
            discount += coupon.calculateDiscount(totalPrice);
        }
        double finalPrice = totalPrice - discount;
        System.out.println("总价：" + totalPrice);
        System.out.println("优惠：" + discount);
        System.out.println("实付：" + finalPrice);
    }
}