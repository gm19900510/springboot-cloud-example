package com.gm.local.cache.config;

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Caffeine配置类
 */
@Configuration
@EnableCaching
public class CaffeineConfig {

    @Bean
    public CacheManager caffeineCacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        List<CaffeineCache> caches = new ArrayList<>();
        caches.add(new CaffeineCache(CacheNameConstant.CACHE_5SECS,
                Caffeine.newBuilder().expireAfterWrite(5, TimeUnit.SECONDS).build()));
        caches.add(new CaffeineCache(CacheNameConstant.CACHE_10SECS,
                Caffeine.newBuilder().expireAfterWrite(10, TimeUnit.SECONDS).build()));
        caches.add(new CaffeineCache(CacheNameConstant.CACHE_30SECS,
                Caffeine.newBuilder().expireAfterWrite(300, TimeUnit.SECONDS).build()));
        caches.add(new CaffeineCache(CacheNameConstant.USERS,
                Caffeine.newBuilder().expireAfterWrite(120, TimeUnit.SECONDS).build()));
        cacheManager.setCaches(caches);
        return cacheManager;
    }

    /**
     * 配置缓存管理器（代码方式配置）
     *
     * @return 缓存管理器
     */
    /**
    @Bean
    public CacheManager cacheManager() {
        Caffeine<Object, Object> caffeineCache = caffeineCache();

        CaffeineCacheManager cacheManager = new CaffeineCacheManager();
        cacheManager.setCaffeine(caffeineCache);
        // 设定缓存器名称
        cacheManager.setCacheNames(getNames());
        // 值不可为空
        cacheManager.setAllowNullValues(false);
        return cacheManager;
    }

    private static List<String> getNames() {
        List<String> names = new ArrayList<>(1);
        names.add(CacheNameConstant.USERS);
        return names;
    }

    @Bean
    public Caffeine<Object, Object> caffeineCache() {
        return Caffeine.newBuilder()
                // 设置最后一次写入或访问后经过固定时间过期
                .expireAfterWrite(60, TimeUnit.SECONDS)
                // 初始的缓存空间大小
                .initialCapacity(100)
                // 缓存的最大条数
                .maximumSize(1000);
    }
    **/

    /**
     * Caused by: java.lang.IllegalStateException: refreshAfterWrite requires a LoadingCache
     *
     * @return
     */
    /*@Bean
    public CacheLoader<Object, Object> cacheLoader() {
        return this::loadData;
    }*/

    /**
     * 根据key加载缓存元素
     * @param key
     * @return
     */
    /*private Object loadData(Object key) {
        return null;
    }*/


}