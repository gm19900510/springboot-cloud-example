package com.gm.local.cache.config;

public interface CacheNameConstant {
    String CACHE_DEFAULT = "CACHE_DEFAULT";
    String CACHE_5SECS = "CACHE_5SECS";
    String CACHE_10SECS = "CACHE_10SECS";
    String CACHE_30SECS = "CACHE_30SECS";
    String USERS = "USERS";
}
