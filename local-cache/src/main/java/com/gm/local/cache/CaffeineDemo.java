package com.gm.local.cache;

import com.github.benmanes.caffeine.cache.*;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.index.qual.NonNegative;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.io.Serializable;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

@Slf4j
public class CaffeineDemo {

    private static void manual() {
        // 构建caffeine的缓存对象，并指定在写入后的10分钟内有效，且最大允许写入的条目数为10000
        Cache<String, String> cache = Caffeine.newBuilder().expireAfterWrite(10, TimeUnit.MINUTES).maximumSize(10_000).build();
        String key = "hello";
        // 查找某个缓存元素，若找不到则返回null
        String str = cache.getIfPresent(key);
        System.out.println("cache.getIfPresent(key) ---> " + str);
        // 查找某个缓存元素，若找不到则调用函数生成，如无法生成则返回null
        str = cache.get(key, k -> create(key));
        System.out.println("cache.get(key, k -> create(key)) ---> " + str);
        // 添加或者更新一个缓存元素
        cache.put(key, str);
        System.out.println("cache.put(key, str) ---> " + cache.getIfPresent(key));
        // 移除一个缓存元素
        cache.invalidate(key);
        System.out.println("cache.invalidate(key) ---> " + cache.getIfPresent(key));
    }

    public static void loading() {
        LoadingCache<String, String> cache = Caffeine.newBuilder().maximumSize(10_000).expireAfterWrite(10, TimeUnit.MINUTES).build(key -> create(key)); // 当调用get或者getAll时，若找不到缓存元素，则会统一调用create(key)生成
        String key = "hello";
        String str = cache.get(key);
        System.out.println("cache.get(key) ---> " + str);
        List<String> keys = Arrays.asList("a", "b", "c", "d", "e");
        // 批量查找缓存元素，如果缓存不存在则生成缓存元素
        Map<String, String> maps = cache.getAll(keys);
        System.out.println("cache.getAll(keys) ---> " + maps);
    }


    private static void asynchronous() {
        AsyncCache<String, String> cache = Caffeine.newBuilder().expireAfterWrite(10, TimeUnit.MINUTES).maximumSize(10_000).buildAsync();
        String key = "Hello";
        // 查找某个缓存元素，若找不到则返回null
        CompletableFuture<String> value = cache.getIfPresent(key);
        // 查找某个缓存元素，若不存在则异步调用create方法生成
        value = cache.get(key, k -> create(key));
        // 添加或者更新一个缓存元素
        cache.put(key, value);
        // 移除一个缓存元素
        cache.synchronous().invalidate(key);
    }


    private static void asynchronouslyLoading() {
        AsyncLoadingCache<String, String> cache = Caffeine.newBuilder().maximumSize(10_000).expireAfterWrite(10, TimeUnit.MINUTES)
                // 异步构建一个同步的调用方法create(key)
                .buildAsync(key -> create(key));
        // 也可以使用下面的方式来异步构建缓存，并返回一个future
        // .buildAsync((key, executor) -> createAsync(key, executor));
        String key = "Hello";
        // 查找某个缓存元素，若找不到则会异步生成。
        CompletableFuture<String> value = cache.get(key);
        List<String> keys = Arrays.asList("a", "b", "c", "d", "e");
        // 批量查找某些缓存元素，若找不到则会异步生成。
        CompletableFuture<Map<String, String>> values = cache.getAll(keys);
    }

    private static String create(Object key) {
        return key + " world";
    }


    private static void removalsListeners() {
        Cache<String, String> cache = Caffeine.newBuilder().expireAfterAccess(5, TimeUnit.SECONDS)
                // 注意：evictionListener是3.X版本中新特性，2.X版本中没有
                //.scheduler(Scheduler.forScheduledExecutorService(Executors.newScheduledThreadPool(1)))
                //.executor(Executors.newScheduledThreadPool(1))
                .evictionListener((String key, String value, RemovalCause cause) -> System.out.printf("EvictionListener Key %s was removed (%s)%n", key, cause)).removalListener((String key, String value, RemovalCause cause) -> System.out.printf("RemovalListener Key %s was removed (%s)%n", key, cause)).build();
        cache.put("Hello", "Caffeine");
        System.out.println(cache.getIfPresent("Hello"));
        // cache.invalidate("Hello");
        try {
            // 监听是异步执行的
            Thread.sleep(1000000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // cache.put("Hello", "guave");
        // System.out.println(cache.getIfPresent("Hello"));
    }

    private static void customTime() throws InterruptedException {
        LoadingCache<String, String> cache = Caffeine.newBuilder()
                .scheduler(Scheduler.forScheduledExecutorService(Executors.newScheduledThreadPool(1)))
                .evictionListener((String key, String value, RemovalCause cause) -> {
                    log.info("EvictionListener key {} was removed {}", key, cause);
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }).removalListener((String key, String value, RemovalCause cause) -> {
                    log.info("RemovalListener key {} was removed {}", key, cause);
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }).expireAfter(new Expiry<String, String>() {
                    @Override
                    public long expireAfterCreate(@NonNull String key, @NonNull String value, long currentTime) {
                        // 这里的currentTime由Ticker提供，默认情况下与系统时间无关，单位为纳秒
                        log.info("expireAfterCreate----key:{},value:{},currentTime:{}", key, value, currentTime);
                        return TimeUnit.SECONDS.toNanos(10);
                    }

                    @Override
                    public long expireAfterUpdate(@NonNull String key, @NonNull String value, long currentTime, @NonNegative long currentDuration) {
                        // 这里的currentTime由Ticker提供，默认情况下与系统时间无关，单位为纳秒
                        log.info("expireAfterUpdate----key:{},value:{},currentTime:{},currentDuration:{}", key, value, currentTime, currentDuration);
                        return TimeUnit.SECONDS.toNanos(5);
                    }

                    @Override
                    public long expireAfterRead(@NonNull String key, @NonNull String value, long currentTime, @NonNegative long currentDuration) {
                        // 这里的currentTime由Ticker提供，默认情况下与系统时间无关，单位为纳秒
                        log.info("expireAfterRead----key:{},value:{},currentTime:{},currentDuration:{}", key, value, currentTime, currentDuration);
                        return TimeUnit.SECONDS.toNanos(5);
                    }
                }).build(key -> create(key));

        String one = cache.get("one");
        log.info("第一次获取one:{}", one);

        String two = cache.get("two");
        log.info("第一次获取two:{}", two);

        cache.put("one", one + "_new");

        log.info("---------------开始休眠5秒---------------");
        Thread.sleep(5000);
        log.info("---------------结束休眠5秒---------------");

        one = cache.get("one");
        log.info("第二次获取one:{}", one);

        two = cache.get("two");
        log.info("第二次获取two:{}", two);

        log.info("---------------开始休眠10秒---------------");
        Thread.sleep(10000);
        log.info("---------------结束休眠10秒---------------");

        one = cache.get("one");
        log.info("第三次获取one:{}", one);

        two = cache.get("two");
        log.info("第三次获取two:{}", two);

        Thread.sleep(20000);
        //cache.cleanUp();

        Thread.sleep(20000);
    }

    private static String create(String key) {
        log.info("自动加载数据:{}", key);
        return UUID.randomUUID().toString();
    }

    private static void refresh() throws InterruptedException {
        LoadingCache<String, String> cache = Caffeine.newBuilder()
                .scheduler(Scheduler.forScheduledExecutorService(Executors.newScheduledThreadPool(1)))
                .evictionListener((String key, String value, RemovalCause cause) -> {
                    log.info("EvictionListener key {} was removed {}", key, cause);
                }).removalListener((String key, String value, RemovalCause cause) -> {
                    log.info("RemovalListener key {} was removed {}", key, cause);
                }).expireAfterWrite(10, TimeUnit.SECONDS).
                build(key -> create(key));

        String one = cache.get("one");
        log.info("第一次获取one:{}", one);

        String two = cache.get("two");
        log.info("第一次获取two:{}", two);

        log.info("---------------开始休眠30秒---------------");
        Thread.sleep(30000);
        log.info("---------------结束休眠30秒---------------");

        one = cache.get("one");
        log.info("第二次获取one:{}", one);

        two = cache.get("two");
        log.info("第二次获取two:{}", two);
    }

    public static void main(String[] args) throws InterruptedException {
        refresh();
    }


}
