package com.gm.local.cache.service.impl;

import com.gm.local.cache.config.CacheNameConstant;
import com.gm.local.cache.entity.User;
import com.gm.local.cache.repository.UserDao;
import com.gm.local.cache.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Cacheable(cacheNames = CacheNameConstant.USERS, key = "#id")
    // @Cacheable(value = "users", key = "#p0")
    // @Cacheable(value = "users", key = "#user.id")
    // @Cacheable(value = "users", key = "#p0.id")
    public User find(Integer id) {
        log.info("进入方法find，参数{}",id);
        return userDao.findById(id).orElse(null);
    }


    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    @CacheEvict(cacheNames = CacheNameConstant.USERS, key = "#user.id")
    public void delete(User user) {
        userDao.deleteById(user.getId());
    }

    @Override
    @CacheEvict(cacheNames = CacheNameConstant.USERS, key = "#user.id")
    public void update(User user) {
        userDao.save(user);
    }

    @Override
    @Cacheable(cacheNames = CacheNameConstant.USERS, unless = "#result == null")
    public User find(User user) {
        Example<User> example = Example.of(user);
        return userDao.findOne(example).orElse(null);
    }

    @Override
    @Cacheable(cacheNames = CacheNameConstant.USERS, key = "#p0", sync = true)
    public List<User> findAll(String nickName) {
        return userDao.findByNickNameLike("%" + nickName + "%");
    }
}
