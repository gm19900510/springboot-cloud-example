# 客户端
## 一、单配置
查看application-single.yaml
## 二、双配置
查看application-multi.yaml，其中messaging-client访问自己搭建的授权服务，详见simple-oauth2-authorization-server

取保确保messaging-client的scope包含openid获取用户信息，其中用户信息接口为/userinfo，可通过http://127.0.0.1:8080/.well-known/oauth-authorization-server
查看授权服务元数据信息

>其中在授权服务中配置的redirect_uri不能为localhost和127.0.0.1
>在客户端配置的client-secret为加密前的内容，并配置jwk-set-uri避免出现：
> [missing_signature_verifier] Failed to find a Signature Verifier for Client Registration: 'messaging-client'. Check to ensure you have configured the JwkSet URI.