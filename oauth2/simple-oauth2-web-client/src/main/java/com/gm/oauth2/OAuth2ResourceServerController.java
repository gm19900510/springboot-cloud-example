package com.gm.oauth2;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OAuth2ResourceServerController {

    @Autowired
    private OAuth2AuthorizedClientManager authorizedClientManager;

    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;

    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

    // https://docs.spring.io/spring-security/reference/servlet/oauth2/client/authorization-grants.html
    // 获取客户端授权后使用的令牌

    // https://docs.spring.io/spring-security/reference/servlet/oauth2/client/core.html
    @GetMapping("/resource")
    public String resource(OAuth2AuthenticationToken authentication) {

        ClientRegistration registration =
                this.clientRegistrationRepository.findByRegistrationId("messaging-client");

        System.out.println(JSONUtil.parseObj(registration).toString());
        OAuth2AuthorizedClient authorizedClient =
                this.authorizedClientService.loadAuthorizedClient("messaging-client", authentication.getName());

        System.out.println(JSONUtil.parseObj(authorizedClient).toString());

        OAuth2AuthorizeRequest authorizeRequest = OAuth2AuthorizeRequest.withClientRegistrationId("messaging-client")
                .principal(authentication)
                .build();

        authorizedClient = this.authorizedClientManager.authorize(authorizeRequest);
        OAuth2AccessToken accessToken = authorizedClient.getAccessToken();

        return JSONUtil.parseObj(accessToken).toString();

    }
}
