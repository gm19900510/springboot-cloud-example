package com.gm.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OAuth2WebClientApplication  {

    public static void main(String[] args) {
        SpringApplication.run(OAuth2WebClientApplication.class, args);
    }

}
