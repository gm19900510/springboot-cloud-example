# 授权服务

## 一、授权码模式
1.先访问
http://127.0.0.1:8080/oauth2/authorize?client_id=messaging-client&response_type=code&scope=message.read&redirect_uri=http://www.baidu.com
获取授权码
> 注意redirect_uri必须提前完成配置

2.替换postman中code值
postman地址： https://galactic-comet-82546.postman.co/workspace/%E6%B5%8B%E8%AF%95oauth2~fe69da14-967e-48d0-885b-3262ec3aaf70/request/21200440-b04bdf33-7630-4dd3-b952-27ec758d934f?ctx=documentation

## 二、新增自定义验证码模式
postman地址： https://galactic-comet-82546.postman.co/workspace/%E6%B5%8B%E8%AF%95oauth2~fe69da14-967e-48d0-885b-3262ec3aaf70/request/21200440-901dc62c-c4a6-4973-947f-5ae28728dcab?ctx=documentation
## 三、新增自定义密码模式
postman地址： https://galactic-comet-82546.postman.co/workspace/%E6%B5%8B%E8%AF%95oauth2~fe69da14-967e-48d0-885b-3262ec3aaf70/request/21200440-901dc62c-c4a6-4973-947f-5ae28728dcab?ctx=documentation
## 四、设备码模式
https://github.com/gm19900510/spring-authorization-server/tree/main/samples 
## 五、令牌自省
postman地址： https://galactic-comet-82546.postman.co/workspace/%E6%B5%8B%E8%AF%95oauth2~fe69da14-967e-48d0-885b-3262ec3aaf70/request/21200440-24f4ce83-a3e3-4616-a8c0-df522814b6d7?ctx=documentation
## 六、查询用户信息
postman地址： https://galactic-comet-82546.postman.co/workspace/%E6%B5%8B%E8%AF%95oauth2~fe69da14-967e-48d0-885b-3262ec3aaf70/request/21200440-c640f220-b108-4dab-b511-370c62e821e3?ctx=documentation
>需要有opid的授权范围
> 
> 
> 

# 关于id_token的正确使用姿势

"id_token" 本身并不用于实现用户认证，而是用于传递已经认证的用户信息给客户端应用程序。用户认证通常是在身份提供者（如身份提供者或认证服务）处进行的，然后身份提供者生成 "id_token" 以确认用户身份并将用户信息传递给客户端应用程序。以下是实现用户认证和获取 "id_token" 的典型流程：

客户端请求认证： 客户端应用程序向身份提供者发出认证请求。这可以通过将用户重定向到身份提供者的认证终结点来完成，请求中包含必要的参数，如 response_type（设置为 "id_token"）、client_id、redirect_uri 和 scope。

用户身份验证： 用户被引导到身份提供者的认证界面，以进行身份验证。这通常需要用户提供其凭据，如用户名和密码，或使用其他身份验证方法，如社交登录、多因素身份验证等。

用户授权： 如果用户成功进行了身份验证，身份提供者将要求用户授权客户端应用程序访问其身份信息。用户可以选择同意或拒绝授权请求。

生成 "id_token"： 如果用户同意了授权请求，身份提供者将生成一个包含用户身份信息的 "id_token"。这个令牌通常是一个 JSON Web Token (JWT)，其中包含有关用户的基本信息，如用户ID、姓名、电子邮件等。身份提供者还会对令牌进行签名以确保其完整性。

传递 "id_token"： 身份提供者将生成的 "id_token" 返回给客户端应用程序，通常通过重定向回客户端的 redirect_uri。

客户端验证 "id_token"： 客户端应用程序需要验证接收到的 "id_token"，以确保其完整性和有效性。验证包括检查令牌的签名是否有效，并确保它包含所需的声明。通常，这是使用公钥来验证签名的。

使用 "id_token"： 客户端应用程序可以使用验证通过的 "id_token" 中的用户信息来完成用户身份验证过程，或将用户信息用于应用程序中的其他操作，如显示用户的姓名、电子邮件等。

维护 "id_token"： "id_token" 通常有一个有限的有效期。客户端应用程序需要处理令牌的过期和刷新，以确保用户的身份信息仍然有效。

总之，用户认证是在身份提供者处进行的，而 "id_token" 用于将认证后的用户信息传递给客户端应用程序。客户端应用程序需要负责验证和使用 "id_token" 来完成用户身份验证和授权过程。流程中的具体细节和步骤可能会根据身份提供者和客户端应用程序的实际要求而有所不同。