package com.gm.oauth2.config;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 生成密码进行加密
 */
public class Test {

    private static final BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

    public static String bcryptEncode(String password) {
        return bcryptEncoder.encode(password);
    }

    public static String genOauth2EncodePwd(String password) {
        return bcryptEncode(password);
    }

    public static void main(String[] args) {
        String oriPwd = "secret"; // 密码原文
        System.out.println(genOauth2EncodePwd(oriPwd)); // 密文
    }
}
