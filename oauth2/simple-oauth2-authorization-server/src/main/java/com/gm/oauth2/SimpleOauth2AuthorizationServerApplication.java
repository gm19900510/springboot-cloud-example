package com.gm.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleOauth2AuthorizationServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleOauth2AuthorizationServerApplication.class, args);
    }

}
