package com.gm.oauth2.config.customCode;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthorizationGrantAuthenticationToken;
import java.util.Map;

public class CustomCodeGrantAuthenticationToken extends OAuth2AuthorizationGrantAuthenticationToken {

    private final String code;

    public CustomCodeGrantAuthenticationToken(String code, Authentication clientPrincipal, Map<String, Object> additionalParameters) {
        super(new AuthorizationGrantType("urn:ietf:params:oauth:grant-type:custom_code"), clientPrincipal, additionalParameters);
        this.code = code;
    }
}
