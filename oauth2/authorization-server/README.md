# 授权码模式
1.先访问
http://127.0.0.1:8080/oauth2/authorize?client_id=messaging-client&response_type=code&scope=message.read&redirect_uri=http://www.baidu.com
获取授权码
2.替换postman中code值
postman地址： https://galactic-comet-82546.postman.co/workspace/%E6%B5%8B%E8%AF%95oauth2~fe69da14-967e-48d0-885b-3262ec3aaf70/request/21200440-b04bdf33-7630-4dd3-b952-27ec758d934f?ctx=documentation
# 自定义验证码模式
postman地址： https://galactic-comet-82546.postman.co/workspace/%E6%B5%8B%E8%AF%95oauth2~fe69da14-967e-48d0-885b-3262ec3aaf70/request/21200440-901dc62c-c4a6-4973-947f-5ae28728dcab?ctx=documentation
# 新增自定义密码模式
postman地址： https://galactic-comet-82546.postman.co/workspace/%E6%B5%8B%E8%AF%95oauth2~fe69da14-967e-48d0-885b-3262ec3aaf70/request/21200440-901dc62c-c4a6-4973-947f-5ae28728dcab?ctx=documentation
# 设备码模式
https://github.com/gm19900510/spring-authorization-server/tree/main/samples 