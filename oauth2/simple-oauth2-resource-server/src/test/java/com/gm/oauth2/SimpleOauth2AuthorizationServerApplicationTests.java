package com.gm.oauth2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@WebAppConfiguration
class SimpleOauth2AuthorizationServerApplicationTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username = "user", password = "password", roles = {"USER"})
    void getAdmin() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/helloworld")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(print())
                .andReturn();
    }

    @Test
    @WithMockUser(username = "user", password = "password", roles = {"USER"})
    void getAdmin2() throws Exception {
        String json = "{\n" +
                "\t\"grant_type\": \"password\",\n" +
                "\t\"code\": \"7QR49T1W3\"\n" +
                "}";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/oauth2/token")
                        .contentType(MediaType.APPLICATION_JSON).content(json)
                )
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andDo(print())
                .andReturn();
    }


}
