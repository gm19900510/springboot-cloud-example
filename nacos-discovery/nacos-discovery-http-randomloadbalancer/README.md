# nacos-discovery-http

##  简介
nacos服务注册/发现使用示例（使用随机负载均衡算法）


##  调用地址

- 服务提供者请求
http://127.0.0.1:4000/sayHello?world=hello%20world

- 服务调用者请求
http://127.0.0.1:3000/sayHello

- 查看EndPoint
http://nacos.kc:8848/actuator/nacosdiscovery



