package com.gm.nacos_discovery_http_consumer_randomloadbalancer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.gm.nacos_discovery_http_consumer_randomloadbalancer.service")
public class NacosHttpConsumerRandomLoadBalancerApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosHttpConsumerRandomLoadBalancerApplication.class, args);
    }
}
