package com.gm.nacos_discovery_http_consumer_nimbleloadbalancer.controller;

import com.gm.nacos_discovery_http_consumer_nimbleloadbalancer.service.ProviderClusterServiceFeign;
import com.gm.nacos_discovery_http_consumer_nimbleloadbalancer.service.ProviderServiceFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class ConsumerController {

    @Autowired
    ProviderServiceFeign providerServiceFeign;

    @Autowired
    ProviderClusterServiceFeign providerClusterServiceFeign;

    Map<String, AtomicInteger> synchronizedHashMapRandom = Collections.synchronizedMap(new HashMap<String, AtomicInteger>());

    Map<String, AtomicInteger> synchronizedHashMapNacos = Collections.synchronizedMap(new HashMap<String, AtomicInteger>());


    @RequestMapping(value = "sayRandomHello", method = RequestMethod.GET)
    public String sayRandomHello() {
        String result = providerServiceFeign.sayHello("hello world");
        AtomicInteger ai = synchronizedHashMapRandom.get(result);
        Optional<AtomicInteger> opt = Optional.ofNullable(ai);
        if (!opt.isPresent()) {
            ai = new AtomicInteger(1);
            synchronizedHashMapRandom.put(result, ai);
        } else {
            ai.getAndIncrement();
        }
        return synchronizedHashMapRandom.toString();
    }

    @RequestMapping(value = "sayNacosHello", method = RequestMethod.GET)
    public String sayNacosHello() {
        String result = providerClusterServiceFeign.sayHello("hello world");
        AtomicInteger ai = synchronizedHashMapNacos.get(result);
        Optional<AtomicInteger> opt = Optional.ofNullable(ai);
        if (!opt.isPresent()) {
            ai = new AtomicInteger(1);
            synchronizedHashMapNacos.put(result, ai);
        } else {
            ai.getAndIncrement();
        }
        return synchronizedHashMapNacos.toString();
    }

}
