# nacos-discovery-http

##  简介
nacos服务注册/发现使用示例（按服务名称灵活配置负载均衡算法）


##  调用地址

访问[http://127.0.0.1:7000/sayRandomHello](http://127.0.0.1:7000/sayRandomHello)，测试`RandomLoadBalancer` 负载均衡策略：

![在这里插入图片描述](https://img-blog.csdnimg.cn/128b07b0f46a42e490981287860b2788.png)

访问[http://127.0.0.1:7000/sayNacosHello](http://127.0.0.1:7000/sayNacosHello)，测试`NacosLoadBalancer` 负载均衡策略：
![在这里插入图片描述](https://img-blog.csdnimg.cn/d53485f2fa0f40b5b98f085d53a75875.png)



