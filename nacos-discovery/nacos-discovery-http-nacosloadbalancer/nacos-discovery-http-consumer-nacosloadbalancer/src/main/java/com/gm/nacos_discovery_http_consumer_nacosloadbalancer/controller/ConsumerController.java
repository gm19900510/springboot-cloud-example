package com.gm.nacos_discovery_http_consumer_nacosloadbalancer.controller;


import com.gm.nacos_discovery_http_consumer_nacosloadbalancer.service.ProviderServiceFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class ConsumerController {

    @Autowired
    ProviderServiceFeign providerServiceFeign;

    Map<String, AtomicInteger> synchronizedHashMap = Collections.synchronizedMap(new HashMap<String, AtomicInteger>());


    @RequestMapping(value = "sayHello", method = RequestMethod.GET)
    public String sayHello() {
        String result = providerServiceFeign.sayHello("hello world");
        AtomicInteger ai = synchronizedHashMap.get(result);
        Optional<AtomicInteger> opt = Optional.ofNullable(ai);
        if (!opt.isPresent()) {
            ai = new AtomicInteger(1);
            synchronizedHashMap.put(result, ai);
        } else {
            ai.getAndIncrement();
        }
        return synchronizedHashMap.toString();
    }

}
