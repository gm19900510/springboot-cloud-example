package com.gm.nacos_discovery_http_provider_cluster_haerbin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication
@EnableDiscoveryClient
public class NacosHttpProviderClusterHaerbinApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosHttpProviderClusterHaerbinApplication.class, args);
    }

}
