# nacos-discovery-http

##  简介
nacos服务注册/发现使用示例（使用集群负载均衡算法）


##  调用地址

- 服务提供者请求
>cluster-name: haerbin

http://127.0.0.1:4000/sayHello?world=hello%20world
>cluster-name: beijing

http://127.0.0.5000:4000/sayHello?world=hello%20world

- 服务调用者请求
http://127.0.0.1:3100/sayHello

- 验证步骤

两个服务提供者和一个服务调用者服务全部启动，请求服务调用者，多次刷新，发现服务调用者一直访问集群名称相关的服务提供者，将服务提供者在nacos中进行下线，发现服务调用者访问另一个集群名称的服务提供者



