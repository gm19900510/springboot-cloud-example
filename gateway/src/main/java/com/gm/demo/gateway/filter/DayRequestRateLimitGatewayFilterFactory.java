package com.gm.demo.gateway.filter;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Slf4j
public class DayRequestRateLimitGatewayFilterFactory extends AbstractGatewayFilterFactory<DayRequestRateLimitGatewayFilterFactory.Config> {

    private final Map<Long, Map<String, AtomicInteger>> rateLimitMap = new ConcurrentHashMap<>();

    public DayRequestRateLimitGatewayFilterFactory() {
        super(Config.class);
    }

    @Override
    public List<String> shortcutFieldOrder() {
        // yml配置文件中参数的赋值顺序
        return Arrays.asList("limit");
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            String path = exchange.getRequest().getPath().toString();
            long nowSecond = LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().getEpochSecond();
            Map<String, AtomicInteger> apiCount = rateLimitMap.computeIfAbsent(nowSecond, k -> new HashMap<String, AtomicInteger>() {{
                put(path, new AtomicInteger(1));
            }});
            AtomicInteger count = apiCount.computeIfAbsent(path, k -> new AtomicInteger(1));
            int countValue = count.get();
            log.debug("count: {}", countValue);
            if (countValue > config.getLimit()) {
                exchange.getResponse().setStatusCode(HttpStatus.TOO_MANY_REQUESTS);
                return exchange.getResponse().setComplete();
            }
            count.addAndGet(1);
            return chain.filter(exchange).then(Mono.fromRunnable(() -> {
                rateLimitMap.keySet().stream().filter(time -> time < nowSecond).forEach(rateLimitMap::remove);
            }));
        };
    }

    @Data
    public static class Config {
        private Integer limit;
    }
}
