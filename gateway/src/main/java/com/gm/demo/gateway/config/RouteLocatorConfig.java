package com.gm.demo.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteLocatorConfig {

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(p ->
                        p.path("/provider/**")            //请求路径
                                .filters(f -> f.stripPrefix(1))        //过滤前缀
                                .uri("http://localhost:4000")            //指向的uri
                )
                .route(p ->
                        p.path("/lb/**")
                                .filters(f -> f.stripPrefix(1))
                                .uri("lb://nacos-discovery-http-provider")
                )
                .route(p ->
                        p.path("/csdn/**")
                                .filters(f -> f.rewritePath("/csdn/(?<remaining>.*)", "/${remaining}"))
                                .uri("https://blog.csdn.net")
                )
                .build();
    }

}
