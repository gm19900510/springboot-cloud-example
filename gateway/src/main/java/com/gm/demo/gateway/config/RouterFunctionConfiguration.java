package com.gm.demo.gateway.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.nacos.shaded.com.google.gson.JsonObject;
import com.fasterxml.jackson.databind.util.JSONPObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
public class RouterFunctionConfiguration {

    @Bean
    public RouterFunction<ServerResponse> routerFunction() {
        return RouterFunctions.route(RequestPredicates.path("demo").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)), request -> {
            ServerRequest.Headers headers = request.headers();
            JSONObject json = new JSONObject();
            String realIP = headers.firstHeader("X-Real-IP");
            String forwardedFor = headers.firstHeader("X-Forwarded-For");
            String remoteAddr = request.remoteAddress().get().getHostString();
            log.info(realIP);
            log.info(forwardedFor);
            log.info(remoteAddr);
            json.put("realIP",realIP);
            json.put("forwardedFor",forwardedFor);
            json.put("remoteAddr",remoteAddr);
            return ok().contentType(MediaType.APPLICATION_JSON).body(Mono.just(json), JSONObject.class);
        });
    }
}