package com.gm.demo.gateway.component;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.cloud.gateway.handler.predicate.GatewayPredicate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.server.ServerWebExchange;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

@Component
@Slf4j
public class CustomRoutePredicateFactory extends AbstractRoutePredicateFactory<CustomRoutePredicateFactory.Config> {

    public CustomRoutePredicateFactory() {
        super(CustomRoutePredicateFactory.Config.class);
    }

    //  快捷方式配置由过滤器名称识别，后跟等号 (=)，然后是用逗号 (,) 分隔的参数值。
    @Override
    public List<String> shortcutFieldOrder() {
        // 必须跟config中的属性名进行绑定
        return Arrays.asList("param", "value");
    }

    @Override
    public Predicate<ServerWebExchange> apply(CustomRoutePredicateFactory.Config config) {
        return new GatewayPredicate() {
            @Override
            public boolean test(ServerWebExchange exchange) {
                if (!StringUtils.hasLength(config.getParam()) || !StringUtils.hasLength(config.getValue())) {
                    return false;
                }
                log.info(config.toString());
                List<String> values = (List)exchange.getRequest().getQueryParams().get(config.param);
                if (values != null && values.contains(config.value)) {
                    return true;
                }
                return false;
            }
        };
    }

    /**
     * 用于接收配置文件中 断言的信息
     */
    @Validated
    @Data
    public static class Config {

        private String param;

        private String value;
    }
}