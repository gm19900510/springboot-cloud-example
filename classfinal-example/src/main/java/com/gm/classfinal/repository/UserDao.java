package com.gm.classfinal.repository;

import com.gm.classfinal.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {
    List<User> findByNickNameLike(String name);
}