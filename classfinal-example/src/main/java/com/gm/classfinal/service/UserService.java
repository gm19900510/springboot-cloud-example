package com.gm.classfinal.service;

import com.gm.classfinal.entity.User;
import java.util.List;

public interface UserService {

    void save(User user);

    void delete(User user);

    void update(User user);

    User find(User user);

    User find(Integer id);

    List<User> findAll(String nickName);

}
