package com.gm.classfinal.service.impl;

import com.gm.classfinal.entity.User;
import com.gm.classfinal.repository.UserDao;
import com.gm.classfinal.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public User find(Integer id) {
        log.info("进入方法find，参数{}", id);
        return userDao.findById(id).orElse(null);
    }

    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public void delete(User user) {
        userDao.deleteById(user.getId());
    }

    @Override
    public void update(User user) {
        userDao.save(user);
    }

    @Override
    public User find(User user) {
        Example<User> example = Example.of(user);
        return userDao.findOne(example).orElse(null);
    }

    @Override
    public List<User> findAll(String nickName) {
        return userDao.findByNickNameLike("%" + nickName + "%");
    }
}
