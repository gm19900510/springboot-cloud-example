package com.gm.classfinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassfinalApplication {
    public static void main(String[] args) {
        SpringApplication.run(ClassfinalApplication.class, args);
    }
}
