package com.gm.classfinal.controller;

import com.gm.classfinal.entity.User;
import com.gm.classfinal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/find")
    public User find(@RequestParam int id) {
        return userService.find(id);
    }

    @GetMapping("/save")
    public String save(@RequestParam int id, @RequestParam String nickName) {
        User user = new User();
        user.setId(id);
        user.setNickName(nickName);
        userService.save(user);
        return "保存成功";
    }

    @GetMapping("/update")
    public String update(@RequestParam int id, @RequestParam String nickName) {
        User user = new User();
        user.setId(id);
        user.setNickName(nickName);
        userService.update(user);
        return "修改成功";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam int id) {
        User user = new User();
        user.setId(id);
        userService.delete(user);
        return "删除功能";
    }

    @GetMapping("/findAll")
    public Object findAll(@RequestParam String nickName) {
        return userService.findAll(nickName);
    }
}
