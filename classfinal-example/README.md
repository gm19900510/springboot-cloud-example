# 一 官网地址
https://gitee.com/roseboy/classfinal

# 二 使用说明

在Windows下发现：

java -javaagent:classfinal-example-encrypted.jar='-pwd 123456' -jar classfinal-example-encrypted.jar

不生效，但在同级目录下的classfinal.txt或yourpaoject-encrypted.classfinal.txt中写入密码生效。

在Linux下发现：

java -javaagent:classfinal-example-encrypted.jar='-pwd 123456' -jar classfinal-example-encrypted.jar

生效，在设置机器码时会自动读取（需在加密时读取到机器码）