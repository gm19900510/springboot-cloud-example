package com.gm.mqtt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.sql.DataSource;

@RestController
public class HelloWorldController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private JavaMailSenderImpl sender;

    @Value("${spring.mail.properties.from}")
    private String from;

    @Value("${spring.mail.properties.from}")
    private String to;


    @GetMapping("/helloworld")
    public String getUser() {
        return "hello world";
    }

    @GetMapping("/dockerfile")
    public String dockerfileVersion() {
        return "dockerfileVersion 2.0";
    }

    @GetMapping("/showtable")
    public Object showTable() {
        return jdbcTemplate.queryForList("show tables");
    }

    @GetMapping("/mail/send")
    public String sendMail() {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(from);
        simpleMailMessage.setTo(to);
        simpleMailMessage.setSubject("spring boot 发送邮件");
        simpleMailMessage.setText("我的第一封邮件！！！");
        sender.send(simpleMailMessage);
        return "success";
    }

}
