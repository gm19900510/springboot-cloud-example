package com.gm.mqtt.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SensorOriginalDataUtil {

	public SensorOriginalDataBean parseSensorOriginalDataBle5Str(String data) {
		try {
			SensorOriginalDataBean sensorOriginalDataBean = new SensorOriginalDataBean();
			sensorOriginalDataBean.setSensorType(Integer.parseInt(data.substring(0, 2)));
			sensorOriginalDataBean.setSensorState(Integer.parseInt(data.substring(2, 4), 16));
			sensorOriginalDataBean.setSensorDataType(Integer.parseInt(data.substring(4, 6), 16));
			String firmwareVersionHigh = data.substring(6, 7);
			String firmwareVersionLow = data.substring(7, 8);
			sensorOriginalDataBean.setFirmwareVersion(firmwareVersionHigh + "." + firmwareVersionLow);
			Integer[] epdVersion = new Integer[Integer.BYTES];
			for (int i = 0; i < Integer.BYTES; i++) {
				epdVersion[i] = Integer.parseInt(data.substring(8 + i * 2, 10 + i * 2), 16);
			}
			sensorOriginalDataBean.setEpdVersion(epdVersion[0] + epdVersion[1] * 256 + epdVersion[2] * 65536);
			sensorOriginalDataBean.setEpdStyle(epdVersion[3]);
			long[] advCount = new long[Integer.BYTES];
			for (int i = 0; i < Integer.BYTES; i++) {
				advCount[i] = Long.parseLong(data.substring(16 + i * 2, 18 + i * 2), 16);
			}
			sensorOriginalDataBean
					.setAdvCount(advCount[0] + advCount[1] * 256 + advCount[2] * 65536 + advCount[3] * 16777216);
			sensorOriginalDataBean.setSensorMacAddr(data.substring(24, 36));

			sensorOriginalDataBean.setBatteryLevel(Integer.parseInt(data.substring(36, 38), 16));
			sensorOriginalDataBean.setSensorThreshold(Integer.parseInt(data.substring(38, 40), 16));
			sensorOriginalDataBean.setReversed(Integer.parseInt(data.substring(40, 42), 16));
			sensorOriginalDataBean.setSensorData(data.substring(42, 96));
			sensorOriginalDataBean.setSignalIntensity(Integer.parseInt(data.substring(96, 98), 16));
			sensorOriginalDataBean.setSendTimestamp(Long.parseLong(data.substring(98, 111), 10));
			return sensorOriginalDataBean;
		}
		catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}

	public SensorOriginalDataBean parseSensorOriginalDataBle4Str(String data) {
		try {
			SensorOriginalDataBean sensorOriginalDataBean = new SensorOriginalDataBean();
			sensorOriginalDataBean.setSensorType(Integer.parseInt(data.substring(0, 2)));
			sensorOriginalDataBean.setSensorState(Integer.parseInt(data.substring(2, 4), 16));
			sensorOriginalDataBean.setSensorDataType(Integer.parseInt(data.substring(4, 6), 16));
			String firmwareVersionHigh = data.substring(6, 7);
			String firmwareVersionLow = data.substring(7, 8);
			sensorOriginalDataBean.setFirmwareVersion(firmwareVersionHigh + "." + firmwareVersionLow);
			Integer[] epdVersion = new Integer[Integer.BYTES];
			for (int i = 0; i < Integer.BYTES; i++) {
				epdVersion[i] = Integer.parseInt(data.substring(8 + i * 2, 10 + i * 2), 16);
			}
			sensorOriginalDataBean.setEpdVersion(epdVersion[0] + epdVersion[1] * 256 + epdVersion[2] * 65536);
			sensorOriginalDataBean.setEpdStyle(epdVersion[3]);
			long[] advCount = new long[Integer.BYTES];
			for (int i = 0; i < Integer.BYTES; i++) {
				advCount[i] = Long.parseLong(data.substring(16 + i * 2, 18 + i * 2), 16);
			}
			sensorOriginalDataBean
					.setAdvCount(advCount[0] + advCount[1] * 256 + advCount[2] * 65536 + advCount[3] * 16777216);
			sensorOriginalDataBean.setSensorMacAddr(data.substring(24, 36));
			sensorOriginalDataBean.setBatteryLevel(Integer.parseInt(data.substring(36, 38), 16));
			sensorOriginalDataBean.setSensorThreshold(Integer.parseInt(data.substring(38, 40), 16));
			sensorOriginalDataBean.setSignalIntensity(Integer.parseInt(data.substring(40, 42), 16));
			sensorOriginalDataBean.setSendTimestamp(Long.parseLong(data.substring(42, 55), 10));
			return sensorOriginalDataBean;
		}
		catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}

}
