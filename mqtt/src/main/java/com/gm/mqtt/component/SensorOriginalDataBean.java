package com.gm.mqtt.component;

import lombok.Data;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * sensor原始数据实体
 */
@Data
public class SensorOriginalDataBean {

	private Long receiveTimestamp = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();

	private Integer sensorType;

	/**
	 * 传感器硬件状态
	 */
	private Integer sensorState;

	/**
	 * 传感器数据类型
	 */
	private Integer sensorDataType;

	private String firmwareVersion;

	private int epdVersion;

	private int epdStyle;

	private Long advCount;

	private String sensorMacAddr;

	private Integer batteryLevel = 0xff;

	private Integer sensorThreshold = 0;

	private Integer reversed = 0;

	private String sensorData;

	private Integer signalIntensity = 255;

	private Long sendTimestamp;

	private Long brandId;

	private Long shopId;

	private Integer hangState = 0; // 默认0：未知状态

	private Boolean heartbeatException;

	private Boolean allocationState;

}
