package com.gm.mqtt.component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class MqttComponent {

    public static String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    public static final Integer ETAG_B_ORIGINAL_DATA_BLE5_LENGTH = 111;

    public static final Integer ETAG_B_ORIGINAL_DATA_BLE4_LENGTH = 55;

    @Autowired
    SensorOriginalDataUtil sensorOriginalDataUtil;

    @PostConstruct // 构造函数之后执行
    public void init() {
        String broker = "tcp://core.mttsmart.com:1883";
        String topic = "mqtt/test";
        String username = "mqtt";
        String password = "!Mtt2020";
        String clientId = "subscribe_client";
        clientId = getRandomString(10);
        int qos = 0;

        try {
            MqttClient client = new MqttClient(broker, "test_" + clientId, new MemoryPersistence());
            // 连接参数
            MqttConnectOptions options = new MqttConnectOptions();
            options.setUserName(username);
            options.setPassword(password.toCharArray());
            options.setConnectionTimeout(60);
            options.setKeepAliveInterval(60);
            // 设置回调
            client.setCallback(new MqttCallback() {

                public void connectionLost(Throwable cause) {
                    System.out.println("connectionLost: " + cause.getMessage());
                }

                public void messageArrived(String topic, MqttMessage message) {
                    System.out.println("------------------------------");
                    System.out.println("topic: " + topic);
                    System.out.println("Qos: " + message.getQos());
                    String payloadStr = new String(message.getPayload());
                    System.out.println("message content: " + payloadStr);

                    if (topic.contains("original_data")) {
                        JSONObject jsonObject = JSONObject.parseObject(payloadStr);
                        JSONArray sensorOriginalDataArray = jsonObject.getJSONObject("data").getJSONArray("dataList");
                        for (int x = 0; x < sensorOriginalDataArray.size(); x++) {
                            String sensorOriginalDataStr = sensorOriginalDataArray.getString(x);
                            SensorOriginalDataBean sensorOriginalDataBean = null;
                            if (ETAG_B_ORIGINAL_DATA_BLE4_LENGTH.equals(sensorOriginalDataStr.length())) {
                                sensorOriginalDataBean = sensorOriginalDataUtil
                                        .parseSensorOriginalDataBle4Str(sensorOriginalDataStr);
                            } else if (ETAG_B_ORIGINAL_DATA_BLE5_LENGTH
                                    .equals(sensorOriginalDataStr.length())) {
                                sensorOriginalDataBean = sensorOriginalDataUtil
                                        .parseSensorOriginalDataBle5Str(sensorOriginalDataStr);
                            }
                            System.out.println("original content: " + JSONObject.toJSONString(sensorOriginalDataBean));
                        }
                    }
                }

                public void deliveryComplete(IMqttDeliveryToken token) {
                    //System.out.println("deliveryComplete---------" + token.isComplete());
                }

            });
            client.connect(options);
            client.subscribe(topic, qos);
            /**
             client.subscribe("/shoptalk/+/gateway/command/exec_reply", 1);
             client.subscribe("/shoptalk/+/gateway/command/reply", 1);
             client.subscribe("/shoptalk/+/gateway/info", 1);
             client.subscribe("/shoptalk/+/gateway/gateway_firmware_update/reply", 1);
             client.subscribe("/shoptalk/+/gateway/heartbeat", 1);
             client.subscribe("/shoptalk/+/AoAServer/locatorsInfo", 1);
             client.subscribe("/shoptalk/+/label/original_data", 1);
             client.subscribe("/shoptalk/+/gateway/command/special_cmd_exec_reply", 1);
             client.subscribe("/shoptalk/+/pong", 1);
             **/
            List<String> gateways = new ArrayList<String>();
            gateways.add("602112200580");
            gateways.add("602112200558");
            gateways.add("602112200469");
            gateways.add("602112200630");
            gateways.add("602112200651");
            gateways.add("602112200640");
            gateways.add("602112200560");
            gateways.add("602112200659");
            gateways.add("602112200553");
            gateways.add("602112200554");
            gateways.add("602112200555");
            gateways.add("602112200656");
            gateways.add("602210100083");

            for (String gatewayMacAddr : gateways) {
                client.subscribe("/shoptalk/" + gatewayMacAddr + "/label/original_data", 1);

                client.subscribe("/shoptalk/" + gatewayMacAddr + "/gateway/command/exec_reply", 1);
                client.subscribe("/shoptalk/" + gatewayMacAddr + "/gateway/command/reply", 1);
                client.subscribe("/shoptalk/" + gatewayMacAddr + "/gateway/command", 1);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
