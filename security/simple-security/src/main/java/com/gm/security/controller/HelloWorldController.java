package com.gm.security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @GetMapping("/helloworld")
    public String getUser() {
        return "hello world";
    }

    @GetMapping("/all")
    public String all() {
        return "all";
    }

    @GetMapping("/admin")
    public String admin() {
        return "admin";
    }

}
