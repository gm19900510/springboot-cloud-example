package com.gm.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.sql.DataSource;
import java.util.UUID;

@Configuration
@EnableWebSecurity(debug = true)
public class SecurityConfig {

    @Autowired
    private DataSource dataSource;

    //todo 官网地址：https://docs.spring.io/spring-security/reference/servlet/authentication/

    // todo remember 二次校验：https://blog.csdn.net/MAKEJAVAMAN/article/details/121128346

    @Bean
    @Order
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http)
            throws Exception {

        HttpSessionRequestCache requestCache = new HttpSessionRequestCache();

        http.authorizeRequests(authorizeRequests -> authorizeRequests.antMatchers("/all").permitAll() // 登录未登录均可访问
                .antMatchers("/admin").hasRole("ADMIN")  // 有ADMIN角色的权限可访问/admin
                .anyRequest().authenticated()); // 任何请求都必须经过认证

        http.formLogin(form -> form.loginPage("/login").permitAll());// 自定义登录页面请求/login，需permitAll放开访问控制并配置对应的controller请求地址
        http.rememberMe(
                /*(remember) -> remember.tokenRepository(persistentTokenRepository())*/ // 基于持久化方式存储用户信息
                (remember) -> remember.rememberMeServices(rememberMeServices())
                /**
                 * 基于Cookie方式存储用户信息，如果用户修改了密码记住我功能将会自动失效
                 * 格式：username + ":" + expiryTime + ":" + Md5Hex(username + ":" + expiryTime + ":" + password + ":" + key)
                 * 使用BCryptPasswordEncoder加密算法，使用哈希和随机盐来加密密码，所以每次重启服务器使用Cookie方式+内存存储用户方式会造成remember失效
                 */

                /**
                 * 在Spring Security中，当你使用BCryptPasswordEncoder加密密码时，它会自动生成一个随机盐值并将盐值和密码一起进行bcrypt哈希。
                 * 当验证密码时，它会从存储的哈希值中提取盐值，然后使用输入的密码和提取的盐值来重新计算哈希值，并将其与存储的哈希值进行比较。如果两个哈希值匹配，密码验证通过。
                 */
        );

        //rememberMe: https://blog.csdn.net/J080624/article/details/131422239 、 https://blog.csdn.net/Littewood/article/details/125929938
        // https://blog.csdn.net/m0_56154713/article/details/127215242

        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        //这里用固定的用户，后续改成从数据库查询
        UserDetails user = User.withDefaultPasswordEncoder()
                .username("user").password("123").roles("USER").build();

        //新版本方式
        UserDetails admin = User.withUsername("admin").password("456").passwordEncoder(passwordEncoder()::encode).roles("USER", "ADMIN").build();
        return new InMemoryUserDetailsManager(new UserDetails[]{user, admin});
    }


    @Bean
    RememberMeServices rememberMeServices() {
        TokenBasedRememberMeServices rememberMe = new TokenBasedRememberMeServices("REMEMBER-ME", userDetailsService());
        rememberMe.setParameter("remember-me");// 自定义登录页面设置rememberMe的参数名称，此处与官网原登录页面一致
        return rememberMe;
    }

    // 指定数据库持久化
    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);
        //jdbcTokenRepository.setCreateTableOnStartup(true);  //启动创建表结构
        return jdbcTokenRepository;
    }

}
