package com.gm.seata.openfeign.service.impl;

import com.gm.seata.openfeign.entity.Order;
import com.gm.seata.openfeign.feign.AccountServiceApi;
import com.gm.seata.openfeign.feign.StorageServiceApi;
import com.gm.seata.openfeign.mapper.OrderMapper;
import com.gm.seata.openfeign.service.OrderService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    StorageServiceApi storageServiceApi;

    @Autowired
    AccountServiceApi accountServiceApi;

    @GlobalTransactional
    @Override
    public boolean createOrder(String userId, String commodityCode, Integer count) {

        String xid = RootContext.getXID();
        log.info("全局事务 xid：{}", xid);
        try {
            storageServiceApi.deduct(commodityCode, count);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        try {
           accountServiceApi.deduct(userId, new BigDecimal(count * 100.0));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        Order order = new Order();
        order.setCount(count);
        order.setCommodityCode(commodityCode);
        order.setUserId(userId);
        order.setMoney(new BigDecimal(count * 100.0));
        int i = orderMapper.insert(order);

        return i == 1;
    }
}
