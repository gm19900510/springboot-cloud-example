package com.gm.seata.openfeign.controller;

import com.gm.seata.openfeign.entity.Account;
import com.gm.seata.openfeign.service.AccountService;
import com.gm.seata.openfeign.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.math.BigDecimal;

@RestController
public class AccountController {

    @Autowired
    AccountService accountService;

    /**
     * 扣除账户余额
     *
     * @param userId
     * @param money
     * @return
     */
    @RequestMapping(value = "deduct", method = RequestMethod.GET)
    public R<Boolean> deduct(@RequestParam("userId") String userId, @RequestParam("money") BigDecimal money) {
        return R.ok(accountService.deduct(userId, money));
    }

    /**
     * 账户提现（用于演示事务隔离-脏写）
     *
     * @param userId
     * @param money
     * @return
     */
    @RequestMapping(value = "recharge", method = RequestMethod.GET)
    public R<Boolean> withdrawal(@RequestParam("userId") String userId, @RequestParam("money") BigDecimal money) {
        return R.ok(accountService.withdrawal(userId, money));
    }

    /**
     * 账户提现（用于演示事务隔离-@GlobalLock 防止脏写）
     *
     * @param userId
     * @param money
     * @return
     */
    @RequestMapping(value = "recharge/writeIsolationGlobalLock", method = RequestMethod.GET)
    public R<Boolean> writeIsolationGlobalLock(@RequestParam("userId") String userId, @RequestParam("money") BigDecimal money) {
        return R.ok(accountService.writeIsolationGlobalLock(userId, money));
    }

    /**
     * 账户提现（用于演示事务隔离-@GlobalTransactional 防止脏写）
     *
     * @param userId
     * @param money
     * @return
     */
    @RequestMapping(value = "recharge/writeIsolationGlobalTransactional", method = RequestMethod.GET)
    public R<Boolean> writeIsolationGlobalTransactional(@RequestParam("userId") String userId, @RequestParam("money") BigDecimal money) {
        return R.ok(accountService.writeIsolationGlobalTransactional(userId, money));
    }

    /**
     * 余额插洗吗（用于演示事务隔离-脏写）
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "query", method = RequestMethod.GET)
    public R<Account> query(@RequestParam("userId") String userId) {
        return R.ok(accountService.query(userId));
    }

    /**
     * 余额插洗吗（用于演示事务隔离-select for update + @GlobalLock 防止脏读）
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "query/readIsolationGlobalLock", method = RequestMethod.GET)
    public R<Account> readIsolationGlobalLock(@RequestParam("userId") String userId) {
        return R.ok(accountService.readIsolationGlobalLock(userId));
    }

    /**
     * 余额插洗吗（用于演示事务隔离-select for update + @GlobalTransactional 防止脏读）
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "query/readIsolationGlobalTransactional", method = RequestMethod.GET)
    public R<Account> readIsolationGlobalTransactional(@RequestParam("userId") String userId) {
        return R.ok(accountService.readIsolationGlobalTransactional(userId));
    }
}
