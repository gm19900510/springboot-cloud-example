package com.gm.seata.openfeign.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.seata.openfeign.entity.Account;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface AccountMapper extends BaseMapper<Account> {

    @Select("SELECT * FROM t_account WHERE user_id = #{userId} limit 1")
    Account getAccountByUserId(@Param("userId") String userId);


    @Select("SELECT * FROM t_account WHERE user_id = #{userId} for update")
    Account getAccountByUserIdForUpdate(@Param("userId") String userId);
}
