package com.gm.seata.openfeign.service;

import com.gm.seata.openfeign.entity.Account;
import java.math.BigDecimal;

public interface AccountService {

    /**
     * 扣除账户余额
     *
     * @param userId
     * @param money
     * @return
     */
    boolean deduct(String userId, BigDecimal money);


    /**
     * 账户提现（脏写示例）
     *
     * @param userId
     * @param money
     * @return
     */
    boolean withdrawal(String userId, BigDecimal money);

    /**
     * 账户提现利用GlobalLock防止出现脏写
     *
     * @param userId
     * @param money
     * @return
     */
    boolean writeIsolationGlobalLock(String userId, BigDecimal money);

    /**
     * 账户提现利用GlobalTransactional防止出现脏写
     *
     * @param userId
     * @param money
     * @return
     */
    boolean writeIsolationGlobalTransactional(String userId, BigDecimal money);

    /**
     * 账户查询利用GlobalLock防止出现脏读
     *
     * @param userId
     * @return
     */
    Account readIsolationGlobalLock(String userId);

    /**
     * 账户查询利用GlobalTransactional防止出现脏读
     *
     * @param userId
     * @return
     */
    Account readIsolationGlobalTransactional(String userId);

    /**
     * 账户查询（脏读示例）
     *
     * @param userId
     * @return
     */
    Account query(String userId);

}
