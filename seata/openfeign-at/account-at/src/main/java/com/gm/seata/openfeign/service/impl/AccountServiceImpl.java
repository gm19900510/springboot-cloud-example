package com.gm.seata.openfeign.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.openfeign.entity.Account;
import com.gm.seata.openfeign.mapper.AccountMapper;
import com.gm.seata.openfeign.service.AccountService;
import com.gm.seata.openfeign.util.ErrorEnum;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalLock;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    @Override
    public boolean deduct(String userId, BigDecimal money) {
        String xid = RootContext.getXID();
        log.info("全局事务 xid：{}", xid);

        Account account = accountMapper.getAccountByUserId(userId);
        if (account == null) {
            //throw new RuntimeException("账户不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_ACCOUNT.getCode()));
        }
        // 账户余额 与 本次消费金额进行 比较
        if (account.getMoney().compareTo(money) < 0) {
            //throw new RuntimeException("余额不足，预扣款失败");
            throw new RuntimeException(String.valueOf(ErrorEnum.ACCOUNT_LOW_PREPARE.getCode()));
        }
        account.setMoney(account.getMoney().subtract(money));

        QueryWrapper query = new QueryWrapper();
        query.eq("user_id", userId);
        int i = accountMapper.update(account, query);
        log.info("{} 账户余额扣除 {} 元", userId, money);

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (Math.random() < 0.5) {
            throw new RuntimeException(String.valueOf(ErrorEnum.UNKNOWN_EXCEPTION.getCode()));
        }
        return i == 1;
    }

    @Override
    public boolean withdrawal(String userId, BigDecimal money) {
        Account account = accountMapper.getAccountByUserId(userId);
        if (account == null) {
            //throw new RuntimeException("账户不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_ACCOUNT.getCode()));
        }
        // 账户余额 与 本次消费金额进行 比较
        if (account.getMoney().compareTo(money) < 0) {
            //throw new RuntimeException("余额不足，预扣款失败");
            throw new RuntimeException(String.valueOf(ErrorEnum.ACCOUNT_LOW_PREPARE.getCode()));
        }
        account.setMoney(account.getMoney().subtract(money));

        QueryWrapper query = new QueryWrapper();
        query.eq("user_id", userId);
        int i = accountMapper.update(account, query);
        log.info("{} 账户余额提现 {} 元", userId, money);
        return i == 1;
    }

    /**
     * 写隔离，通过GlobalLock方式
     *
     * @param userId
     * @param money
     * @return
     */
    @GlobalLock(lockRetryInterval = 1000, lockRetryTimes = 30)
    @Override
    public boolean writeIsolationGlobalLock(String userId, BigDecimal money) {
        Account account = accountMapper.getAccountByUserIdForUpdate(userId);
        if (account == null) {
            //throw new RuntimeException("账户不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_ACCOUNT.getCode()));
        }
        // 账户余额 与 本次消费金额进行 比较
        if (account.getMoney().compareTo(money) < 0) {
            //throw new RuntimeException("余额不足，预扣款失败");
            throw new RuntimeException(String.valueOf(ErrorEnum.ACCOUNT_LOW_PREPARE.getCode()));
        }
        account.setMoney(account.getMoney().subtract(money));

        QueryWrapper query = new QueryWrapper();
        query.eq("user_id", userId);
        int i = accountMapper.update(account, query);
        log.info("{} 账户余额扣除 {} 元", userId, money);
        return i == 1;
    }

    /**
     * 写隔离，通过GlobalTransactional方式
     *
     * @param userId
     * @param money
     * @return
     */
    @GlobalTransactional
    @Override
    public boolean writeIsolationGlobalTransactional(String userId, BigDecimal money) {
        Account account = accountMapper.getAccountByUserIdForUpdate(userId);
        if (account == null) {
            //throw new RuntimeException("账户不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_ACCOUNT.getCode()));
        }
        // 账户余额 与 本次消费金额进行 比较
        if (account.getMoney().compareTo(money) < 0) {
            //throw new RuntimeException("余额不足，预扣款失败");
            throw new RuntimeException(String.valueOf(ErrorEnum.ACCOUNT_LOW_PREPARE.getCode()));
        }
        account.setMoney(account.getMoney().subtract(money));

        QueryWrapper query = new QueryWrapper();
        query.eq("user_id", userId);
        int i = accountMapper.update(account, query);
        log.info("{} 账户余额扣除 {} 元", userId, money);
        return i == 1;
    }

    @GlobalLock(lockRetryInterval = 1000, lockRetryTimes = 30)
    @Override
    public Account readIsolationGlobalLock(String userId) {
        Account account = accountMapper.getAccountByUserIdForUpdate(userId);
        if (account == null) {
            //throw new RuntimeException("账户不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_ACCOUNT.getCode()));
        }
        return account;
    }

    @GlobalTransactional(lockRetryInterval = 1000, lockRetryTimes = 30)
    @Override
    public Account readIsolationGlobalTransactional(String userId) {
        Account account = accountMapper.getAccountByUserIdForUpdate(userId);
        if (account == null) {
            //throw new RuntimeException("账户不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_ACCOUNT.getCode()));
        }
        return account;
    }

    @Override
    public Account query(String userId) {
        Account account = accountMapper.getAccountByUserId(userId);
        if (account == null) {
            //throw new RuntimeException("账户不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_ACCOUNT.getCode()));
        }
        return account;
    }
}
