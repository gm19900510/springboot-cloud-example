package com.gm.seata.openfeign.feign;

import com.gm.seata.openfeign.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

@FeignClient(value = "account-at")
public interface AccountServiceApi {

    /**
     * 扣除账户余额
     *
     * @param userId
     * @param money
     * @return
     */
    @RequestMapping(value = "deduct", method = RequestMethod.GET)
    R<Boolean> deduct(@RequestParam("userId") String userId, @RequestParam("money") BigDecimal money);
}
