package com.gm.seata.openfeign.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("t_storage")
public class Storage {

    @TableId(type = IdType.ASSIGN_ID)
    private long id;

    private String commodityCode;

    private int count;
}
