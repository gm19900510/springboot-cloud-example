package com.gm.seata.openfeign;

import com.gm.seata.openfeign.entity.Order;
import com.gm.seata.openfeign.mapper.OrderMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

@SpringBootTest
public class OrderShardingDatabaseATApplicationTests {

    @Autowired
    OrderMapper orderMapper;

    @Test
    void addOrder() {
        for (int i = 1; i <= 10; i++) {
            Order shareOrder = new Order();
            shareOrder.setMoney(new BigDecimal(200));
            shareOrder.setCount(2);
            shareOrder.setCommodityCode("iphone");
            shareOrder.setUserId("user1");
            orderMapper.insert(shareOrder);
        }
    }
}
