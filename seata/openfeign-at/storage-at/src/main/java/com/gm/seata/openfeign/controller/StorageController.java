package com.gm.seata.openfeign.controller;

import com.gm.seata.openfeign.service.StorageService;
import com.gm.seata.openfeign.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StorageController {

    @Autowired
    StorageService storageService;

    /**
     * 扣除商品库存
     *
     * @param count
     * @return
     */
    @RequestMapping(value = "deduct", method = RequestMethod.GET)
    public R<Boolean> deduct(@RequestParam("commodityCode") String commodityCode, @RequestParam("count") Integer count) {
        return R.ok(storageService.deduct(commodityCode, count));
    }
}
