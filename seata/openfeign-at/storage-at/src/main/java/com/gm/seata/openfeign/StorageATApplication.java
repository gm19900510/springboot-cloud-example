package com.gm.seata.openfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class StorageATApplication {

    public static void main(String[] args) {
        SpringApplication.run(StorageATApplication.class, args);
    }
}
