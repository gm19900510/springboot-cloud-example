package com.gm.seata.openfeign.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.openfeign.entity.Order;
import com.gm.seata.openfeign.mapper.OrderMapper;
import com.gm.seata.openfeign.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;

@Slf4j
@Service(value = "orderService")
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderMapper orderMapper;

    @Override
    public boolean createOrder(String businessKey, String userId, String commodityCode, Integer count) {
        log.info("saga状态机调用OrderService的createOrder方法，参数businessKey={},userId={},commodityCode={},count={}", businessKey, userId, commodityCode, count);

        Order order = new Order();
        order.setCount(count);
        order.setCommodityCode(commodityCode);
        order.setUserId(userId);
        order.setMoney(new BigDecimal(count * 100.0));
        int i = orderMapper.insert(order);

        log.info("{} 用户购买的 {} 商品共计 {} 件，订单创建成功", userId, commodityCode, count);
        return i == 1;
    }

    @Override
    public boolean compensateOrder(String businessKey, String userId, String commodityCode, Integer count) {
        log.info("saga状态机调用OrderService的compensateOrder方法，参数businessKey={},userId={},commodityCode={},count={}", businessKey, userId, commodityCode, count);

        QueryWrapper query = new QueryWrapper();
        query.eq("commodity_code", commodityCode);
        query.eq("userId", commodityCode);
        query.eq("Money", new BigDecimal(count * 100.0));
        int i = orderMapper.delete(query);

        log.info("{} 用户购买的 {} 商品共计 {} 件，订单补充（移除）成功", userId, commodityCode, count);
        return i == 1;
    }
}
