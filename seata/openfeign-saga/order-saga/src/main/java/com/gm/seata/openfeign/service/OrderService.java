package com.gm.seata.openfeign.service;

public interface OrderService {

    /**
     * 创建订单
     *
     * @param businessKey
     * @param userId
     * @param commodityCode
     * @param count
     * @return
     */
    boolean createOrder(String businessKey, String userId, String commodityCode, Integer count);

    /**
     * 补偿（移除）已创建订单
     *
     * @param businessKey
     * @param userId
     * @param commodityCode
     * @param count
     * @return
     */
    boolean compensateOrder(String businessKey, String userId, String commodityCode, Integer count);
}
