package com.gm.seata.openfeign.service;

public interface AccountService {

    /**
     * 扣除账户余额
     *
     * @param businessKey
     * @param userId
     * @param commodityCode
     * @param count
     * @return
     */
    boolean deduct(String businessKey, String userId, String commodityCode, Integer count);

    /**
     * 补偿账户余额
     *
     * @param businessKey
     * @param userId
     * @param commodityCode
     * @param count
     * @return
     */
    boolean compensateDeduct(String businessKey, String userId, String commodityCode, Integer count);
}
