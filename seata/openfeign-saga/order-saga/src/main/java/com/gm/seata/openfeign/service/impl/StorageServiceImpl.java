package com.gm.seata.openfeign.service.impl;

import com.gm.seata.openfeign.feign.StorageServiceApi;
import com.gm.seata.openfeign.service.StorageService;
import com.gm.seata.openfeign.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service(value = "storageService")
public class StorageServiceImpl implements StorageService {

    @Autowired
    StorageServiceApi storageServiceApi;

    @Override
    public boolean deduct(String businessKey, String userId, String commodityCode, Integer count) {
        log.info("saga状态机调用StorageService的deduct方法，参数businessKey={},userId={},commodityCode={},count={}", businessKey, userId, commodityCode, count);
        try {
            R<Boolean> result = storageServiceApi.deduct(businessKey, commodityCode, count);
            return result.getData();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public boolean compensateDeduct(String businessKey, String userId, String commodityCode, Integer count) {
        log.info("saga状态机调用StorageService的compensateDeduct方法，参数businessKey={},userId={},commodityCode={},count={}", businessKey, userId, commodityCode, count);
        try {
            R<Boolean> result = storageServiceApi.compensateDeduct(businessKey, commodityCode, count);
            return result.getData();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
