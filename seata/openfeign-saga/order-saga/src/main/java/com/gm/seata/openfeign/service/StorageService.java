package com.gm.seata.openfeign.service;

public interface StorageService {

    /**
     * 扣除库存
     *
     * @param businessKey
     * @param userId
     * @param commodityCode
     * @param count
     * @return
     */
    boolean deduct(String businessKey, String userId, String commodityCode, Integer count);

    /**
     * 补充扣除库存
     *
     * @param businessKey
     * @param userId
     * @param commodityCode
     * @param count
     * @return
     */
    boolean compensateDeduct(String businessKey, String userId, String commodityCode, Integer count);
}
