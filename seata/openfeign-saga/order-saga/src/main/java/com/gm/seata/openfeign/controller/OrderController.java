package com.gm.seata.openfeign.controller;

import com.gm.seata.openfeign.util.ErrorEnum;
import com.gm.seata.openfeign.util.R;
import io.seata.saga.engine.StateMachineEngine;
import io.seata.saga.statelang.domain.ExecutionStatus;
import io.seata.saga.statelang.domain.StateMachineInstance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
public class OrderController {

    @Autowired
    StateMachineEngine stateMachineEngine;

    /**
     * 商品下单购买
     *
     * @param userId
     * @param commodityCode
     * @param count
     * @return
     */
    @RequestMapping(value = "buy", method = RequestMethod.GET)
    public R<String> buy(@RequestParam("userId") String userId, @RequestParam("commodityCode") String commodityCode, @RequestParam("count") Integer count) {
        //唯一健
        String businessKey = String.valueOf(System.currentTimeMillis());
        Map<String, Object> startParams = new HashMap<>();
        startParams.put("businessKey", businessKey);
        startParams.put("userId", userId);
        startParams.put("commodityCode", commodityCode);
        startParams.put("count", count);

        //同步执行
        StateMachineInstance inst = stateMachineEngine.startWithBusinessKey("order", null, businessKey, startParams);

        if (ExecutionStatus.SU.equals(inst.getStatus())) {
            log.info("成功,saga transaction execute Succeed. XID: {},Status: {},CompensationStatus: {}", inst.getId(), inst.getStatus(), inst.getCompensationStatus());
            return R.ok("下单成功");
        } else {
            log.info("成功,saga transaction execute Succeed. XID: {},Status: {},CompensationStatus: {}", inst.getId(), inst.getStatus(), inst.getCompensationStatus());
            try {
                int code = Integer.parseInt(inst.getException().getMessage());
                return R.restResult("下单失败", code, ErrorEnum.getEnumByCode(code).getTitle());
            } catch (Exception e) {
                return R.failed("下单失败", inst.getException().getMessage());
            }
        }
    }
}
