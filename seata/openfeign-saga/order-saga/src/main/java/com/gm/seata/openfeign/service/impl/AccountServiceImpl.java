package com.gm.seata.openfeign.service.impl;

import com.gm.seata.openfeign.feign.AccountServiceApi;
import com.gm.seata.openfeign.service.AccountService;
import com.gm.seata.openfeign.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;

@Slf4j
@Service(value = "accountService")
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountServiceApi accountServiceApi;

    @Override
    public boolean deduct(String businessKey, String userId, String commodityCode, Integer count) {
        log.info("saga状态机调用AccountService的deduct方法，参数businessKey={},userId={},commodityCode={},count={}", businessKey, userId, commodityCode, count);
        try {
            R<Boolean> result = accountServiceApi.deduct(businessKey, userId, new BigDecimal(count * 100.0));
            return result.getData();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public boolean compensateDeduct(String businessKey, String userId, String commodityCode, Integer count) {
        log.info("saga状态机调用AccountService的compensateDeduct方法，参数businessKey={},userId={},commodityCode={},count={}", businessKey, userId, commodityCode, count);
        try {
            R<Boolean> result = accountServiceApi.compensateDeduct(businessKey, userId, new BigDecimal(count * 100.0));
            return result.getData();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
