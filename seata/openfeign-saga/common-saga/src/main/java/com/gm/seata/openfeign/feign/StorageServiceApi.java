package com.gm.seata.openfeign.feign;

import com.gm.seata.openfeign.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "storage-saga")
public interface StorageServiceApi {

    /**
     * 扣除库存
     *
     * @param commodityCode
     * @param count
     * @return
     */
    @RequestMapping(value = "deduct", method = RequestMethod.GET)
    R<Boolean> deduct(@RequestParam("businessKey") String businessKey, @RequestParam("commodityCode") String commodityCode, @RequestParam("count") Integer count);

    /**
     * 补偿扣除的库存
     *
     * @param commodityCode
     * @param count
     * @return
     */
    @RequestMapping(value = "compensateDeduct", method = RequestMethod.GET)
    R<Boolean> compensateDeduct(@RequestParam("businessKey") String businessKey, @RequestParam("commodityCode") String commodityCode, @RequestParam("count") Integer count);
}
