package com.gm.seata.openfeign.handle;

import com.alibaba.fastjson.JSONObject;
import com.gm.seata.openfeign.util.ErrorEnum;
import feign.Response;
import feign.RetryableException;
import feign.Util;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import java.nio.charset.Charset;

@Slf4j
@Configuration
public class FeignErrorDecoder extends ErrorDecoder.Default {

    @Override
    public Exception decode(String methodKey, Response response) {
        try {
            // 可以自定义一些逻辑
            String message = Util.toString(response.body().asReader(Charset.forName("utf8")));
            JSONObject jsonObject = JSONObject.parseObject(message);
            int code = jsonObject.getInteger("code");
            ErrorEnum errorEnum = ErrorEnum.getEnumByCode(code);
            // 包装成自己自定义的异常
            return new RuntimeException(String.valueOf(errorEnum.getCode()));
        } catch (Exception e) {
            log.error("非已知异常", e.getMessage(), e);
        }

        Exception exception = super.decode(methodKey, response);
        // 如果是RetryableException，则返回继续重试
        if (exception instanceof RetryableException) {
            return exception;
        }
        return new RuntimeException(String.valueOf(ErrorEnum.UNKNOWN_EXCEPTION.getCode()));
    }
}
