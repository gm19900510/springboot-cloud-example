package com.gm.seata.openfeign.service;

import java.math.BigDecimal;

public interface AccountService {

    /**
     * 扣除账户余额
     *
     * @param userId
     * @param money
     * @return
     */
    boolean deduct(String businessKey, String userId, BigDecimal money);

    /**
     * 补充扣除的账户余额
     *
     * @param userId
     * @param money
     * @return
     */
    boolean compensateDeduct(String businessKey, String userId, BigDecimal money);
}
