package com.gm.seata.openfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class AccountSagaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountSagaApplication.class, args);
    }
}
