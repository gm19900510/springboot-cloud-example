package com.gm.seata.openfeign.controller;

import com.gm.seata.openfeign.service.AccountService;
import com.gm.seata.openfeign.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.math.BigDecimal;

@RestController
public class AccountController {

    @Autowired
    AccountService accountService;

    /**
     * 扣除账户余额
     *
     * @param userId
     * @param money
     * @return
     */
    @RequestMapping(value = "deduct", method = RequestMethod.GET)
    public R<Boolean> deduct(@RequestParam("businessKey") String businessKey, @RequestParam("userId") String userId, @RequestParam("money") BigDecimal money) {
        return R.ok(accountService.deduct(businessKey, userId, money));
    }

    /**
     * 补偿扣除的账户余额
     *
     * @param userId
     * @param money
     * @return
     */
    @RequestMapping(value = "compensateDeduct", method = RequestMethod.GET)
    public R<Boolean> compensateDeduct(@RequestParam("businessKey") String businessKey, @RequestParam("userId") String userId, @RequestParam("money") BigDecimal money) {
        return R.ok(accountService.compensateDeduct(businessKey, userId, money));
    }
}
