package com.gm.seata.openfeign.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.openfeign.entity.Account;
import com.gm.seata.openfeign.mapper.AccountMapper;
import com.gm.seata.openfeign.service.AccountService;
import com.gm.seata.openfeign.util.ErrorEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    @Override
    public boolean deduct(String businessKey, String userId, BigDecimal money) {
        Account account = accountMapper.getAccountByUserId(userId);
        if (account == null) {
            //throw new RuntimeException("账户不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_ACCOUNT.getCode()));
        }
        // 账户余额 与 本次消费金额进行 比较
        if (account.getMoney().compareTo(money) < 0) {
            //throw new RuntimeException("余额不足，预扣款失败");
            throw new RuntimeException(String.valueOf(ErrorEnum.ACCOUNT_LOW_PREPARE.getCode()));
        }
        account.setMoney(account.getMoney().subtract(money));
        account.setBusinessKey(businessKey);

        QueryWrapper query = new QueryWrapper();
        query.eq("user_id", userId);
        int i = accountMapper.update(account, query);
        log.info("{} 账户余额扣除 {} 元", userId, money);
        return i == 1;
    }

    @Override
    public boolean compensateDeduct(String businessKey, String userId, BigDecimal money) {
        Account account = accountMapper.getAccountByUserId(userId);

        account.setMoney(account.getMoney().add(money));

        QueryWrapper query = new QueryWrapper();
        query.eq("user_id", userId);
        query.eq("business_key", businessKey);
        int i = accountMapper.update(account, query);
        if (i == 1) {
            log.info("{} 账户补偿余额 {} 元", userId, money);
        } else {
            log.info("{} 账户无需补偿", userId);
        }
        return i == 1;
    }
}
