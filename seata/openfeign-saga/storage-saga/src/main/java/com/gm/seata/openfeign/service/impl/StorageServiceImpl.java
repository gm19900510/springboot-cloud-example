package com.gm.seata.openfeign.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.openfeign.entity.Storage;
import com.gm.seata.openfeign.mapper.StorageMapper;
import com.gm.seata.openfeign.service.StorageService;
import com.gm.seata.openfeign.util.ErrorEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class StorageServiceImpl implements StorageService {

    @Autowired
    StorageMapper storageMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deduct(String businessKey, String commodityCode, Integer count) {
        Storage storage = storageMapper.getStorageByCommodityCode(commodityCode);
        if (storage == null) {
            //throw new RuntimeException("商品不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_COMMODITY.getCode()));
        }
        if (storage.getCount() < count) {
            //throw new RuntimeException("库存不足，预扣库存失败");
            throw new RuntimeException(String.valueOf(ErrorEnum.STORAGE_LOW_PREPARE.getCode()));
        }
        storage.setCount(storage.getCount() - count);
        storage.setBusinessKey(businessKey);

        QueryWrapper query = new QueryWrapper();
        query.eq("commodity_code", commodityCode);
        Integer i = storageMapper.update(storage, query);
        log.info("{} 商品库存扣除 {} 个", commodityCode, count);
        return i == 1;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean compensateDeduct(String businessKey, String commodityCode, Integer count) {
        Storage storage = storageMapper.getStorageByCommodityCode(commodityCode);
        storage.setCount(storage.getCount() + count);

        QueryWrapper query = new QueryWrapper();
        query.eq("commodity_code", commodityCode);
        query.eq("business_key", businessKey);
        Integer i = storageMapper.update(storage, query);
        log.info("{} 商品补偿库存 {} 个", commodityCode, count);
        return i == 1;
    }
}
