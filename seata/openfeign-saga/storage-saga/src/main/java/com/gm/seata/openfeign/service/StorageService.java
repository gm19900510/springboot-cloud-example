package com.gm.seata.openfeign.service;

public interface StorageService {

    /**
     * 扣除库存
     *
     * @param commodityCode
     * @param count
     * @return
     */
    boolean deduct(String businessKey, String commodityCode, Integer count);

    /**
     * 补充扣除库存
     *
     * @param commodityCode
     * @param count
     * @return
     */
    boolean compensateDeduct(String businessKey, String commodityCode, Integer count);
}
