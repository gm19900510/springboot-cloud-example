package com.gm.seata.openfeign.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.openfeign.entity.Account;
import com.gm.seata.openfeign.mapper.AccountMapper;
import com.gm.seata.openfeign.service.AccountService;
import com.gm.seata.openfeign.util.ErrorEnum;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    /**
     * 预扣款阶段，检查账户余额
     *
     * @param userId
     * @param money
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean prepare(BusinessActionContext actionContext, String userId, BigDecimal money) {
        Account account = accountMapper.getAccountByUserId(userId);
        if (account == null) {
            //throw new RuntimeException("账户不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_ACCOUNT.getCode()));
        }
        // 账户余额 与 本次消费金额进行 比较
        if (account.getMoney().compareTo(money) < 0) {
            //throw new RuntimeException("余额不足，预扣款失败");
            throw new RuntimeException(String.valueOf(ErrorEnum.ACCOUNT_LOW_PREPARE.getCode()));
        }
        account.setFreezeMoney(account.getFreezeMoney().add(money));
        account.setMoney(account.getMoney().subtract(money));

        QueryWrapper query = new QueryWrapper();
        query.eq("user_id", userId);
        Integer i = accountMapper.update(account, query);
        log.info("{} 账户预扣款 {} 元", userId, money);
        return i == 1;
    }

    /**
     * 实际扣款阶段
     *
     * @param actionContext
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean commit(BusinessActionContext actionContext) {
        String userId = (String) actionContext.getActionContext("userId");
        BigDecimal money = new BigDecimal(actionContext.getActionContext("money").toString());

        Account account = accountMapper.getAccountByUserId(userId);
        // 账户冻结金额 与 本次消费金额进行 比较
        if (account.getFreezeMoney().compareTo(money) < 0) {
            // 抛出指定异常
            throw new RuntimeException(String.valueOf(ErrorEnum.ACCOUNT_LOW_COMMIT.getCode()));
        }
        account.setFreezeMoney(account.getFreezeMoney().subtract(money));

        QueryWrapper query = new QueryWrapper();
        query.eq("user_id", userId);
        Integer i = accountMapper.update(account, query);
        log.info("{} 账户扣款 {} 元", userId, money);
        return i == 1;
    }

    /**
     * 账户回滚阶段
     *
     * @param actionContext
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean rollback(BusinessActionContext actionContext) {
        String userId = (String) actionContext.getActionContext("userId");
        BigDecimal money = new BigDecimal(actionContext.getActionContext("money").toString());

        Account account = accountMapper.getAccountByUserId(userId);

        if (account.getFreezeMoney().compareTo(money) >= 0) {
            account.setFreezeMoney(account.getFreezeMoney().subtract(money));
            account.setMoney(account.getMoney().add(money));

            QueryWrapper query = new QueryWrapper();
            query.eq("user_id", userId);
            Integer i = accountMapper.update(account, query);

            log.info("{} 账户释放冻结金额 {} 元", userId, money);
            return i == 1;
        }
        log.info("{} 账户资金已释放", userId);
        // 说明prepare中抛出异常，未冻结资金
        return true;
    }
}
