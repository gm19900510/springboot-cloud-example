package com.gm.seata.openfeign.feign;

import com.gm.seata.openfeign.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "order-tcc")
public interface OrderServiceApi {

    /**
     * 创建订单
     * @param userId
     * @param commodityCode
     * @param count
     * @return
     */
    @RequestMapping(value = "createOrder", method = RequestMethod.GET)
    R<Boolean> createOrder(@RequestParam("userId") String userId, @RequestParam("commodityCode") String commodityCode, @RequestParam("count") Integer count);
}
