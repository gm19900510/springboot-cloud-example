package com.gm.seata.openfeign.handle;

import com.gm.seata.openfeign.util.ErrorEnum;
import com.gm.seata.openfeign.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理器
 */
@Slf4j
@Order(10000)
@RestControllerAdvice
public class GlobalBizExceptionHandler {

    /**
     * 全局异常.
     *
     * @param e the e
     * @return R
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public R handleGlobalException(Exception e) {
        log.error("全局异常信息 ex={}", e.getMessage(), e);
        R r = null;
        // 根据异常信息与已知异常进行匹配
        try {
            int code = Integer.parseInt(e.getLocalizedMessage());
            ErrorEnum errorEnum = ErrorEnum.getEnumByCode(code);
            if (errorEnum != null) {
                r = R.restResult(null, errorEnum.getCode(), errorEnum.getTitle());
            }
        } finally {
            if (e instanceof feign.FeignException) {
                ErrorEnum errorEnum = ErrorEnum.UNKNOWN_EXCEPTION;
                r = R.restResult(null, errorEnum.getCode(), errorEnum.getTitle());
            }
            if (r == null) {
                r = R.failed(e.getLocalizedMessage());
            }
        }
        return r;
    }
}
