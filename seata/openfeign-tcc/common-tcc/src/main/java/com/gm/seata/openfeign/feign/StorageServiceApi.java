package com.gm.seata.openfeign.feign;

import com.gm.seata.openfeign.entity.Storage;
import com.gm.seata.openfeign.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "storage-tcc")
public interface StorageServiceApi {

    /**
     * 扣除库存
     * @param commodityCode
     * @param count
     * @return
     */
    @RequestMapping(value = "deduct", method = RequestMethod.GET)
    R<Boolean> deduct(@RequestParam("commodityCode") String commodityCode, @RequestParam("count") Integer count);
}
