package com.gm.seata.openfeign.service;

public interface BusinessService {
    void buy(String userId, String commodityCode, Integer count);
}
