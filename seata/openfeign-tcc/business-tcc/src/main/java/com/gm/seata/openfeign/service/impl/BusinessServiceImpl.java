package com.gm.seata.openfeign.service.impl;

import com.gm.seata.openfeign.feign.OrderServiceApi;
import com.gm.seata.openfeign.feign.StorageServiceApi;
import com.gm.seata.openfeign.service.BusinessService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class BusinessServiceImpl implements BusinessService {

    @Autowired
    StorageServiceApi storageServiceApi;
    @Autowired
    OrderServiceApi orderServiceApi;

    /**
     * 下单购买，先扣除库存再创建订单
     *
     * @param userId
     * @param commodityCode
     * @param count
     */
    @GlobalTransactional
    public void buy(String userId, String commodityCode, Integer count) {
        String xid = RootContext.getXID();
        log.info("xid={}", xid);

        /**
         * 扣除库存
         */
        // 只有抛出异常，才能触发GlobalTransactional 的回滚逻辑处理
        try {
            storageServiceApi.deduct(commodityCode, count);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        /**
         * 创建订单
         */
        try {
            orderServiceApi.createOrder(userId, commodityCode, count);
        } catch (Exception e) {
            // 远程方法调用失败
            throw new RuntimeException(e.getMessage());
        }
    }
}
