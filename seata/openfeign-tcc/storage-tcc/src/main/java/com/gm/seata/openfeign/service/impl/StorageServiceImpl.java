package com.gm.seata.openfeign.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.openfeign.entity.Storage;
import com.gm.seata.openfeign.mapper.StorageMapper;
import com.gm.seata.openfeign.service.StorageService;
import com.gm.seata.openfeign.util.ErrorEnum;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class StorageServiceImpl implements StorageService {

    @Autowired
    StorageMapper storageMapper;

    /**
     * 扣除商品库存预处理阶段，进行商品库存冻结
     *
     * @param commodityCode
     * @param count
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean prepare(BusinessActionContext actionContext, String commodityCode, Integer count) {
        Storage storage = storageMapper.getStorageByCommodityCode(commodityCode);
        if (storage == null) {
            //throw new RuntimeException("商品不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_COMMODITY.getCode()));
        }
        if (storage.getCount() < count) {
            //throw new RuntimeException("库存不足，预扣库存失败");
            throw new RuntimeException(String.valueOf(ErrorEnum.STORAGE_LOW_PREPARE.getCode()));
        }
        storage.setFreezeCount(storage.getFreezeCount() + count);
        storage.setCount(storage.getCount() - count);

        QueryWrapper query = new QueryWrapper();
        query.eq("commodity_code", commodityCode);
        Integer i = storageMapper.update(storage, query);
        log.info("{} 商品库存冻结 {} 个", commodityCode, count);
        return i ==1;
    }


    /**
     * 扣除商品库存提交阶段，进行商品库存扣除
     *
     * @param actionContext
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean commit(BusinessActionContext actionContext) {
        String commodityCode = (String) actionContext.getActionContext("commodityCode");
        Integer count = (Integer) actionContext.getActionContext("count");

        Storage storage = storageMapper.getStorageByCommodityCode(commodityCode);
        if (storage.getFreezeCount() < count) {
            //throw new RuntimeException("库存不足，扣库存失败");
            throw new RuntimeException(String.valueOf(ErrorEnum.STORAGE_LOW_COMMIT.getCode()));
        }
        storage.setFreezeCount(storage.getFreezeCount() - count);

        QueryWrapper query = new QueryWrapper();
        query.eq("commodity_code", commodityCode);
        int i = storageMapper.update(storage, query);
        log.info("{} 商品库存扣除 {} 个", commodityCode, count);
        return i == 1;
    }


    /**
     * 扣除商品库存回滚阶段
     *
     * @param actionContext
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean rollback(BusinessActionContext actionContext) {
        String commodityCode = (String) actionContext.getActionContext("commodityCode");
        Integer count = (Integer) actionContext.getActionContext("count");
        Storage storage = storageMapper.getStorageByCommodityCode(commodityCode);
        if (storage.getFreezeCount() >= count) {
            storage.setFreezeCount(storage.getFreezeCount() - count);
            storage.setCount(storage.getCount() + count);

            QueryWrapper query = new QueryWrapper();
            query.eq("commodity_code", commodityCode);
            int i = storageMapper.update(storage, query);
            log.info("{} 商品释放库存 {} 个", commodityCode, count);
            return i == 1;
        }
        // 说明 prepare 阶段就没有冻结
        return true;
    }
}
