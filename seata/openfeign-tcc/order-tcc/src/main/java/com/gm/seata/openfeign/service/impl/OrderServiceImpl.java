package com.gm.seata.openfeign.service.impl;

import com.gm.seata.openfeign.entity.Order;
import com.gm.seata.openfeign.feign.AccountServiceApi;
import com.gm.seata.openfeign.mapper.OrderMapper;
import com.gm.seata.openfeign.service.OrderService;
import com.gm.seata.openfeign.util.R;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    AccountServiceApi accountServiceApi;

    /**
     * 创建商品订单预处理阶段，扣除账户余额
     *
     * @param commodityCode
     * @param count
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean prepare(BusinessActionContext actionContext, String userId, String commodityCode, Integer count) {
        //先去扣款，假设每个产品100块钱
        R<Boolean> result = null;
        try {
            result = accountServiceApi.deduct(userId, new BigDecimal(count * 100.0));
        } catch (Exception e) {
            e.printStackTrace();
            // 远程方法调用失败
            throw new RuntimeException(e.getMessage());
        }

        log.info("{} 用户购买的 {} 商品共计 {} 件，预下单成功", userId, commodityCode, count);

        return result.getData();
    }

    /**
     * 创建商品订单提交阶段，创建订单记录
     *
     * @param actionContext
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean commit(BusinessActionContext actionContext) {
        String userId = (String) actionContext.getActionContext("userId");
        String commodityCode = (String) actionContext.getActionContext("commodityCode");
        Integer count = (Integer) actionContext.getActionContext("count");

        Order order = new Order();
        order.setCount(count);
        order.setCommodityCode(commodityCode);
        order.setUserId(userId);
        order.setMoney(new BigDecimal(count * 100.0));

        int i = orderMapper.insert(order);
        log.info("{} 用户购买的 {} 商品共计 {} 件，下单成功", userId, commodityCode, count);
        return i == 1;

    }

    /**
     * 创建商品订单回滚阶段，暂无业务操作
     *
     * @param actionContext
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean rollback(BusinessActionContext actionContext) {
        String userId = (String) actionContext.getActionContext("userId");
        String commodityCode = (String) actionContext.getActionContext("commodityCode");
        Integer count = (Integer) actionContext.getActionContext("count");
        log.info("{} 用户购买的 {} 商品共计 {} 件，订单回滚成功", userId, commodityCode, count);
        return true;
    }
}
