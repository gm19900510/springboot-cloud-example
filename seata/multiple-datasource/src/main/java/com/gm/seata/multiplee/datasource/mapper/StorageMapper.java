package com.gm.seata.multiplee.datasource.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.seata.multiplee.datasource.entity.Storage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface StorageMapper extends BaseMapper<Storage> {

    @Select("SELECT * FROM t_storage WHERE commodity_code = #{commodityCode} limit 1")
    Storage getStorageByCommodityCode(@Param("commodityCode") String commodityCode);
}
