package com.gm.seata.multiplee.datasource.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("t_account")
public class Account {

    @TableId(type = IdType.ASSIGN_ID)
    private long id;

    private String userId;

    private BigDecimal money;
}
