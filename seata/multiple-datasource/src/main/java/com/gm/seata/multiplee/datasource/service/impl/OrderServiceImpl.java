package com.gm.seata.multiplee.datasource.service.impl;

import com.gm.seata.multiplee.datasource.entity.Order;
import com.gm.seata.multiplee.datasource.mapper.OrderMapper;
import com.gm.seata.multiplee.datasource.service.AccountService;
import com.gm.seata.multiplee.datasource.service.OrderService;
import com.gm.seata.multiplee.datasource.service.StorageService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    StorageService storageService;

    @Autowired
    AccountService accountService;

    @GlobalTransactional
    @Override
    public boolean createOrder(String userId, String commodityCode, Integer count) {

        String xid = RootContext.getXID();
        log.info("全局事务 xid：{}", xid);
        try {
            storageService.deduct(commodityCode, count);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        try {
           accountService.deduct(userId, new BigDecimal(count * 100.0));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        Order order = new Order();
        order.setCount(count);
        order.setCommodityCode(commodityCode);
        order.setUserId(userId);
        order.setMoney(new BigDecimal(count * 100.0));
        int i = orderMapper.insert(order);

        return i == 1;
    }
}
