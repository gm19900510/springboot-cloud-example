package com.gm.seata.multiplee.datasource.service;

import java.math.BigDecimal;

public interface AccountService {

    /**
     * 扣除账户余额
     *
     * @param userId
     * @param money
     * @return
     */
    boolean deduct(String userId, BigDecimal money);
}
