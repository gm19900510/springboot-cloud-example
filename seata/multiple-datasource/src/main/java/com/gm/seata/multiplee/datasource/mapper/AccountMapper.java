package com.gm.seata.multiplee.datasource.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.seata.multiplee.datasource.entity.Account;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface AccountMapper extends BaseMapper<Account> {

    @Select("SELECT * FROM t_account WHERE user_id = #{userId} limit 1")
    Account getAccountByUserId(@Param("userId") String userId);
}
