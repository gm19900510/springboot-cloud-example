package com.gm.seata.multiplee.datasource.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.multiplee.datasource.entity.Account;
import com.gm.seata.multiplee.datasource.mapper.AccountMapper;
import com.gm.seata.multiplee.datasource.service.AccountService;
import com.gm.seata.multiplee.datasource.util.ErrorEnum;
import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    // @DS注解切换数据源必须要在@Transaction之前执行
    @Override
    @DS("account")
    @Transactional(rollbackFor = Exception.class)
    public boolean deduct(String userId, BigDecimal money) {
        String xid = RootContext.getXID();
        log.info("全局事务 xid：{}", xid);

        Account account = accountMapper.getAccountByUserId(userId);
        if (account == null) {
            //throw new RuntimeException("账户不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_ACCOUNT.getCode()));
        }
        // 账户余额 与 本次消费金额进行 比较
        if (account.getMoney().compareTo(money) < 0) {
            //throw new RuntimeException("余额不足，预扣款失败");
            throw new RuntimeException(String.valueOf(ErrorEnum.ACCOUNT_LOW_PREPARE.getCode()));
        }
        account.setMoney(account.getMoney().subtract(money));

        QueryWrapper query = new QueryWrapper();
        query.eq("user_id", userId);
        int i = accountMapper.update(account, query);
        log.info("{} 账户余额扣除 {} 元", userId, money);

        return i == 1;
    }
}
