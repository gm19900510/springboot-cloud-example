package com.gm.seata.multiplee.datasource.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorEnum {

    NO_SUCH_COMMODITY(3000, "无此商品"),
    STORAGE_LOW_PREPARE(3001, "库存不足，预扣库存失败"),
    STORAGE_LOW_COMMIT(3002, "库存不足，扣库存失败"),
    NO_SUCH_ACCOUNT(4000, "无此账户"),
    ACCOUNT_LOW_PREPARE(4001, "余额不足，预扣款失败"),
    ACCOUNT_LOW_COMMIT(4002, "余额不足，扣款失败"),
    UNKNOWN_EXCEPTION(9999, "远程方法调用异常");

    private final Integer code;
    private final String title;

    public static ErrorEnum getEnumByCode(int code) {
        for (ErrorEnum error : ErrorEnum.values()) {
            if (error.getCode().equals(code)) {
                return error;
            }
        }
        return null;
    }
}
