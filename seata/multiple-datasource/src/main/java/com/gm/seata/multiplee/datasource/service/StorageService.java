package com.gm.seata.multiplee.datasource.service;

public interface StorageService {

    /**
     * 扣除库存
     *
     * @param commodityCode
     * @param count
     * @return
     */
    boolean deduct(String commodityCode, Integer count);
}
