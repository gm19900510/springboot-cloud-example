package com.gm.seata.multiplee.datasource.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.multiplee.datasource.entity.Storage;
import com.gm.seata.multiplee.datasource.mapper.StorageMapper;
import com.gm.seata.multiplee.datasource.service.StorageService;
import com.gm.seata.multiplee.datasource.util.ErrorEnum;
import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class StorageServiceImpl implements StorageService {

    @Autowired
    StorageMapper storageMapper;

    // @DS注解切换数据源必须要在@Transaction之前执行
    @Override
    @DS("storage")
    @Transactional(rollbackFor = Exception.class)
    public boolean deduct(String commodityCode, Integer count) {
        String xid = RootContext.getXID();
        log.info("全局事务 xid：{}", xid);
        Storage storage = storageMapper.getStorageByCommodityCode(commodityCode);
        if (storage == null) {
            //throw new RuntimeException("商品不存在");
            throw new RuntimeException(String.valueOf(ErrorEnum.NO_SUCH_COMMODITY.getCode()));
        }
        if (storage.getCount() < count) {
            //throw new RuntimeException("库存不足，预扣库存失败");
            throw new RuntimeException(String.valueOf(ErrorEnum.STORAGE_LOW_PREPARE.getCode()));
        }
        storage.setCount(storage.getCount() - count);

        QueryWrapper query = new QueryWrapper();
        query.eq("commodity_code", commodityCode);
        Integer i = storageMapper.update(storage, query);
        log.info("{} 商品库存扣除 {} 个", commodityCode, count);
        return i == 1;
    }
}
