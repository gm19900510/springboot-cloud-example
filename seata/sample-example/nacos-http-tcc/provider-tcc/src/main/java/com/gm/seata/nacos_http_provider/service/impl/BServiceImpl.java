package com.gm.seata.nacos_http_provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.nacos_http_provider.entity.B;
import com.gm.seata.nacos_http_provider.mapper.BMapper;
import com.gm.seata.nacos_http_provider.service.BService;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
public class BServiceImpl implements BService {

    @Autowired
    BMapper bMapper;

    @Override
    public void prepare(BusinessActionContext actionContext, String createBy) {
        String xid = actionContext.getXid();
        log.info("xid：{}，createBy：{}", xid, createBy);
        B b = new B();
        b.setCreateBy(createBy);
        b.setCreateTime(LocalDateTime.now());
        bMapper.insert(b);
    }

    @Override
    public boolean commit(BusinessActionContext actionContext) {
        String xid = actionContext.getXid();
        log.info("xid：{}", xid);

        // 自行补充提交业务
        return true;
    }

    @Override
    public boolean rollback(BusinessActionContext actionContext) {
        String xid = actionContext.getXid();
        log.info("xid：{}", xid);
        String createBy = (String) actionContext.getActionContext("createBy");
        QueryWrapper<B> wrapper = new QueryWrapper();
        wrapper.eq("create_by", createBy);
        B b = bMapper.selectOne(wrapper);
        log.info("b：{}", b);
        // 执行自定义回滚删除操作
        bMapper.deleteById(b.getId());
        return true;
    }
}
