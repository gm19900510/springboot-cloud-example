package com.gm.seata.nacos_http_provider.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

@LocalTCC
public interface BService {

    /**
     * 执行资源检查及预业务操作
     */
    // @BusinessActionContextParameter 注解就是将对应的参数放入到 BusinessActionContext 中，将来可以从 BusinessActionContext 中取出对应的参数。
    @TwoPhaseBusinessAction(name = "bService", commitMethod = "commit", rollbackMethod = "rollback")
    void prepare(BusinessActionContext actionContext, @BusinessActionContextParameter(paramName = "createBy") String createBy);

    /**
     * 全局事物进行提交
     */
    boolean commit(BusinessActionContext actionContext);

    /**
     * 全局事务进行回滚
     */
    boolean rollback(BusinessActionContext actionContext);
}
