package com.gm.seata.nacos_http_provider.controller;

import com.gm.seata.nacos_http_provider.service.BService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProviderController {

    @Autowired
    BService bService;

    @RequestMapping(value = "save", method = RequestMethod.GET)
    public void saveB(@RequestParam("createBy") String createBy) {
        bService.prepare(null, createBy);
    }

}
