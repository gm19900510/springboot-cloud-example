package com.gm.seata.nacos_http_consumer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.seata.rm.tcc.api.BusinessActionContext;
import com.gm.seata.nacos_http_consumer.entity.A;
import com.gm.seata.nacos_http_consumer.mapper.AMapper;
import com.gm.seata.nacos_http_consumer.service.AService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@Service
public class AServiceImpl implements AService {

    @Autowired
    AMapper aMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void prepare(BusinessActionContext actionContext, String createBy) {
        String xid = actionContext.getXid();
        log.info("xid：{}，参数 createBy：{}", xid, createBy);
        A a = new A();
        a.setCreateBy(createBy);
        a.setCreateTime(LocalDateTime.now());
        aMapper.insert(a);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean commit(BusinessActionContext actionContext) {
        String xid = actionContext.getXid();
        log.info("xid：{}", xid);

        // 自行补充提交业务
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean rollback(BusinessActionContext actionContext) {
        String xid = actionContext.getXid();
        log.info("xid：{}", xid);
        String createBy = (String) actionContext.getActionContext("createBy");
        QueryWrapper<A> wrapper = new QueryWrapper();
        wrapper.eq("create_by", createBy);
        A a = aMapper.selectOne(wrapper);
        log.info("a：{}", a);
        // 执行自定义回滚删除操作
        aMapper.deleteById(a.getId());
        return true;
    }
}
