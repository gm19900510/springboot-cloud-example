package com.gm.seata.nacos_http_consumer.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "provider-tcc")
public interface ProviderServiceFeign {

    @RequestMapping(value = "save", method = RequestMethod.GET)
    void save(@RequestParam("createBy") String createBy);

}
