package com.gm.seata.nacos_http_consumer.service.impl;

import com.gm.seata.nacos_http_consumer.service.AService;
import com.gm.seata.nacos_http_consumer.service.IService;
import com.gm.seata.nacos_http_consumer.service.ProviderServiceFeign;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceImpl implements IService {

    @Autowired
    ProviderServiceFeign providerServiceFeign;

    @Autowired
    AService aService;

    @Override
    @GlobalTransactional
    public void save(String createBy) {
        providerServiceFeign.save(createBy);
        aService.prepare(null, createBy);
        int i = 1 / 0;
    }
}
