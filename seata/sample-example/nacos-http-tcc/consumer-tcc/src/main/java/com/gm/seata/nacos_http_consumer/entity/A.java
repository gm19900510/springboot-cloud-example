package com.gm.seata.nacos_http_consumer.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_a")
public class A {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    private String createBy;

    private LocalDateTime createTime;
}
