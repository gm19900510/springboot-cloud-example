package com.gm.seata.nacos_http_consumer.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

@LocalTCC
public interface AService {

    /**
     * 执行资源检查及预业务操作
     */
    @TwoPhaseBusinessAction(name = "bService", commitMethod = "commit", rollbackMethod = "rollback")
    void prepare(BusinessActionContext actionContext, @BusinessActionContextParameter(paramName = "createBy") String createBy);

    /**
     * 全局事物进行提交
     */
    boolean commit(BusinessActionContext actionContext);

    /**
     * 全局事务进行回滚
     */
    boolean rollback(BusinessActionContext actionContext);

}
