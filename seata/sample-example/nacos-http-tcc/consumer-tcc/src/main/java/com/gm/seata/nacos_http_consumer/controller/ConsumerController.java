package com.gm.seata.nacos_http_consumer.controller;

import com.gm.seata.nacos_http_consumer.service.IService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ConsumerController {

    @Autowired
    IService iService;

    @RequestMapping(value = "save", method = RequestMethod.GET)
    public String save(@RequestParam("createBy") String createBy) {
        try {
            iService.save(createBy);
            return "全局事务操作成功";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "全局事务操作失败，error=" + e.getMessage();
        }
    }
}
