package com.gm.seata.nacos_http_provider.service.impl;

import com.gm.seata.nacos_http_provider.entity.B;
import com.gm.seata.nacos_http_provider.mapper.BMapper;
import com.gm.seata.nacos_http_provider.service.BService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class BServiceImpl implements BService {

    @Autowired
    BMapper bMapper;

    @Override
    public void save(String createBy) {
        B b = new B();
        b.setCreateBy(createBy);
        b.setCreateTime(LocalDateTime.now());
        bMapper.insert(b);
    }
}
