package com.gm.seata.nacos_http_provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.seata.nacos_http_provider.entity.B;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BMapper extends BaseMapper<B> {
}
