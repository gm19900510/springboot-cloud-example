package com.gm.seata.nacos_http_consumer.service.impl;

import com.gm.seata.nacos_http_consumer.service.ProviderServiceFeign;
import io.seata.spring.annotation.GlobalTransactional;
import com.gm.seata.nacos_http_consumer.entity.A;
import com.gm.seata.nacos_http_consumer.mapper.AMapper;
import com.gm.seata.nacos_http_consumer.service.AService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AServiceImpl implements AService {

    @Autowired
    ProviderServiceFeign providerServiceFeign;

    @Autowired
    AMapper aMapper;

    @GlobalTransactional
    @Override
    public void save(String createBy) {
        providerServiceFeign.save(createBy);
        A a = new A();
        a.setCreateBy(createBy);
        a.setCreateTime(LocalDateTime.now());
        aMapper.insert(a);

        int i = 1 / 0;

    }
}
