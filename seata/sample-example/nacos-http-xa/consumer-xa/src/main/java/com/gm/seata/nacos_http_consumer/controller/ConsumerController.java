package com.gm.seata.nacos_http_consumer.controller;

import com.gm.seata.nacos_http_consumer.service.AService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsumerController {

    @Autowired
    AService aService;

    @RequestMapping(value = "save", method = RequestMethod.GET)
    public String save(@RequestParam("createBy") String createBy) {
        aService.save(createBy);
        return "成功";
    }

}
