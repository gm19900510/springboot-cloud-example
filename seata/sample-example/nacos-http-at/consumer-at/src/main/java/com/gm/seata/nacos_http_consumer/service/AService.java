package com.gm.seata.nacos_http_consumer.service;

import com.gm.seata.nacos_http_consumer.entity.A;
import java.util.Map;

public interface AService {
    Long save(String createBy);

    void update(Long id, Map<String, String> map);

    void writeIsolation_GlobalLock(Long id);

    void writeIsolation_GlobalTransactional(Long id);

    Map<String, Object> readIsolation_GlobalLock(Long id);

    Map<String, Object> readIsolation_GlobalTransactional(Long id);

    Map<String, Object> dirtyReadExample(Long id);

    A dirtyWriteExample(Long id);
}
