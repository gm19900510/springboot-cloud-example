package com.gm.seata.nacos_http_consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.gm.seata.nacos_http_consumer.service")
public class NacosHttpConsumerATApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosHttpConsumerATApplication.class, args);
    }
}
