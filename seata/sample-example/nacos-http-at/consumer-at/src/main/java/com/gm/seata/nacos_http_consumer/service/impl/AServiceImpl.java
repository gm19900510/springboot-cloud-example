package com.gm.seata.nacos_http_consumer.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.nacos_http_consumer.service.ProviderServiceFeign;
import io.seata.spring.annotation.GlobalLock;
import io.seata.spring.annotation.GlobalTransactional;
import com.gm.seata.nacos_http_consumer.entity.A;
import com.gm.seata.nacos_http_consumer.mapper.AMapper;
import com.gm.seata.nacos_http_consumer.service.AService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class AServiceImpl implements AService {

    @Autowired
    ProviderServiceFeign providerServiceFeign;

    @Autowired
    AMapper aMapper;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @GlobalTransactional
    @Override
    public Long save(String createBy) {
        providerServiceFeign.save(createBy);
        A a = new A();
        a.setCreateBy(createBy);
        a.setCreateTime(LocalDateTime.now());
        aMapper.insert(a);

        if (Math.random() < 0.5) {
            throw new RuntimeException("模拟调用本地接口异常");
        }
        return a.getId();
    }

    /**
     * 增加本地线程休眠，模式全局事务进行
     *
     * @param id
     */
    @GlobalTransactional
    @Override
    public void update(Long id, Map<String, String> map) {
        String createBy = UUID.randomUUID().toString();
        providerServiceFeign.save(createBy);

        A a = aMapper.selectById(id);
        map.put("修改前", JSON.toJSON(a).toString());

        a.setCreateBy(createBy);
        a.setCreateTime(LocalDateTime.now());
        QueryWrapper<A> queryWrapper = new QueryWrapper();
        queryWrapper.eq("id", id);
        aMapper.update(a, queryWrapper);

        map.put("修改后", JSON.toJSON(a).toString());
        try {
            TimeUnit.SECONDS.sleep(15);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        if (Math.random() < 0.5) {
            throw new RuntimeException("模拟调用本地接口异常");
        }
    }

    /**
     * 写隔离，通过GlobalLock方式，可了解lockRetryInterval和lockRetryTimes属性
     *
     * @param id
     */
    @GlobalLock(lockRetryInterval = 1000, lockRetryTimes = 30)
    @Override
    public void writeIsolation_GlobalLock(Long id) {
        A a = new A();
        a.setId(id);
        a.setCreateBy(UUID.randomUUID().toString());
        a.setCreateTime(LocalDateTime.now());
        QueryWrapper<A> queryWrapper = new QueryWrapper();
        queryWrapper.eq("id", id);
        aMapper.update(a, queryWrapper);
    }

    /**
     * 写隔离，通过GlobalTransactional方式，可了解lockRetryInterval和lockRetryTimes属性
     *
     * @param id
     */
    @GlobalTransactional/*(lockRetryInterval = 1000, lockRetryTimes = 30)*/
    @Override
    public void writeIsolation_GlobalTransactional(Long id) {
        A a = new A();
        a.setId(id);
        a.setCreateBy(UUID.randomUUID().toString());
        a.setCreateTime(LocalDateTime.now());
        QueryWrapper<A> queryWrapper = new QueryWrapper();
        queryWrapper.eq("id", id);
        aMapper.update(a, queryWrapper);
    }

    @GlobalLock(lockRetryInterval = 1000, lockRetryTimes = 30)
    @Override
    public Map<String, Object> readIsolation_GlobalLock(Long id) {
        return jdbcTemplate.queryForMap("select * from t_a where id=? for update ", id);
    }

    @GlobalTransactional(lockRetryInterval = 1000, lockRetryTimes = 30)
    @Override
    public Map<String, Object> readIsolation_GlobalTransactional(Long id) {
        return jdbcTemplate.queryForMap("select * from t_a where id=? for update ", id);
    }

    @Override
    public Map<String, Object> dirtyReadExample(Long id) {
        return jdbcTemplate.queryForMap("select * from t_a where id=?", id);
    }

    @Override
    public A dirtyWriteExample(Long id) {
        A a = new A();
        a.setId(id);
        a.setCreateBy(UUID.randomUUID().toString());
        a.setCreateTime(LocalDateTime.now());
        QueryWrapper<A> queryWrapper = new QueryWrapper();
        queryWrapper.eq("id", id);
        aMapper.update(a, queryWrapper);
        return a;
    }
}
