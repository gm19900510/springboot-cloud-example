package com.gm.seata.nacos_http_consumer.controller;

import com.alibaba.fastjson.JSON;
import com.gm.seata.nacos_http_consumer.entity.A;
import com.gm.seata.nacos_http_consumer.service.AService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
public class ConsumerController {

    @Autowired
    AService aService;

    @RequestMapping(value = "save", method = RequestMethod.GET)
    public String save(@RequestParam("createBy") String createBy) {
        try {
            return "全局事务操作成功，id=" + aService.save(createBy);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "全局事务操作失败，error=" + e.getMessage();
        }
    }

    /**
     * 模拟长全局事务，随机成功失败
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String update(@RequestParam("id") Long id) {
        Map<String, String> map = new HashMap<>();
        try {
            aService.update(id, map);
            return "全局事务操作成功，" + JSON.toJSON(map).toString();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "全局事务操作失败，" + JSON.toJSON(map).toString() + "，error=" + e.getMessage();
        }
    }

    @RequestMapping(value = "dirty/write", method = RequestMethod.GET)
    public String dirtyWrite(@RequestParam("id") Long id) {
        try {
            A a = aService.dirtyWriteExample(id);
            return "本地事务-写操作成功，" + JSON.toJSON(a).toString();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "本地事务-写操作失败，error=" + e.getMessage();
        }
    }

    @RequestMapping(value = "dirty/read", method = RequestMethod.GET)
    public String dirtyRead(@RequestParam("id") Long id) {
        return JSON.toJSON(aService.dirtyReadExample(id)).toString();
    }

    @RequestMapping(value = "write/globallock", method = RequestMethod.GET)
    public String writeIsolation_GlobalLock(@RequestParam("id") Long id) {
        try {
            aService.writeIsolation_GlobalLock(id);
            return "本地事务-写操作成功";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "本地事务-写操作失败，error=" + e.getMessage();
        }
    }

    @RequestMapping(value = "write/globaltransactional", method = RequestMethod.GET)
    public String writeIsolation_GlobalTransactional(@RequestParam("id") Long id) {
        try {
            aService.writeIsolation_GlobalTransactional(id);
            return "本地事务-写操作成功";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "本地事务-写操作失败，error=" + e.getMessage();
        }
    }

    @RequestMapping(value = "read/globallock", method = RequestMethod.GET)
    public String readIsolation_GlobalLock(@RequestParam("id") Long id) {
        return JSON.toJSON(aService.readIsolation_GlobalLock(id)).toString();
    }

    @RequestMapping(value = "read/globaltransactional", method = RequestMethod.GET)
    public String readIsolation_GlobalTransactional(@RequestParam("id") Long id) {
        return JSON.toJSON(aService.readIsolation_GlobalTransactional(id)).toString();
    }
}
