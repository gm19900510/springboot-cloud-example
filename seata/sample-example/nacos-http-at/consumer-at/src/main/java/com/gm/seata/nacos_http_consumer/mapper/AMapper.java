package com.gm.seata.nacos_http_consumer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.seata.nacos_http_consumer.entity.A;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AMapper extends BaseMapper<A> {
}
