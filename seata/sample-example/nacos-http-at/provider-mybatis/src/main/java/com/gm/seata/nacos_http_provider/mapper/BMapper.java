package com.gm.seata.nacos_http_provider.mapper;

import com.gm.seata.nacos_http_provider.entity.B;

public interface BMapper {
    void insert(B b);
}
