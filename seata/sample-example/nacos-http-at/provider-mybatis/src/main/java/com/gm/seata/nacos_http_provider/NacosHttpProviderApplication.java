package com.gm.seata.nacos_http_provider;

import io.seata.spring.annotation.datasource.EnableAutoDataSourceProxy;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication/*(exclude = {DataSourceAutoConfiguration.class})*/
@EnableDiscoveryClient
@MapperScan(value = "com.gm.seata.nacos_http_provider.mapper")
/*@EnableAutoDataSourceProxy*/
public class NacosHttpProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosHttpProviderApplication.class, args);
    }

}
