package com.gm.seata.nacos_http_provider.entity;

import lombok.Data;
import java.time.LocalDateTime;

@Data
public class B {

    private Long id;

    private String createBy;

    private LocalDateTime createTime;
}
