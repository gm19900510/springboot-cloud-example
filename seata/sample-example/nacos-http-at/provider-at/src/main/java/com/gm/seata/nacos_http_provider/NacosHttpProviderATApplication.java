package com.gm.seata.nacos_http_provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class NacosHttpProviderATApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosHttpProviderATApplication.class, args);
    }

}
