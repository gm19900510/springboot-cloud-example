package com.gm.seata.nacos_http_provider;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.nacos_http_provider.entity.JsonDemo;
import com.gm.seata.nacos_http_provider.mapper.JsonDemoMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class NacosHttpProviderATApplicationTests {
    @Autowired
    JsonDemoMapper jsonDemoMapper;

    @Test
    @Transactional
    @Rollback(value = false)
    void test1(){
        JsonDemo json = new JsonDemo();
        JSONObject j = new JSONObject();
        j.put("name","gm");
        j.put("sex","男");
        json.setExtraObject(j);
        List<JSONObject> l = new ArrayList<>();
        l.add(j);
        json.setExtraList(l);
        jsonDemoMapper.insert(json);
    }

    @Test
    void test2(){
        QueryWrapper<JsonDemo> q = new QueryWrapper<>();
        q.eq("id",4);
        JsonDemo json = jsonDemoMapper.selectOne(q);
        System.out.println(JSONObject.toJSONString(json));
    }
}
