package com.gm.seata.nacos_http_consumer.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "provider")
public interface ProviderServiceFeign {

    @RequestMapping(value = "save", method = RequestMethod.GET)
    boolean save(@RequestParam("createBy") String createBy);

    @RequestMapping(value = "compensateSave", method = RequestMethod.GET)
    boolean compensateSave(@RequestParam("createBy") String createBy);
}
