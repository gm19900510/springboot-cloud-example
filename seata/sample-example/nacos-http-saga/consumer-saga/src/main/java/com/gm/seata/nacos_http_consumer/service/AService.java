package com.gm.seata.nacos_http_consumer.service;

public interface AService {
    boolean save(String businessKey, String createBy);

    boolean compensateSave(String businessKey, String createBy);
}
