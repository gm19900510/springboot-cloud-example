package com.gm.seata.nacos_http_consumer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.nacos_http_consumer.entity.A;
import com.gm.seata.nacos_http_consumer.mapper.AMapper;
import com.gm.seata.nacos_http_consumer.service.AService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@Service(value = "aService")
public class AServiceImpl implements AService {

    @Autowired
    AMapper aMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean save(String businessKey, String createBy) {
        log.info("saga状态机调用save方法，参数businessKey={},createBy={}", businessKey, createBy);

        A a = new A();
        a.setCreateBy(createBy);
        a.setCreateTime(LocalDateTime.now());
        aMapper.insert(a);

        if (Math.random() < 0.5) {
            throw new RuntimeException("模拟调用本地save方法异常");
        }

        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean compensateSave(String businessKey, String createBy) {
        log.info("saga状态机调用compensateSave方法，参数businessKey={},createBy={}", businessKey, createBy);
        QueryWrapper<A> wrapper = new QueryWrapper();
        wrapper.eq("create_by", createBy);
        wrapper.last("limit 1");
        A a = aMapper.selectOne(wrapper);
        log.info("a：{}", a);
        if (a != null) {
            // 执行自定义回滚删除操作
            aMapper.deleteById(a.getId());
        }
        return true;
    }
}
