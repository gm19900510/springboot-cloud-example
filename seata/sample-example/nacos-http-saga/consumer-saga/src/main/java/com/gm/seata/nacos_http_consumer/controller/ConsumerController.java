package com.gm.seata.nacos_http_consumer.controller;

import io.seata.saga.engine.StateMachineEngine;
import io.seata.saga.statelang.domain.ExecutionStatus;
import io.seata.saga.statelang.domain.StateMachineInstance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
public class ConsumerController {

    @Autowired
    StateMachineEngine stateMachineEngine;

    @RequestMapping(value = "save", method = RequestMethod.GET)
    public String save(@RequestParam("createBy") String createBy) {

        //唯一健
        String businessKey = String.valueOf(System.currentTimeMillis());
        Map<String, Object> startParams = new HashMap<>();
        startParams.put("businessKey", businessKey);
        startParams.put("createBy", createBy);

        //同步执行
        StateMachineInstance inst = stateMachineEngine.startWithBusinessKey("demo", null, businessKey, startParams);

        if (ExecutionStatus.SU.equals(inst.getStatus())) {
            log.info("成功,saga transaction execute Succeed. XID: " + inst.getId());
            return "成功";
        } else {
            log.info("失败 ,saga transaction execute failed. XID: " + inst.getId());
            return "失败";
        }
    }

}
