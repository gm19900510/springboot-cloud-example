package com.gm.seata.nacos_http_consumer.service.impl;

import com.gm.seata.nacos_http_consumer.service.BService;
import com.gm.seata.nacos_http_consumer.service.ProviderServiceFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service(value = "bService")
public class BServiceImpl implements BService {

    @Autowired
    ProviderServiceFeign providerServiceFeign;

    @Override
    public boolean save(String businessKey, String createBy) {
        log.info("saga状态机调用save方法，参数businessKey={},createBy={}", businessKey, createBy);
        providerServiceFeign.save(createBy);
        if (Math.random() < 0.5) {
            throw new RuntimeException("模拟远程调用第三方接口异常");
        }
        return true;
    }

    @Override
    public boolean compensateSave(String businessKey, String createBy) {
        log.info("saga状态机调用compensateSave方法，参数businessKey={},createBy={}", businessKey, createBy);
        return providerServiceFeign.compensateSave(createBy);
    }
}
