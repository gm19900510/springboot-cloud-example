seata官方 saga模式介绍：http://seata.io/zh-cn/docs/user/saga.html

seata官方 saga设计器：https://seata.io/saga_designer/index.html#/

seata-saga数据库表结构：https://github.com/seata/seata/blob/1.6.1/script/client/saga/db/mysql.sql

seata官方 示例：https://github.com/seata/seata-samples/tree/master/saga

第三方 seata-saga使用介绍：https://blog.csdn.net/w1014074794/article/details/116997363

第三方示例源码：https://github.com/StarlightWANLI/local-saga
