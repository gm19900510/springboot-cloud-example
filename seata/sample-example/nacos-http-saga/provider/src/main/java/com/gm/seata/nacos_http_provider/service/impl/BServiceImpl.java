package com.gm.seata.nacos_http_provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.seata.nacos_http_provider.entity.B;
import com.gm.seata.nacos_http_provider.mapper.BMapper;
import com.gm.seata.nacos_http_provider.service.BService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
public class BServiceImpl implements BService {

    @Autowired
    BMapper bMapper;

    @Override
    public boolean save(String createBy) {
        B b = new B();
        b.setCreateBy(createBy);
        b.setCreateTime(LocalDateTime.now());
        bMapper.insert(b);
        return true;
    }

    @Override
    public boolean compensateSave(String createBy) {
        QueryWrapper<B> wrapper = new QueryWrapper();
        wrapper.eq("create_by", createBy);
        wrapper.last("limit 1");
        B b = bMapper.selectOne(wrapper);
        log.info("b：{}", b);
        if (b != null) {
            // 执行自定义回滚删除操作
            bMapper.deleteById(b.getId());
        }
        return true;
    }
}
