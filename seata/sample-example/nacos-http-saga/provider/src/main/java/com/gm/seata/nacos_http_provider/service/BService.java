package com.gm.seata.nacos_http_provider.service;

public interface BService {
    boolean save(String createBy);
    boolean compensateSave(String createBy);
}
