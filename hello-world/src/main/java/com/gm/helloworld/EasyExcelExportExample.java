package com.gm.helloworld;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import com.alibaba.excel.write.style.column.LongestMatchColumnWidthStyleStrategy;
import com.alibaba.fastjson.JSONObject;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import java.io.IOException;
import java.util.*;

public class EasyExcelExportExample {
    public static void main(String[] args) throws IOException {
        // 输出文件路径（提前创建好测试目录 D:/easyexcel-test/test-dynamic-head）
        String filename = "test01.xlsx";
        String jsonStr = "{\n" +
                "\t\"商品条码\": {\n" +
                "\t\t\"columnName\": \"commodity_code\",\n" +
                "\t\t\"index\": 1\n" +
                "\t},\n" +
                "\t\"商品名称\": {\n" +
                "\t\t\"columnName\": \"commodity_name\",\n" +
                "\t\t\"index\": 2\n" +
                "\t},\n" +
                "\t\"款式名称\": {\n" +
                "\t\t\"columnName\": \"style\",\n" +
                "\t\t\"index\": 3\n" +
                "\t},\n" +
                "\t\"建议零售价\": {\n" +
                "\t\t\"columnName\": \"suggested_price\",\n" +
                "\t\t\"index\": 4\n" +
                "\t},\n" +
                "\t\"尺码名称\": {\n" +
                "\t\t\"columnName\": \"size\",\n" +
                "\t\t\"index\": 5\n" +
                "\t},\n" +
                "\t\"颜色名称\": {\n" +
                "\t\t\"columnName\": \"color\",\n" +
                "\t\t\"index\": 6\n" +
                "\t},\n" +
                "\t\"零售价\": {\n" +
                "\t\t\"columnName\": \"price\",\n" +
                "\t\t\"index\": 7\n" +
                "\t},\n" +
                "\t\"长度\": {\n" +
                "\t\t\"columnName\": \"length\",\n" +
                "\t\t\"index\": 8\n" +
                "\t},\n" +
                "\t\"商品图片\": {\n" +
                "\t\t\"columnName\": \"commodity_img\",\n" +
                "\t\t\"index\": 9\n" +
                "\t},\n" +
                "\t\"分类1编码\": {\n" +
                "\t\t\"columnName\": \"category1_code\",\n" +
                "\t\t\"index\": 10\n" +
                "\t},\n" +
                "\t\"分类1名称\": {\n" +
                "\t\t\"columnName\": \"category1_name\",\n" +
                "\t\t\"index\": 11\n" +
                "\t},\n" +
                "\t\"分类2编码\": {\n" +
                "\t\t\"columnName\": \"category2_code\",\n" +
                "\t\t\"index\": 12\n" +
                "\t},\n" +
                "\t\"分类2名称\": {\n" +
                "\t\t\"columnName\": \"category2_name\",\n" +
                "\t\t\"index\": 13\n" +
                "\t}\n" +
                "}";
        // 设置动态头
        List<List<String>> headList = new ArrayList<>();
        /*List<String> head0 = new ArrayList<>();
        head0.add("用户名");
        List<String> head1 = new ArrayList<>();
        head1.add("年龄");
        List<String> head2 = new ArrayList<>();
        head2.add("地址");
        headList.add(head0);
        headList.add(head1);
        headList.add(head2);*/

        Map<String, Map> jsonObject = JSONObject.parseObject(jsonStr, Map.class);

        List<Map.Entry<String, Map>> orderList = new ArrayList<>(jsonObject.entrySet());
        Collections.sort(orderList, Comparator.comparingInt(o -> Integer.parseInt(o.getValue().get("index").toString())));

        for (Map.Entry<String, Map> map : orderList) {
            System.out.println(map.getKey() + "<====>" + map.getValue());
            List<String> head = new ArrayList<>();
            head.add(map.getKey());
            headList.add(head);
        }

        // 获取动态数据
        List<List<Object>> dataList = new ArrayList<>();

        /*List<Object> data = new ArrayList<>();
        data.add("小白");
        data.add(26);
        data.add("广东广州");
        dataList.add(data);
        List<Object> data2 = new ArrayList<>();
        data2.add("小黑小黑小黑小黑小黑小黑小黑小黑小黑小黑小黑小黑小黑小黑小黑小黑小黑");
        data2.add(25);
        data2.add("广东汕头");
        dataList.add(data2);*/

        // 内容样式策略
        WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
        // 垂直居中,水平居中
        contentWriteCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
        contentWriteCellStyle.setBorderLeft(BorderStyle.THIN);
        contentWriteCellStyle.setBorderTop(BorderStyle.THIN);
        contentWriteCellStyle.setBorderRight(BorderStyle.THIN);
        contentWriteCellStyle.setBorderBottom(BorderStyle.THIN);

        // 设置自动换行
        // contentWriteCellStyle.setWrapped(true);
        // 字体策略
        WriteFont contentWriteFont = new WriteFont();
        // 字体大小
        contentWriteFont.setFontHeightInPoints((short) 12);
        contentWriteFont.setFontName("宋体");
        contentWriteCellStyle.setWriteFont(contentWriteFont);

        //头策略使用默认 设置字体大小
        WriteCellStyle headWriteCellStyle = new WriteCellStyle();
        WriteFont headWriteFont = new WriteFont();
        headWriteFont.setFontHeightInPoints((short) 12);
        headWriteFont.setFontName("宋体");
        headWriteCellStyle.setWriteFont(headWriteFont);
        headWriteCellStyle.setFillForegroundColor(IndexedColors.WHITE.index);

        // 先仅仅写入头，再以不写入头的方式写入数据
        EasyExcel.write(filename).
                registerWriteHandler(new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle))
                .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy()).head(headList).sheet("Sheet1").doWrite(dataList);

    }
}