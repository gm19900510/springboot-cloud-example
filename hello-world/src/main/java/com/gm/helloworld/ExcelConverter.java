package com.gm.helloworld;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 通过java反射机制利用json映射excel列头与实体属性的关系实现动态读取excel
 */
public class ExcelConverter {
    public static void main(String[] args) {
        String json = "{\"姓名\":\"name\",\"年龄\":\"age\",\"性别\":\"gender\",\"价格\":\"price\"}";

        try {
            // 使用Jackson库将JSON字符串转换为Map对象
            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> mapping = mapper.readValue(json, Map.class);
            File excelFile = new File("hello-world/file.xlsx");

            // 使用 EasyExcel 读取 Excel 文件并转换为实体类对象
            List<User> userList = new ArrayList<>();

            EasyExcel.read(excelFile)
                    .headRowNumber(1)  // 设置 Excel 的列头行号，一般为第一行
                    .registerReadListener(new AnalysisEventListener<Map<Integer, Object>>() {

                        //Excel表头（列名）数据缓存结构
                        private Map<Integer, String> headMap = new HashMap<>();

                        @Override
                        public void invoke(Map<Integer, Object> rowData, AnalysisContext context) {
                            System.out.println(rowData);
                            User user = new User();
                            for (Map.Entry<Integer, Object> entry : rowData.entrySet()) {
                                Integer index = entry.getKey();
                                Object value = entry.getValue();
                                String propertyName = mapping.get(headMap.get(index));
                                if (propertyName != null) {
                                    // 利用反射将列数据设置到实体对象的对应属性上
                                    try {
                                        Field field = User.class.getDeclaredField(propertyName);
                                        field.setAccessible(true);

                                        Class<?> fieldType = field.getType();

                                        if (fieldType == Integer.class) {
                                            field.set(user, Integer.valueOf(value.toString()));
                                        } else if (fieldType == Long.class) {
                                            field.set(user, Long.valueOf((String) value));
                                        } else if (fieldType == Double.class) {
                                            field.set(user, Double.valueOf(value.toString()));
                                        } else if (fieldType == BigDecimal.class) {
                                            field.set(user, BigDecimal.valueOf(Double.valueOf(value.toString())));
                                        } else {
                                            field.set(user, value.toString());
                                        }
                                    } catch (NoSuchFieldException | IllegalAccessException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            userList.add(user);
                        }

                        @Override
                        public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
                            this.headMap = headMap;
                        }

                        @Override
                        public void doAfterAllAnalysed(AnalysisContext context) {
                            // 数据解析完成后的操作
                        }
                    })
                    .sheet()  // 默认读取第一个 Sheet
                    .doRead();

            System.out.println(userList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    static class User {
        private String name;
        private Integer age;
        private String gender;
        private BigDecimal price;

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    ", gender='" + gender + '\'' +
                    ", price=" + price +
                    '}';
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }
    }
}