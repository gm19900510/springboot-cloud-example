package com.gm.helloworld.controller.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import javax.annotation.PostConstruct;
import java.util.Random;

@Component
public class MqttComponent {

    public static String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    @PostConstruct // 构造函数之后执行
    public void init() {
        String broker = "tcp://core.mttsmart.com:1883";
        String topic = "mqtt/test";
        String username = "mqtt";
        String password = "!Mtt2020";
        String clientId = "subscribe_client";
        clientId = getRandomString(10);
        int qos = 0;

        try {
            MqttClient client = new MqttClient(broker, "MqttClient_" + clientId, new MemoryPersistence());
            // 连接参数
            MqttConnectOptions options = new MqttConnectOptions();
            options.setUserName(username);
            options.setPassword(password.toCharArray());
            options.setConnectionTimeout(60);
            options.setKeepAliveInterval(60);
            // 设置回调
            client.setCallback(new MqttCallback() {

                public void connectionLost(Throwable cause) {
                    System.out.println("connectionLost: " + cause.getMessage());
                }

                public void messageArrived(String topic, MqttMessage message) {
                    //System.out.println("topic: " + topic);
                    //System.out.println("Qos: " + message.getQos());
                    //System.out.println("message content: " + new String(message.getPayload()));

                }

                public void deliveryComplete(IMqttDeliveryToken token) {
                    //System.out.println("deliveryComplete---------" + token.isComplete());
                }

            });
            client.connect(options);
            client.subscribe(topic, qos);
            client.subscribe("/shoptalk/+/gateway/command/exec_reply", 1);
            client.subscribe("/shoptalk/+/gateway/command/reply", 1);
            client.subscribe("/shoptalk/+/gateway/info", 1);
            client.subscribe("/shoptalk/+/gateway/gateway_firmware_update/reply", 1);
            client.subscribe("/shoptalk/+/gateway/heartbeat", 1);
            client.subscribe("/shoptalk/+/AoAServer/locatorsInfo", 1);
            client.subscribe("/shoptalk/+/label/original_data", 1);
            client.subscribe("/shoptalk/+/gateway/command/special_cmd_exec_reply", 1);
            client.subscribe("/shoptalk/+/pong", 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
