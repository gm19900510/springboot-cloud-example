package com.gm.helloworld;

import java.lang.reflect.Field;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonMapper {

    public static void main(String[] args) {
        // 创建一个JSON字符串，包含要转换的JSON数据
        String jsonMappingString = "{\"id\": \"userId\", \"name\": \"userName\", \"age\": \"userAge\"}";
        String jsonDataString = "{\"id\": 1, \"name\": \"John Doe\", \"age\": 30}";

        try {
            // 使用Jackson库将JSON字符串转换为Map对象
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> mappingMap = mapper.readValue(jsonMappingString, Map.class);
            Map<String, Object> dataMap = mapper.readValue(jsonDataString, Map.class);

            // 创建一个实体类，用于存储转换后的数据
            User user = new User();

            // 使用反射机制，遍历映射关系
            for (Map.Entry<String, Object> entry : mappingMap.entrySet()) {
                String jsonFieldName = entry.getKey();
                String fieldName = (String) entry.getValue();

                // 从数据Map中获取JSON字段的值
                Object jsonFieldValue = dataMap.get(jsonFieldName);

                // 使用反射机制，将JSON字段的值设置到实体类对应的属性中
                Field field = User.class.getDeclaredField(fieldName);
                field.setAccessible(true);
                field.set(user, jsonFieldValue);
            }

            // 打印转换后的实体对象
            System.out.println(user);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // 实体类，用于存储转换后的数据
    static class User {
        private int userId;
        private String userName;
        private int userAge;

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public int getUserAge() {
            return userAge;
        }

        public void setUserAge(int userAge) {
            this.userAge = userAge;
        }

        // 省略getter和setter方法

        @Override
        public String toString() {
            return "User [userId=" + userId + ", userName=" + userName + ", userAge=" + userAge + "]";
        }
    }
}