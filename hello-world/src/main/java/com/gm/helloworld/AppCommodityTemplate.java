/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the mttsmart4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.gm.helloworld;

import lombok.Data;
import java.math.BigDecimal;

/**
 * 商品默认模板表
 *
 */
@Data
public class AppCommodityTemplate  {

	/**
	 * 商品条码
	 */
	private String commodityCode;

	/**
	 * 商品名称
	 */
	private String commodityName;

	/**
	 * 商品图片
	 */
	private String commodityImg;

	/**
	 * 销售价
	 */
	private BigDecimal price;

	/**
	 * 建议价
	 */
	private BigDecimal suggestedPrice;

	/**
	 * 款式编码
	 */
	private String style;

	/**
	 * 颜色名称
	 */
	private String color;

	/**
	 * 颜色编码
	 */
	private String colorCode;

	/**
	 * 尺码名称
	 */
	private String size;

	/**
	 * 尺码编码
	 */
	private String sizeCode;

	/**
	 * 长度
	 */
	private String length;

	/**
	 * 性别
	 */
	private String gender;

	/**
	 * 货号
	 */
	private String articleNo;

	/**
	 * 材质
	 */
	private String material;

	/**
	 * 季节
	 */
	private String season;

	/**
	 * 年份
	 */
	private String year;

	/**
	 * 场景
	 */
	private String scene;

	/**
	 * 主题
	 */
	private String theme;

	/**
	 * 分类名称
	 */
	private String category1Name;

	/**
	 * 类编编码
	 */
	private String category1Code;



}
