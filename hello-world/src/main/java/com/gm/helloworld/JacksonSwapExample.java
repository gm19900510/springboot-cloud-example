package com.gm.helloworld;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Iterator;

public class JacksonSwapExample {
    public static void main(String[] args) throws IOException {
        String json = "{\n" +
                "\t\"commodity_code\": \"商品条码\",\n" +
                "\t\"commodity_name\": \"商品名称\",\n" +
                "\t\"style\": \"款式名称\",\n" +
                "\t\"suggested_price\": \"建议零售价\",\n" +
                "\t\"size\": \"尺码名称\",\n" +
                "\t\"color\": \"颜色名称\",\n" +
                "\t\"price\": \"零售价\",\n" +
                "\t\"length\": \"长度\",\n" +
                "\t\"commodity_img\": \"商品图片\",\n" +
                "\t\"category1_code\": \"分类1编码\",\n" +
                "\t\"category1_name\": \"分类1名称\",\n" +
                "\t\"category2_code\": \"分类2编码\",\n" +
                "\t\"category2_name\": \"分类2名称\"\n" +
                "}";

        // 创建 ObjectMapper 对象
        ObjectMapper mapper = new ObjectMapper();

        // 解析 JSON 字符串为 JsonNode 对象
        JsonNode jsonNode = mapper.readTree(json);

        // 交换键值对位置
        JsonNode swappedJson = swapKeyValue(jsonNode);

        // 输出交换后的 JSON 字符串
        System.out.println(mapper.writeValueAsString(swappedJson));
    }

    private static JsonNode swapKeyValue(JsonNode node) {
        if (node.isObject()) { // 如果 JsonNode 是一个对象
            ObjectMapper mapper = new ObjectMapper();
            JsonNode swappedNode = mapper.createObjectNode(); // 创建一个新的 JsonNode 对象

            for (Iterator<String> it = node.fieldNames(); it.hasNext(); ) {
                String field = it.next();
                JsonNode valueNode = node.get(field);
                ((com.fasterxml.jackson.databind.node.ObjectNode) swappedNode).put(valueNode.asText(), field);
            }

            return swappedNode;
        } else if (node.isArray()) { // 如果 JsonNode 是一个数组
            for (JsonNode childNode : node) {
                swapKeyValue(childNode);
            }

            return node;
        } else { // 如果 JsonNode 是一个标量值
            return node;
        }
    }
}