package com.gm.helloworld;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSONObject;

import java.io.File;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 通过java反射机制利用json映射excel列头与实体属性的关系实现动态读取excel
 */
public class RealExcelConverter {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        try {
            String json = "{\"商品条码\":{\"fieldName\":\"commodityCode\",\"index\":1},\"商品名称\":{\"fieldName\":\"commodityName\",\"index\":2},\"款式名称\":{\"fieldName\":\"style\",\"index\":3},\"建议零售价\":{\"fieldName\":\"suggestedPrice\",\"index\":4},\"尺码名称\":{\"fieldName\":\"size\",\"index\":5},\"颜色名称\":{\"fieldName\":\"color\",\"index\":6},\"零售价\":{\"fieldName\":\"price\",\"index\":7},\"长度\":{\"fieldName\":\"length\",\"index\":8},\"商品图片\":{\"fieldName\":\"commodityImg\",\"index\":9},\"商品分类\":{\"fieldName\":\"category1Name\",\"index\":10},\"性别\":{\"fieldName\":\"gender\",\"index\":11},\"年份\":{\"fieldName\":\"year\",\"index\":12},\"材质\":{\"fieldName\":\"material\",\"index\":13},\"季节\":{\"fieldName\":\"season\",\"index\":14},\"货号编码\":{\"fieldName\":\"articleNo\",\"index\":15},\"颜色编码\":{\"fieldName\":\"colorCode\",\"index\":16},\"尺码编码\":{\"fieldName\":\"sizeCode\",\"index\":17},\"场景\":{\"fieldName\":\"scene\",\"index\":18},\"主题\":{\"fieldName\":\"theme\",\"index\":19}}";
            System.out.println(json);
            Map<String, Map> jsonObject = JSONObject.parseObject(json, Map.class);
            Map<String, String> mapping = new HashMap<>();
            List<Map.Entry<String, Map>> list = new ArrayList<>(jsonObject.entrySet());
            for (Map.Entry<String, Map> map : list) {
                mapping.put(map.getKey(), map.getValue().get("fieldName").toString());
            }
            System.out.println("映射关系：");
            System.out.println(mapping);
            File excelFile = new File("hello-world/新款sku-1.xlsx");


            // 使用 EasyExcel 读取 Excel 文件并转换为实体类对象
            List<AppCommodityTemplate> resultList = new ArrayList<>();

            EasyExcel.read(excelFile)
                    .headRowNumber(1)  // 设置 Excel 的列头行号，一般为第一行
                    .registerReadListener(new AnalysisEventListener<Map<Integer, Object>>() {

                        //Excel表头（列名）数据缓存结构
                        private Map<Integer, String> headMap = new HashMap<>();

                        @Override
                        public void invoke(Map<Integer, Object> rowData, AnalysisContext context) {
                            //System.out.println(rowData);
                            AppCommodityTemplate commodity = new AppCommodityTemplate();
                            for (Map.Entry<Integer, Object> entry : rowData.entrySet()) {
                                Integer index = entry.getKey();
                                Object value = entry.getValue();
                                String propertyName = mapping.get(headMap.get(index));
                                if (propertyName != null) {
                                    // 利用反射将列数据设置到实体对象的对应属性上

                                    try {
                                        Field field = AppCommodityTemplate.class.getDeclaredField(propertyName);
                                        field.setAccessible(true);

                                        Class<?> fieldType = field.getType();

                                        if (fieldType == Integer.class) {
                                            field.set(commodity, Integer.valueOf(value.toString()));
                                        } else if (fieldType == Long.class) {
                                            field.set(commodity, Long.valueOf((String) value));
                                        } else if (fieldType == Double.class) {
                                            field.set(commodity, Double.valueOf(value.toString()));
                                        } else if (fieldType == BigDecimal.class) {
                                            field.set(commodity, BigDecimal.valueOf(Double.valueOf(value.toString())));
                                        } else {
                                            if (value != null) {
                                                field.set(commodity, value.toString());

                                            }
                                        }
                                    } catch (NoSuchFieldException | IllegalAccessException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            resultList.add(commodity);
                        }

                        @Override
                        public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
                            this.headMap = headMap;
                        }

                        @Override
                        public void doAfterAllAnalysed(AnalysisContext context) {
                            // 数据解析完成后的操作
                        }
                    })
                    .sheet()  // 默认读取第一个 Sheet
                    .doRead();

            System.out.println(resultList.size());
            System.out.println(resultList.get(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        System.out.println("消耗时间：" + (endTime - startTime));
    }
}