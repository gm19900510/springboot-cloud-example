package com.gm.shading.encrypt.mask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("t_auto_user_info_mod")
public class AutoUserInfoMod {

    @TableId(type = IdType.ASSIGN_ID)
    private Long userId;
    // 身份证，存储加密
    private String idCard;
    // 密码，存储加密
    private String password;
    // 电话，数据脱敏
    private String telephone;
    // 电子邮箱，数据脱敏
    private String email;
}
