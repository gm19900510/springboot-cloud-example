package com.gm.shading.encrypt.mask.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.encrypt.mask.entity.AutoUserInfoMod;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AutoUserInfoModMapper extends BaseMapper<AutoUserInfoMod> {
    void save(AutoUserInfoMod autoOrder);
}
