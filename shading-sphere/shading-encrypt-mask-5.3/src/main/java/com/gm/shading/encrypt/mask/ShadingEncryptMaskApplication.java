package com.gm.shading.encrypt.mask;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.gm.shading.encrypt.mask.mapper")
public class ShadingEncryptMaskApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShadingEncryptMaskApplication.class, args);
    }
}