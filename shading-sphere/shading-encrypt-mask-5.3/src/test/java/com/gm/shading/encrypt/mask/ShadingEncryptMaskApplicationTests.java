package com.gm.shading.encrypt.mask;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.shading.encrypt.mask.entity.AutoUserInfoMod;
import com.gm.shading.encrypt.mask.mapper.AutoUserInfoModMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@SpringBootTest
@Slf4j
public class ShadingEncryptMaskApplicationTests {

    @Autowired
    AutoUserInfoModMapper autoUserInfoModMapper;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    public void testCreateAutUserInfoMod() {
        jdbcTemplate.execute("CREATE TABLE `t_auto_user_info_mod` (\n" +
                " `user_id` bigint(20) NOT NULL COMMENT '用户id',\n" +
                " `id_card` varchar(50) NOT NULL COMMENT '身份证',\n" +
                " `password` varchar(50) NOT NULL COMMENT '用户密码',\n" +
                " `telephone` varchar(50) NOT NULL COMMENT '电话',\n" +
                " `email` varchar(50) NOT NULL COMMENT '邮箱',\n" +
                " PRIMARY KEY (`user_id`) USING BTREE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;");
    }

    @Test
    public void testInsertAutoUserInfoMod() {

        List<String> idCards = Arrays.asList("34122119920825123", "42122319870601123", "32072319750626123",
                "34120219900429123", "15222319970119123", "32038219891112123", "32072319950606123", "32038219900219123", "37132419880902123");
        for (int i = 1; i < 6; i++) {
            AutoUserInfoMod userInfo = new AutoUserInfoMod();
            userInfo.setIdCard(idCards.get(i) + i);
            userInfo.setPassword("abcdefg1234567" + 1);
            userInfo.setTelephone("18645026410");
            userInfo.setEmail("1025304567@qq.com");
            autoUserInfoModMapper.save(userInfo);
        }
    }

    @Test
    public void testSelectAutoUserInfoModAll() {
        List<AutoUserInfoMod> list = autoUserInfoModMapper.selectList(new QueryWrapper<AutoUserInfoMod>());
        for (AutoUserInfoMod userInfo : list) {
            log.info("{}", userInfo);
        }
    }

    @Test
    public void testSelectAutoUserInfoModByIdCard() {

        List<Map<String, Object>> list = jdbcTemplate.queryForList("select * from t_auto_user_info_mod where id_card='320723197506261232'");
        for (Map<String, Object> map : list) {
            log.info("{}", map);
        }

        list = jdbcTemplate.queryForList("select * from t_auto_user_info_mod where id_card like '%3412%'");
        for (Map<String, Object> map : list) {
            log.info("{}", map);
        }
    }
}
