package com.gm.shading.databases.tables;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.shading.databases.tables.entity.Goods;
import com.gm.shading.databases.tables.entity.Order;
import com.gm.shading.databases.tables.entity.OrderItem;
import com.gm.shading.databases.tables.entity.SysDict;
import com.gm.shading.databases.tables.mapper.GoodsMapper;
import com.gm.shading.databases.tables.mapper.OrderItemMapper;
import com.gm.shading.databases.tables.mapper.OrderMapper;
import com.gm.shading.databases.tables.mapper.SysDictMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

@SpringBootTest
@Slf4j
public class ShadingDatabasesTablesApplicationTests {

    @Autowired
    GoodsMapper goodsMapper;

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    OrderItemMapper orderItemMapper;

    @Autowired
    SysDictMapper sysDictMapper;

    @Test
    void addGoods() {
        for (int i = 1; i <= 10; i++) {
            Goods goods = new Goods();

            Random random = new Random();
            int mainClass = random.nextInt(100) % 2 + 1;
            int subClass = random.nextInt(4) + 1;
            goods.setMainClass((long) mainClass);
            goods.setSubClass((long) subClass);
            goods.setGoodsName("商品" + i);
            goodsMapper.insert(goods);
        }
    }

    @Test
    void addGoods2() {
        for (int i = 1; i <= 10; i++) {
            Goods goods = new Goods();

            Random random = new Random();
            int mainClass = random.nextInt(100) % 2 + 1;
            int subClass = random.nextInt(4) + 1;
            goods.setMainClass((long) mainClass);
            goods.setSubClass((long) subClass);
            goods.setGoodsName("商品" + i);
            goodsMapper.save(goods);
        }
    }


    @Test
    void addOrder() {
        List<Goods> list = goodsMapper.selectList(new QueryWrapper());
        for (int i = 1; i <= 10; i++) {
            Order order = new Order();

            Random random = new Random();
            int createBy = random.nextInt(2) + 1;
            int itemCount = random.nextInt(2) + 1;

            order.setCreateTime(LocalDateTime.now());
            order.setCreateBy((long) createBy);
            orderMapper.insert(order);

            for (int j = 0; j < itemCount; j++) {
                OrderItem orderItem = new OrderItem();
                orderItem.setOrderId(order.getOrderId());
                int z = random.nextInt(10);
                Goods goods = list.get(z);
                if (goods != null) {
                    orderItem.setGoodsId(goods.getGoodsId());
                    orderItem.setGoodsName(goods.getGoodsName());
                    orderItem.setCreateTime(LocalDateTime.now());
                    orderItem.setCreateBy((long) createBy);
                    orderItemMapper.insert(orderItem);
                }
            }
            log.info("获取插入的订单ID：{}", order.getOrderId());
        }
    }


    @Test
    void queryAllDict() {
        SysDict sysDict = sysDictMapper.queryAllDict();
        log.info("获取数据字典：{}", sysDict);
    }
}
