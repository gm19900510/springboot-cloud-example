package com.gm.shading.databases.tables.entity;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import org.springframework.util.StringUtils;
import java.util.Map;

@Data
public class SysDictItem {
    // 字典子项代码
    private String dictItemCode;
    // 字典子项展示值
    private String dictItemValue;
    // 字典子项详细描述
    private String dictItemDesc;
    // 自定义json字符串属性
    private Map<String, Object> itemAttrs;

    // 将数据库里json字符串转为map对象
    public void setItemAttrs(String itemAttrs) {
        if (!StringUtils.isEmpty(itemAttrs)) {
            // json字符串转map
            Map mapType = JSON.parseObject(itemAttrs, Map.class);
        }
    }
}

