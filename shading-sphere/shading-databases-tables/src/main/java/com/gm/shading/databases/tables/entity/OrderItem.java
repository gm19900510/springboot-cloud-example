package com.gm.shading.databases.tables.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_order_item")
public class OrderItem {

    @TableId(type = IdType.ASSIGN_ID)
    private Long orderItemId;
    private Long orderId;
    private Long goodsId;
    private String goodsName;
    private LocalDateTime createTime;
    private Long createBy;


}
