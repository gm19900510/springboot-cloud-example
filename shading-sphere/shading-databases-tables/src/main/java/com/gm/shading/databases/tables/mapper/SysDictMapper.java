package com.gm.shading.databases.tables.mapper;

import com.gm.shading.databases.tables.entity.SysDict;
import com.gm.shading.databases.tables.entity.SysDictItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SysDictMapper {
    SysDict queryAllDict();

    SysDictItem queryDictById(@Param("dictId") long dictId);
}
