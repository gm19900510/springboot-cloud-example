package com.gm.shading.databases.tables.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.databases.tables.entity.OrderItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItem> {
}
