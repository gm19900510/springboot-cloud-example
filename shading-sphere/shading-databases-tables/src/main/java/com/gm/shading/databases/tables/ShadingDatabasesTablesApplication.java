package com.gm.shading.databases.tables;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.gm.shading.databases.tables.mapper")
public class ShadingDatabasesTablesApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShadingDatabasesTablesApplication.class, args);
    }
}