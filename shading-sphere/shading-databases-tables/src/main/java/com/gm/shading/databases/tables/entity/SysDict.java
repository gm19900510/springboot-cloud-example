package com.gm.shading.databases.tables.entity;

import lombok.Data;
import java.util.List;

@Data
public class SysDict {
    // 字典代码
    private String dictCode;
    // 字典名称
    private String dictName;
    // 0系统 9解析
    private Integer dictType;
    // 字典项列表
    private List<SysDictItem> dictItems;
}

