package com.gm.shading.databases.tables.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("t_goods")
public class Goods {

    @TableId(type = IdType.ASSIGN_ID)
    private Long goodsId;
    private String goodsName;
    @TableField(value = "main_class")
    private Long mainClass;
    @TableField(value = "sub_class")
    private Long subClass;

}
