package com.gm.shading.databases.tables.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.databases.tables.entity.Order;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {
}
