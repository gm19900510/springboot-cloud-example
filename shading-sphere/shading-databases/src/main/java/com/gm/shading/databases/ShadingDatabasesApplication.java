package com.gm.shading.databases;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.gm.shading.databases.mapper")
public class ShadingDatabasesApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShadingDatabasesApplication.class, args);
    }
}