package com.gm.shading.databases.seata;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan("com.gm.shading.databases.seata.mapper")
@EnableFeignClients("com.gm.shading.databases.seata.service")
public class ShadingDatabasesSeataApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShadingDatabasesSeataApplication.class, args);
    }
}