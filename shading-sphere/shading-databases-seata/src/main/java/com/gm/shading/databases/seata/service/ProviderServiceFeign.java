package com.gm.shading.databases.seata.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "provider-2")
public interface ProviderServiceFeign {

    @RequestMapping(value = "save", method = RequestMethod.GET)
    void save(@RequestParam("createBy") String createBy);

}
