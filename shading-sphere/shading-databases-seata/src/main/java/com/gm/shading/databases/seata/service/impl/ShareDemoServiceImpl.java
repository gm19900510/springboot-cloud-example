package com.gm.shading.databases.seata.service.impl;

import com.gm.shading.databases.seata.entity.ShareDemo;
import com.gm.shading.databases.seata.mapper.ShareDemoMapper;
import com.gm.shading.databases.seata.service.ProviderServiceFeign;
import com.gm.shading.databases.seata.service.ShareDemoService;
import org.apache.shardingsphere.transaction.annotation.ShardingSphereTransactionType;
import org.apache.shardingsphere.transaction.core.TransactionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ShareDemoServiceImpl implements ShareDemoService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    ShareDemoMapper shareDemoMapper;

    @Autowired
    ProviderServiceFeign providerServiceFeign;

    //https://github.com/seata/seata-samples/tree/master/springcloud-seata-sharding-jdbc-mybatis-plus-samples
    @Transactional
    @ShardingSphereTransactionType(TransactionType.BASE)
    @Override
    public void insert() {
        providerServiceFeign.save("gm----");

        for (int i = 1; i <= 10; i++) {
            ShareDemo shareDemo = new ShareDemo();
            if (i == 5) {
                int b = i / 0;
            }
            shareDemo.setName("测试分表" + i);
            //shareDemoMapper.insert(shareDemo);

            jdbcTemplate.execute("insert into t_share_demo (name) values ('"+"测试分表" + i+"')");
        }
    }
}
