package com.gm.shading.databases.seata;

import com.gm.shading.databases.seata.entity.ShareDemo;
import com.gm.shading.databases.seata.mapper.ShareDemoMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ShadingDatabasesApplicationTests {

    @Autowired
    ShareDemoMapper shareDemoMapper;

    @Test
    void addDemo() {
        for (int i = 1; i <= 10; i++) {
            ShareDemo shareDemo = new ShareDemo();
            shareDemo.setName("测试分表" + i);
            shareDemoMapper.insert(shareDemo);
        }

    }

    @Test
    void addDemo2() {
        for (int i = 1; i <= 10; i++) {
            ShareDemo shareDemo = new ShareDemo();
            shareDemo.setName("测试分表" + i);
            shareDemoMapper.save(shareDemo);
        }
    }
}
