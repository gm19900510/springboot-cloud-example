package com.gm.shading.dynamic.datasource.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.dynamic.datasource.entity.AutoOrderMod;
import org.apache.ibatis.annotations.Mapper;

@DS("master_2")
@Mapper
public interface AutoOrderModMapper extends BaseMapper<AutoOrderMod> {
    void save(AutoOrderMod autoOrder);
}
