package com.gm.shading.dynamic.datasource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.gm.shading.dynamic.datasource.mapper")
public class ShadingDynamicDatasourceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShadingDynamicDatasourceApplication.class, args);
    }
}