package com.gm.shading.dynamic.datasource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.shading.dynamic.datasource.entity.AutoOrderMod;
import com.gm.shading.dynamic.datasource.mapper.AutoOrderModMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

@SpringBootTest
@Slf4j
public class ShadingDynamicDatasourceApplicationTests {

    @Autowired
    AutoOrderModMapper autoOrderModMapper;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    public void testCreateAutoOrderMod() {
        jdbcTemplate.execute("CREATE TABLE `t_auto_order_mod` (\n" +
                " `order_id` bigint(20) NOT NULL COMMENT '订单id',\n" +
                " `price` decimal(10,2) NOT NULL COMMENT '订单价格',\n" +
                " `user_id` bigint(20) NOT NULL COMMENT '下单用户id',\n" +
                " `status` varchar(50) NOT NULL COMMENT '订单状态',\n" +
                " PRIMARY KEY (`order_id`) USING BTREE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;");
    }

    @Test
    public void testInsertAutoOrderMod() {
        Random random = new Random();
        for (int i = 1; i < 10; i++) {
            AutoOrderMod order = new AutoOrderMod();
            order.setPrice(new BigDecimal(i));
            order.setUserId(Integer.valueOf(random.nextInt(25)).longValue());
            order.setStatus(i + "");
            autoOrderModMapper.save(order);
        }
    }

    @Test
    public void testSelectAutoOrderMod() {
        List<AutoOrderMod> list = autoOrderModMapper.selectList(new QueryWrapper<AutoOrderMod>());
        for (AutoOrderMod autoordermod : list) {
            log.info("{}", autoordermod);
        }
    }
}
