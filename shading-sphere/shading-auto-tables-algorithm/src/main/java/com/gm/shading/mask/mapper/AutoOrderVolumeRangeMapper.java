package com.gm.shading.mask.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.mask.entity.AutoOrderVolumeRange;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AutoOrderVolumeRangeMapper extends BaseMapper<AutoOrderVolumeRange> {
    void save(AutoOrderVolumeRange autoOrder);
}
