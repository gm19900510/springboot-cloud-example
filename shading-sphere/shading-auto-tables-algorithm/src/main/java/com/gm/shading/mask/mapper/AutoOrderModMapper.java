package com.gm.shading.mask.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.mask.entity.AutoOrderMod;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AutoOrderModMapper extends BaseMapper<AutoOrderMod> {
    void save(AutoOrderMod autoOrder);
}
