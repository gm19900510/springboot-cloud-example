package com.gm.shading.mask.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.mask.entity.AutoOrderHashMod;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AutoOrderHashModMapper extends BaseMapper<AutoOrderHashMod> {
    void save(AutoOrderHashMod autoOrder);
}
