package com.gm.shading.mask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.math.BigDecimal;

@Data
@TableName("t_auto_order_volume_range")
public class AutoOrderVolumeRange {

    @TableId(type = IdType.ASSIGN_ID)
    private Long orderId;
    private BigDecimal price;
    private Long userId;
    private String status;
}
