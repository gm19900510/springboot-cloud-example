package com.gm.shading.mask.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.mask.entity.AutoOrderBoundaryRange;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AutoOrderBoundaryRangeMapper extends BaseMapper<AutoOrderBoundaryRange> {
    void save(AutoOrderBoundaryRange autoOrder);
}
