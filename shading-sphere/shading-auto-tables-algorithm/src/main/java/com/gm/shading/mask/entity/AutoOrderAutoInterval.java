package com.gm.shading.mask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@TableName("t_auto_order_auto_interval")
public class AutoOrderAutoInterval {

    @TableId(type = IdType.ASSIGN_ID)
    private Long orderId;
    private BigDecimal price;
    private Long userId;
    private String status;
    private LocalDateTime createTime;
}
