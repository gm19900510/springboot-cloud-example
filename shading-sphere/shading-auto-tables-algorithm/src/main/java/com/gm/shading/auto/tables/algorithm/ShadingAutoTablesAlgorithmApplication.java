package com.gm.shading.auto.tables.algorithm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.gm.shading.mask.mapper")
public class ShadingAutoTablesAlgorithmApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShadingAutoTablesAlgorithmApplication.class, args);
    }
}