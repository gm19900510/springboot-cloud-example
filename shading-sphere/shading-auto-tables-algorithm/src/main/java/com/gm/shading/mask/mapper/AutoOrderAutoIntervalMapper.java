package com.gm.shading.mask.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.mask.entity.AutoOrderAutoInterval;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AutoOrderAutoIntervalMapper extends BaseMapper<AutoOrderAutoInterval> {
    void save(AutoOrderAutoInterval autoOrder);
}
