package com.gm.shading.auto.tables.algorithm;

import com.gm.shading.mask.entity.*;
import com.gm.shading.mask.mapper.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Random;

@SpringBootTest
@Slf4j
public class ShadingAutoTablesAlgorithmApplicationTests {


    @Autowired
    AutoOrderModMapper autoOrderModMapper;

    @Autowired
    AutoOrderHashModMapper autoOrderHashModMapper;

    @Autowired
    AutoOrderVolumeRangeMapper autoOrderVolumeRangeMapper;

    @Autowired
    AutoOrderBoundaryRangeMapper autoOrderBoundaryRangeMapper;

    @Autowired
    AutoOrderAutoIntervalMapper autoOrderAutoIntervalMapper;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    public void testCreateAutoOrderMod() {
        jdbcTemplate.execute("CREATE TABLE `t_auto_order_mod` (\n" +
                " `order_id` bigint(20) NOT NULL COMMENT '订单id',\n" +
                " `price` decimal(10,2) NOT NULL COMMENT '订单价格',\n" +
                " `user_id` bigint(20) NOT NULL COMMENT '下单用户id',\n" +
                " `status` varchar(50) NOT NULL COMMENT '订单状态',\n" +
                " PRIMARY KEY (`order_id`) USING BTREE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;");
    }

    @Test
    public void testInsertAutoOrderMod() {
        Random random = new Random();
        for (int i = 1; i < 10; i++) {
            AutoOrderMod order = new AutoOrderMod();
            order.setPrice(new BigDecimal(i));
            order.setUserId(Integer.valueOf(random.nextInt(25)).longValue());
            order.setStatus(i + "");
            autoOrderModMapper.save(order);
        }
    }

    @Test
    public void testCreateAutoOrderHashMod() {
        jdbcTemplate.execute("CREATE TABLE `t_auto_order_hash_mod` (\n" +
                " `order_id` bigint(20) NOT NULL COMMENT '订单id',\n" +
                " `price` decimal(10,2) NOT NULL COMMENT '订单价格',\n" +
                " `user_id` bigint(20) NOT NULL COMMENT '下单用户id',\n" +
                " `status` varchar(50) NOT NULL COMMENT '订单状态',\n" +
                " PRIMARY KEY (`order_id`) USING BTREE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;");
    }

    @Test
    public void testInsertAutoOrderHashMod() {
        Random random = new Random();
        for (int i = 1; i < 10; i++) {
            AutoOrderHashMod order = new AutoOrderHashMod();
            order.setPrice(new BigDecimal(i));
            order.setUserId(Integer.valueOf(random.nextInt(25)).longValue());
            order.setStatus(i + "");
            autoOrderHashModMapper.save(order);
        }
    }

    @Test
    public void testCreateAutoOrderVolumeRange() {
        jdbcTemplate.execute("CREATE TABLE `t_auto_order_volume_range` (\n" +
                " `order_id` bigint(20) NOT NULL COMMENT '订单id',\n" +
                " `price` decimal(10,2) NOT NULL COMMENT '订单价格',\n" +
                " `user_id` bigint(20) NOT NULL COMMENT '下单用户id',\n" +
                " `status` varchar(50) NOT NULL COMMENT '订单状态',\n" +
                " PRIMARY KEY (`order_id`) USING BTREE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;");
    }

    @Test
    public void testInsertAutoOrderVolumeRange() {
        Random random = new Random();
        for (int i = 1; i < 10; i++) {
            AutoOrderVolumeRange order = new AutoOrderVolumeRange();
            order.setPrice(new BigDecimal(random.nextInt(20000)));
            order.setUserId(Integer.valueOf(random.nextInt(25)).longValue());
            order.setStatus(i + "");
            autoOrderVolumeRangeMapper.save(order);
        }
    }

    @Test
    public void testCreateAutoOrderBoundaryRange() {
        jdbcTemplate.execute("CREATE TABLE `t_auto_order_boundary_range` (\n" +
                " `order_id` bigint(20) NOT NULL COMMENT '订单id',\n" +
                " `price` decimal(10,2) NOT NULL COMMENT '订单价格',\n" +
                " `user_id` bigint(20) NOT NULL COMMENT '下单用户id',\n" +
                " `status` varchar(50) NOT NULL COMMENT '订单状态',\n" +
                " PRIMARY KEY (`order_id`) USING BTREE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;");
    }

    @Test
    public void testInsertAutoOrderBoundaryRange() {
        Random random = new Random();
        for (int i = 1; i < 10; i++) {
            AutoOrderBoundaryRange order = new AutoOrderBoundaryRange();
            order.setPrice(new BigDecimal(random.nextInt(20000)));
            order.setUserId(Integer.valueOf(random.nextInt(25)).longValue());
            order.setStatus(i + "");
            autoOrderBoundaryRangeMapper.save(order);
        }
    }

    @Test
    public void testCreateAutoOrderAutoInterval() {
        jdbcTemplate.execute("CREATE TABLE `t_auto_order_auto_interval` (\n" +
                " `order_id` bigint(20) NOT NULL COMMENT '订单id',\n" +
                " `price` decimal(10,2) NOT NULL COMMENT '订单价格',\n" +
                " `user_id` bigint(20) NOT NULL COMMENT '下单用户id',\n" +
                " `status` varchar(50) NOT NULL COMMENT '订单状态',\n" +
                " `create_time` datetime NOT NULL COMMENT '订单时间',\n" +
                " PRIMARY KEY (`order_id`) USING BTREE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;");
    }

    @Test
    public void testInsertAutoOrderAutoInterval() {
        Random random = new Random();
        for (int i = 1; i < 10; i++) {
            AutoOrderAutoInterval order = new AutoOrderAutoInterval();
            order.setPrice(new BigDecimal(random.nextInt(20000)));
            order.setUserId(Integer.valueOf(random.nextInt(25)).longValue());
            order.setStatus(i + "");
            // 随机减少一定天数再随机增加一定天数
            order.setCreateTime(LocalDateTime.now().minusDays(random.nextInt(10)).plusDays(random.nextInt(10)));
            autoOrderAutoIntervalMapper.save(order);
        }
    }
}
