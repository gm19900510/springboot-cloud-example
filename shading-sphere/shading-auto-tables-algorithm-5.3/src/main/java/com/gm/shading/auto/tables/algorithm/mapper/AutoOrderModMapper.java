package com.gm.shading.auto.tables.algorithm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.auto.tables.algorithm.entity.AutoOrderMod;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AutoOrderModMapper extends BaseMapper<AutoOrderMod> {
    void save(AutoOrderMod autoOrder);
}
