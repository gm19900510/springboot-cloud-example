package com.gm.shading.auto.tables.algorithm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.auto.tables.algorithm.entity.AutoOrderVolumeRange;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AutoOrderVolumeRangeMapper extends BaseMapper<AutoOrderVolumeRange> {
    void save(AutoOrderVolumeRange autoOrder);
}
