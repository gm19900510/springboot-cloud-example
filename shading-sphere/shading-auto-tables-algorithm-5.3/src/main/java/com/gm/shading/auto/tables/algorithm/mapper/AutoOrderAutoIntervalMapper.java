package com.gm.shading.auto.tables.algorithm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.auto.tables.algorithm.entity.AutoOrderAutoInterval;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AutoOrderAutoIntervalMapper extends BaseMapper<AutoOrderAutoInterval> {
    void save(AutoOrderAutoInterval autoOrder);
}
