package com.gm.shading.auto.tables.algorithm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.auto.tables.algorithm.entity.AutoOrderBoundaryRange;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AutoOrderBoundaryRangeMapper extends BaseMapper<AutoOrderBoundaryRange> {
    void save(AutoOrderBoundaryRange autoOrder);
}
