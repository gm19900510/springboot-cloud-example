package com.gm.shading.auto.tables.algorithm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.auto.tables.algorithm.entity.AutoOrderHashMod;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AutoOrderHashModMapper extends BaseMapper<AutoOrderHashMod> {
    void save(AutoOrderHashMod autoOrder);
}
