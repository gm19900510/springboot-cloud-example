package com.gm.shading.readwrite;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gm.shading.readwrite.entity.Goods;
import com.gm.shading.readwrite.mapper.GoodsMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;
import java.util.Random;

@SpringBootTest
public class ShadingReadWriteApplicationTests {

    @Autowired
    GoodsMapper goodsMapper;

    // 分布式队列不生效，使用数据库递增
    @Test
    void addGoods() throws InterruptedException {
        for (int i = 1; i <= 10; i++) {
            Goods goods = new Goods();

            Random random = new Random();
            int mainClass = random.nextInt(100) + 1;
            int subClass = random.nextInt(100) + 1;
            goods.setMainClass((long) mainClass);
            goods.setSubClass((long) subClass);
            goods.setGoodsName("商品" + i);
            goodsMapper.insert(goods);
        }

        Thread.sleep(2000);
        for (int i = 1; i <= 10; i++) {
            QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
            List<Goods> list = goodsMapper.selectList(queryWrapper.orderByAsc("goods_id"));
            for (Goods goods : list) {
                System.out.println(goods.toString());
            }
        }
    }

    // 分布式队列生效，使用雪花算法生成ID
    @Test
    void addGoods2() throws InterruptedException {
        for (int i = 1; i <= 10; i++) {
            Goods goods = new Goods();

            Random random = new Random();
            int mainClass = random.nextInt(100) + 1;
            int subClass = random.nextInt(100) + 1;
            goods.setMainClass((long) mainClass);
            goods.setSubClass((long) subClass);
            goods.setGoodsName("商品" + i);
            goodsMapper.save(goods);
        }

        Thread.sleep(2000);
        for (int i = 1; i <= 10; i++) {
            QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
            List<Goods> list = goodsMapper.selectList(queryWrapper.orderByAsc("goods_id"));
            for (Goods goods : list) {
                System.out.println(goods.toString());
            }
        }
    }
}
