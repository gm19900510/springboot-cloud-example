package com.gm.shading.readwrite;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.gm.shading.readwrite.mapper")
public class ShadingReadWriteApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShadingReadWriteApplication.class, args);
    }
}