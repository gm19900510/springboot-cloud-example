package com.gm.shading.databases.seata.service;

import io.seata.core.exception.TransactionException;

public interface ShareDemoService {
    void insert() throws TransactionException;
}
