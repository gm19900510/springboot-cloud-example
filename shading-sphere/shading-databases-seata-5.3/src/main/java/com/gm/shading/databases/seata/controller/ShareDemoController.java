package com.gm.shading.databases.seata.controller;

import com.gm.shading.databases.seata.service.ShareDemoService;
import io.seata.core.exception.TransactionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShareDemoController {

    @Autowired
    ShareDemoService shareDemoService;

    @RequestMapping(value = "save", method = RequestMethod.GET)
    public String save() throws TransactionException {
        shareDemoService.insert();
        return "成功";
    }

}
