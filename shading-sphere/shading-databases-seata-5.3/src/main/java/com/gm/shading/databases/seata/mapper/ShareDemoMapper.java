package com.gm.shading.databases.seata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.databases.seata.entity.ShareDemo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShareDemoMapper extends BaseMapper<ShareDemo> {
    void save(ShareDemo shareDemo);
}
