package com.gm.shading.tables;

import com.gm.shading.tables.entity.ShareDemo;
import com.gm.shading.tables.entity.ShareMonth;
import com.gm.shading.tables.mapper.ShareDemoMapper;
import com.gm.shading.tables.mapper.ShareMonthMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Date;

@SpringBootTest
public class ShadingTablesApplicationTests {

    @Autowired
    ShareDemoMapper shareDemoMapper;

    @Autowired
    ShareMonthMapper shareMonthMapper;

    @Test
    void addDemo() {
        for (int i = 1; i <= 10; i++) {
            ShareDemo shareDemo = new ShareDemo();
            shareDemo.setName("测试分表" + i);
            shareDemoMapper.insert(shareDemo);
        }

    }

    @Test
    void addDemo2() {
        for (int i = 1; i <= 10; i++) {
            ShareDemo shareDemo = new ShareDemo();
            shareDemo.setName("测试分表" + i);
            shareDemoMapper.save(shareDemo);
        }
    }

    @Test
    void addDemo3() {
        for (int i = 1; i <= 10; i++) {
            ShareMonth shareMonth = new ShareMonth();
            shareMonth.setName("测试分表" + i);
            shareMonth.setCreateTime(LocalDateTime.now());
            shareMonthMapper.save(shareMonth);
        }
    }
}
