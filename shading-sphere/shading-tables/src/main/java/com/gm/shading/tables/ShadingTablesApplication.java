package com.gm.shading.tables;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.gm.shading.tables.mapper")
public class ShadingTablesApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShadingTablesApplication.class, args);
    }
}