package com.gm.shading.tables.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.tables.entity.ShareDemo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShareDemoMapper extends BaseMapper<ShareDemo> {
    void save(ShareDemo shareDemo);
}
