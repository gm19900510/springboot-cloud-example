package com.gm.shading.tables.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gm.shading.tables.entity.ShareMonth;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShareMonthMapper extends BaseMapper<ShareMonth> {
    void save(ShareMonth shareMonth);
}
