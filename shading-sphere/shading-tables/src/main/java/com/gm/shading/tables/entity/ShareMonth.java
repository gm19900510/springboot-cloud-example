package com.gm.shading.tables.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("t_share_month")
public class ShareMonth {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    private String name;

    private LocalDateTime createTime;

}
