package com.gm.shading.tables.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("t_share_demo")
public class ShareDemo {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    private String name;

}
