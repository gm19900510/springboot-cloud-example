package com.gm.multi.redis.config.select.annotation;

import java.lang.annotation.*;

/**
 * 注解，用于切换不同数据源的redis
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisSelect {
    /**
     * redis库   0 - 15  库
     *
     * @return
     */
    String instance() default "default";
}
