package com.gm.multi.redis.service.impl;

import com.gm.multi.redis.config.select.RedisSelectSupport;
import com.gm.multi.redis.service.RedisMultiInstanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RedisMultiInstanceServiceImpl implements RedisMultiInstanceService {

    @Autowired
    private StringRedisTemplate redisTemplate;


    public void testMultiInstance(String suffix) {
        String defaultkv = "default-" + suffix + "-instance";
        String twokv = "two-" + suffix + "-instance";
        //使用默认连接
        redisTemplate.opsForValue().set(defaultkv, defaultkv);

        //使用 two 连接
        RedisSelectSupport.selectInstance("two");
        redisTemplate.opsForValue().set(twokv, twokv);

        //使用默认连接
        RedisSelectSupport.selectInstance("default");
        String value1 = String.valueOf(redisTemplate.opsForValue().get(defaultkv));

        //使用 two 连接
        RedisSelectSupport.selectInstance("two");
        String value2 = String.valueOf(redisTemplate.opsForValue().get(twokv));

        log.info("suffix:{}     default={}    two={}", suffix, (defaultkv).equals(value1), (twokv).equals(value2));
    }
}
