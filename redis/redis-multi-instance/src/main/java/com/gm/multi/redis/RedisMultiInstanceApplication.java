package com.gm.multi.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisMultiInstanceApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisMultiInstanceApplication.class, args);
    }
}
