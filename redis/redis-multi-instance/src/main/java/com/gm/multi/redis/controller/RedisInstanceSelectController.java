package com.gm.multi.redis.controller;

import com.gm.multi.redis.config.select.RedisSelectSupport;
import com.gm.multi.redis.config.select.annotation.RedisSelect;
import com.gm.multi.redis.service.RedisMultiInstanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 测试redis切换库
 */
@Slf4j
@RestController
@RequestMapping("redis/instance")
public class RedisInstanceSelectController {

    @Autowired
    private StringRedisTemplate redisTemplate;


    @RequestMapping("/one")
    @RedisSelect()         //选择默认库
    public String selectOne() {
        redisTemplate.opsForValue().set("one", "one_" + System.currentTimeMillis());
        String one = redisTemplate.opsForValue().get("one");
        return one;
    }

    @RequestMapping("/two")
    @RedisSelect(instance = "two")         //选择two库
    public String selectTwo() {
        redisTemplate.opsForValue().set("two", "two_" + System.currentTimeMillis());
        String two = redisTemplate.opsForValue().get("two");
        return two;
    }

    /**
     * 同一个方法中切换不同的redis数据源
     *
     * @return
     */
    @RequestMapping("/three")
    @RedisSelect
    public String selectThree() {
        String one = redisTemplate.opsForValue().get("one");
        log.info(one);
        RedisSelectSupport.selectInstance("two");//此处切换到two库
        String two = redisTemplate.opsForValue().get("two");
        log.info(two);
        return two;
    }


    @Autowired
    RedisMultiInstanceService redisMultiInstanceService;

    @RequestMapping("/testMultiInstance")
    public String testMultiIndex() {
        Thread thread[] = new Thread[500];
        AtomicBoolean result = new AtomicBoolean(true);
        for (int i = 0; i < thread.length; i++) {
            int finalI = i;
            thread[i] = new Thread(() -> {
                try {
                    redisMultiInstanceService.testMultiInstance("Thread-" + finalI);
                } catch (Exception e) {
                    e.printStackTrace();
                    result.set(false);
                }
            });
            thread[i].setName("Thread-" + i);

        }
        for (int i = 0; i < thread.length; i++) {
            thread[i].start();
        }
        return "";
    }
}
