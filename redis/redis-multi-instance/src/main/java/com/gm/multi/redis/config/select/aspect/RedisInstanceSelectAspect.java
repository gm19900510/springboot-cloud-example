package com.gm.multi.redis.config.select.aspect;

import com.gm.multi.redis.config.select.RedisSelectSupport;
import com.gm.multi.redis.config.select.annotation.RedisSelect;
import com.gm.multi.redis.config.select.instance.MultiRedisLettuceConnectionFactory;
import com.gm.multi.redis.config.select.instance.MultiRedisProperties;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;

@Slf4j
@Aspect
@Component
public class RedisInstanceSelectAspect {

    private final String defaultInstance = MultiRedisProperties.DEFAULT;

    /**
     * 创建RedisSelect对应的切面，来对标有注解的方法拦截
     *
     * @param point
     * @return
     * @throws Throwable
     */
    @Around("@annotation(com.gm.multi.redis.config.select.annotation.RedisSelect)")
    @ConditionalOnBean(MultiRedisLettuceConnectionFactory.class)
    public Object configRedis(ProceedingJoinPoint point) throws Throwable {
        String instance = defaultInstance;
        try {
            MethodSignature signature = (MethodSignature) point.getSignature();
            Method method = signature.getMethod();

            RedisSelect config = method.getAnnotation(RedisSelect.class);
            if (config != null) {
                instance = config.instance();
            }
            RedisSelectSupport.selectInstance(instance);
            return point.proceed();
        } finally {
            RedisSelectSupport.selectInstance(defaultInstance);
            log.debug("redis instance reset {} to {}", instance, defaultInstance);
        }
    }
}
