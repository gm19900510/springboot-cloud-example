package com.gm.multi.redis.config.select;

import com.gm.multi.redis.config.select.RedisSelectTemplate;
import com.gm.multi.redis.config.select.instance.MultiRedisLettuceConnectionFactory;
import com.gm.multi.redis.config.select.instance.MultiRedisProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Slf4j
@Configuration(proxyBeanMethods = false)
public class RedisSelectConfig {

    @Autowired(required = false)
    private LettuceConnectionFactory factory;

    @Autowired
    private MultiRedisLettuceConnectionFactory multiRedisLettuceConnectionFactory;

    @Bean
    @ConditionalOnMissingBean
    public RedisSelectTemplate<String, Object> redisIndexSelectTemplate() {

        /**
         * 使用默认注入的RedisConnectionFactory factory时，切换db时出现以下异常：
         *
         * java.lang.UnsupportedOperationException:Selecting a new database not supported due to shared connection.
         * Use separate ConnectionFactorys to work with multiple databases
         * 从默认RedisConnectionFactory factory注入，改为LettuceConnectionFactory factory，
         * 并通过factory.setShareNativeConnection(false)关闭共享链接
         */
        RedisSelectTemplate<String, Object> redisTemplate = new RedisSelectTemplate<>();
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new GenericToStringSerializer(Object.class));
        redisTemplate.setValueSerializer(new StringRedisSerializer());

        if (factory == null) {
            if (multiRedisLettuceConnectionFactory != null) {
                factory = multiRedisLettuceConnectionFactory.getConnectionFactoryMap().get(MultiRedisProperties.DEFAULT);
                redisTemplate.setMultiRedisLettuceConnectionFactory(multiRedisLettuceConnectionFactory);
            }
        }
        // 关闭共享链接
        factory.setShareNativeConnection(false);
        redisTemplate.setConnectionFactory(factory);
        redisTemplate.afterPropertiesSet();
        log.info("实例化 SelectableRedisTemplate 对象完成");
        return redisTemplate;
    }
}
