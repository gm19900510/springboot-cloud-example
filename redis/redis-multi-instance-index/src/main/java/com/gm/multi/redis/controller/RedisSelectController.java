package com.gm.multi.redis.controller;

import com.gm.multi.redis.config.select.RedisSelectSupport;
import com.gm.multi.redis.config.select.RedisSelectTemplate;
import com.gm.multi.redis.config.select.annotation.RedisSelect;
import com.gm.multi.redis.service.RedisMultiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 测试redis切换库
 */
@Slf4j
@RestController
@RequestMapping("redis/")
public class RedisSelectController {

    @Autowired
    private RedisSelectTemplate redisTemplate;

    /**
     * 使用@RedisSelect()需特别注意，默认配置的db index为db 0，
     * 与配置文件中默认数据源的database不一致时，需主动利用@RedisSelect()设置index参数
     * @return
     */
    @RequestMapping("/one")
    @RedisSelect()         //选择默认库，默认db
    public String selectOne() {
        redisTemplate.opsForValue().set("one", "one_" + System.currentTimeMillis());
        String one = (String) redisTemplate.opsForValue().get("one");
        log.info(one);
        return one;
    }

    @RequestMapping("/two")
    @RedisSelect(instance = "two", index = 2)         //选择two库的 db2
    public String selectTwo() {
        redisTemplate.opsForValue().set("two", "two_" + System.currentTimeMillis());
        String two = (String) redisTemplate.opsForValue().get("two");
        log.info(two);
        return two;
    }

    @RequestMapping("/three")
    @RedisSelect
    public String selectThree() {
        String one = (String) redisTemplate.opsForValue().get("one");
        log.info(one);
        redisTemplate.opsForValue().set("two", "two_" + System.currentTimeMillis());
        RedisSelectSupport.select("two", 2);//此处切换到two库
        String two = (String) redisTemplate.opsForValue().get("two");
        log.info(two);
        return two;
    }


    @Autowired
    RedisMultiService redisMultiService;

    @RequestMapping("/testMultiInstance")
    public String testMultiIndex() {
        Thread thread[] = new Thread[500];
        AtomicBoolean result = new AtomicBoolean(true);
        for (int i = 0; i < thread.length; i++) {
            int finalI = i;
            thread[i] = new Thread(() -> {
                try {
                    redisMultiService.testMultiInstanceAndIndex("Thread-" + finalI);
                } catch (Exception e) {
                    e.printStackTrace();
                    result.set(false);
                }
            });
            thread[i].setName("Thread-" + i);

        }
        for (int i = 0; i < thread.length; i++) {
            thread[i].start();
        }
        return "";
    }
}
