package com.gm.multi.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisMultiInstanceAndIndexApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisMultiInstanceAndIndexApplication.class, args);
    }
}
