package com.gm.multi.redis.config.select;

import com.gm.multi.redis.config.select.instance.MultiRedisProperties;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SelectAttribute {
    private String instance = MultiRedisProperties.DEFAULT;
    private int index;
}
