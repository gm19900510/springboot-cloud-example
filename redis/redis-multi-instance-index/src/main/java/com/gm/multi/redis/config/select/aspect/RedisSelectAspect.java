package com.gm.multi.redis.config.select.aspect;

import com.gm.multi.redis.config.select.RedisSelectSupport;
import com.gm.multi.redis.config.select.annotation.RedisSelect;
import com.gm.multi.redis.config.select.instance.MultiRedisLettuceConnectionFactory;
import com.gm.multi.redis.config.select.instance.MultiRedisProperties;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;

@Slf4j
@Aspect
@Component
public class RedisSelectAspect {

    @Autowired(required = false)
    MultiRedisProperties multiRedisProperties;

    private final String defaultInstance = MultiRedisProperties.DEFAULT;

    private final int defaultIndex = 0;

    /**
     * 创建RedisSelect对应的切面，来对标有注解的方法拦截
     *
     * @param point
     * @return
     * @throws Throwable
     */
    @Around("@annotation(com.gm.multi.redis.config.select.annotation.RedisSelect)")
    @ConditionalOnBean(MultiRedisLettuceConnectionFactory.class)
    public Object configRedis(ProceedingJoinPoint point) throws Throwable {
        String instance = defaultInstance;
        int index = defaultIndex;
        try {
            MethodSignature signature = (MethodSignature) point.getSignature();
            Method method = signature.getMethod();

            RedisSelect config = method.getAnnotation(RedisSelect.class);
            if (config != null) {
                instance = config.instance();
                index = config.index();
                if (multiRedisProperties != null) {
                    Map<String, RedisProperties> multi = multiRedisProperties.getMulti();
                    RedisProperties properties = multi.get(instance);
                    if (properties == null) {
                        throw new Exception("配置文件中未配置动态切换的数据源：" + instance);
                    }
                }
            }
            RedisSelectSupport.select(instance, index);
            log.debug("redis select instance：{}，index： {}", instance, index);
            return point.proceed();
        } finally {
            RedisSelectSupport.select(defaultInstance, defaultIndex);
            log.debug("redis instance reset {} to {} , index reset {} to {}", instance, defaultInstance, index, defaultIndex);
        }
    }
}
