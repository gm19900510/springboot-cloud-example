package com.gm.multi.redis.config.select;

import com.gm.multi.redis.config.select.instance.MultiRedisProperties;

/**
 * Redis 切换不同redis数据源操作
 */
public class RedisSelectSupport {

    /**
     * 定义一个静态变量，用于线程间传递值
     */
    private static final ThreadLocal<SelectAttribute> SELECT_CONTEXT = new ThreadLocal<>();

    public static void select(String instance, int index) {
        SELECT_CONTEXT.set(new SelectAttribute(instance, index));
    }

    public static SelectAttribute getSelect() {
        return SELECT_CONTEXT.get();
    }
}
