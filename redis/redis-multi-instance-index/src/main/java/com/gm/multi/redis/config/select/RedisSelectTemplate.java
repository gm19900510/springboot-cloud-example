package com.gm.multi.redis.config.select;

import com.gm.multi.redis.config.select.instance.MultiRedisLettuceConnectionFactory;
import com.gm.multi.redis.config.select.instance.MultiRedisProperties;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;

public class RedisSelectTemplate<K, V> extends RedisTemplate<K, V> {

    private MultiRedisLettuceConnectionFactory multiRedisLettuceConnectionFactory;

    public void setMultiRedisLettuceConnectionFactory(MultiRedisLettuceConnectionFactory multiRedisLettuceConnectionFactory) {
        this.multiRedisLettuceConnectionFactory = multiRedisLettuceConnectionFactory;
    }

    @Override
    public RedisConnectionFactory getConnectionFactory() {
        String instance = MultiRedisProperties.DEFAULT;
        SelectAttribute selectAttribute = RedisSelectSupport.getSelect();
        if (!StringUtils.isEmpty(selectAttribute)) {
            instance = selectAttribute.getInstance();
        }
        LettuceConnectionFactory factory = multiRedisLettuceConnectionFactory.getConnectionFactoryMap().get(instance);
        factory.setShareNativeConnection(false);
        return factory;
    }

    @Override
    protected RedisConnection createRedisConnectionProxy(RedisConnection pm) {
        return super.createRedisConnectionProxy(pm);
    }

    /**
     * 在连接Redis之前做一些配置
     *
     * @param connection
     * @param existingConnection
     * @return
     */
    @Override
    protected RedisConnection preProcessConnection(RedisConnection connection, boolean existingConnection) {
        SelectAttribute selectAttribute = RedisSelectSupport.getSelect();
        if (!StringUtils.isEmpty(selectAttribute)) {
            Integer index = selectAttribute.getIndex();
            //切换 redis db 到 其他的库
            connection.select(index);
        }
        return super.preProcessConnection(connection, existingConnection);
    }
}

