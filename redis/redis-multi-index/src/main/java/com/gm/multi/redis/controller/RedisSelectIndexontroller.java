package com.gm.multi.redis.controller;

import com.gm.multi.redis.config.select.RedisSelectSupport;
import com.gm.multi.redis.config.select.RedisSelectTemplate;
import com.gm.multi.redis.config.select.annotation.RedisSelect;
import com.gm.multi.redis.service.RedisMultiIndexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 测试redis切换db库
 */
@Slf4j
@RestController
@RequestMapping("redis/index")
public class RedisSelectIndexontroller {
    @Autowired
    private RedisSelectTemplate<String, Object> redisTemplate;

    @RequestMapping("/one")
    @RedisSelect(index = 1)         //选择db1库
    public String selectOne() {
        redisTemplate.opsForValue().set("one", "one_" + System.currentTimeMillis());
        String one = (String) redisTemplate.opsForValue().get("one");
        return one;
    }

    @RequestMapping("/two")
    @RedisSelect(index = 2)         //选择db2库
    public String selectTwo() {
        redisTemplate.opsForValue().set("two", "two_" + System.currentTimeMillis());
        String two = (String) redisTemplate.opsForValue().get("two");
        return two;
    }

    /**
     * 同一个方法中切换不同的redis库
     *
     * @return
     */
    @RequestMapping("/three")
    @RedisSelect(index = 2)         //选择db2库
    public String selectThree() {
        redisTemplate.opsForValue().set("two", "two_" + System.currentTimeMillis());
        String two = (String) redisTemplate.opsForValue().get("two");
        log.info(two);
        RedisSelectSupport.selectIndex(3);//此处切换到db3库
        redisTemplate.opsForValue().set("three", "three_" + System.currentTimeMillis());
        String three = (String) redisTemplate.opsForValue().get("three");
        log.info(three);
        return three;
    }

    @Autowired
    RedisMultiIndexService redisMultiIndexService;

    @RequestMapping("/testMultiIndex")
    public String testMultiIndex() {
        Thread thread[] = new Thread[500];
        AtomicBoolean result = new AtomicBoolean(true);
        for (int i = 0; i < thread.length; i++) {
            int finalI = i;
            thread[i] = new Thread(() -> {
                try {
                    redisMultiIndexService.testMultiIndex("Thread-" + finalI);
                } catch (Exception e) {
                    e.printStackTrace();
                    result.set(false);
                }
            });
            thread[i].setName("Thread-" + i);

        }
        for (int i = 0; i < thread.length; i++) {
            thread[i].start();
        }
        return "";
    }

}
