package com.gm.multi.redis.config.select;

/**
 * Redis 切换DB操作
 */
public class RedisSelectSupport {

    /**
     * 定义一个静态变量，用于线程间传递值
     */
    private static final ThreadLocal<Integer> DB_SELECT_CONTEXT = new ThreadLocal<>();

    public static void selectIndex(int db) {
        DB_SELECT_CONTEXT.set(db);
    }

    public static Integer getSelectIndex() {
        return DB_SELECT_CONTEXT.get();
    }

}
