package com.gm.multi.redis.config.select;

import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;

public class RedisSelectTemplate<K, V> extends RedisTemplate<K, V> {

    @Override
    protected RedisConnection createRedisConnectionProxy(RedisConnection pm) {
        return super.createRedisConnectionProxy(pm);
    }

    /**
     * 在连接Redis之前做一些配置
     *
     * @param connection
     * @param existingConnection
     * @return
     */
    @Override
    protected RedisConnection preProcessConnection(RedisConnection connection, boolean existingConnection) {
        Integer index = RedisSelectSupport.getSelectIndex();
        if (index != null) {
            //切换 redis db 到 其他的库
            connection.select(index);
        }
        return super.preProcessConnection(connection, existingConnection);
    }
}

