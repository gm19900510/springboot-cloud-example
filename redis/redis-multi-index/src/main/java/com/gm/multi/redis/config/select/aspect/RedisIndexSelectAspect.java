package com.gm.multi.redis.config.select.aspect;

import com.gm.multi.redis.config.select.RedisSelectSupport;
import com.gm.multi.redis.config.select.annotation.RedisSelect;
import com.gm.multi.redis.config.select.RedisSelectTemplate;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;

@Slf4j
@Aspect
@Component
public class RedisIndexSelectAspect {

    @Value("${spring.redis.database:0}")
    private int defaultIndex;

    /**
     * 创建RedisSelect对应的切面，来对标有注解的方法拦截
     *
     * @param point
     * @return
     * @throws Throwable
     */
    @Around("@annotation(com.gm.multi.redis.config.select.annotation.RedisSelect)")
    @ConditionalOnBean(RedisSelectTemplate.class)
    public Object configRedis(ProceedingJoinPoint point) throws Throwable {
        int index = defaultIndex;
        try {
            MethodSignature signature = (MethodSignature) point.getSignature();
            Method method = signature.getMethod();
            RedisSelect config = method.getAnnotation(RedisSelect.class);
            if (config != null) {
                index = config.index();
            }
            RedisSelectSupport.selectIndex(index);
            return point.proceed();
        } finally {
            RedisSelectSupport.selectIndex(defaultIndex);
            log.debug("redis index reset {} to {}", index, defaultIndex);
        }
    }
}
