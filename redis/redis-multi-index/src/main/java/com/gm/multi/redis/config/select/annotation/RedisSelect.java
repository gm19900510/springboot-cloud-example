package com.gm.multi.redis.config.select.annotation;

import java.lang.annotation.*;

/**
 * 注解，用于切换同一redis数据源下的不同db index
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisSelect {
    /**
     * redis库   0 - 15  库
     *
     * @return
     */
    int index() default 0;
}
