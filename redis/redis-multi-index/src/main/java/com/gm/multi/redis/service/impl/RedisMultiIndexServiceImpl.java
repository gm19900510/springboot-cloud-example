package com.gm.multi.redis.service.impl;

import com.gm.multi.redis.config.select.RedisSelectSupport;
import com.gm.multi.redis.config.select.RedisSelectTemplate;
import com.gm.multi.redis.service.RedisMultiIndexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RedisMultiIndexServiceImpl implements RedisMultiIndexService {

    @Autowired
    private RedisSelectTemplate<String, Object> redisSelectTemplate;

    public void testMultiIndex(String suffix) {
        String defaultkv = "default-" + suffix + "-index";
        String twokv = "two-" + suffix + "-index";
        String threekv = "three-" + suffix + "-index";
        String fourkv = "two-" + suffix + "-index";
        //使用认连接
        RedisSelectSupport.selectIndex(10);
        redisSelectTemplate.opsForValue().set(defaultkv, defaultkv);

        //使用 two 连接
        RedisSelectSupport.selectIndex(11);
        redisSelectTemplate.opsForValue().set(twokv, twokv);

        //使用 three 连接
        RedisSelectSupport.selectIndex(12);
        redisSelectTemplate.opsForValue().set(threekv, threekv);

        //使用 four 连接
        RedisSelectSupport.selectIndex(13);
        redisSelectTemplate.opsForValue().set(fourkv, fourkv);

        //使用默认连接
        RedisSelectSupport.selectIndex(10);
        String value1 = String.valueOf(redisSelectTemplate.opsForValue().get(defaultkv));

        //使用 two 连接
        RedisSelectSupport.selectIndex(11);
        String value2 = String.valueOf(redisSelectTemplate.opsForValue().get(twokv));

        //使用 three 连接
        RedisSelectSupport.selectIndex(12);
        String value3 = String.valueOf(redisSelectTemplate.opsForValue().get(threekv));

        //使用 four 连接
        RedisSelectSupport.selectIndex(13);
        String value4 = String.valueOf(redisSelectTemplate.opsForValue().get(fourkv));

        log.info("suffix:{}     default={}    two={}   three={}    four={}", suffix, (defaultkv).equals(value1),
                (twokv).equals(value2), (threekv).equals(value3), (fourkv).equals(value4));
    }
}
