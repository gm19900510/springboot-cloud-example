package com.gm.lock.controller;

import com.gm.lock.config.ConcurrentLockUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@RestController
@Slf4j
public class LockController {


    @Autowired
    RedisTemplate/*<String, Object>*/ redisTemplate;

    @Autowired
    ConcurrentLockUtil concurrentLockUtil;

    @RequestMapping(value = "test", method = RequestMethod.GET)
    public String test1() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        String key = "test-lock";

        Callable<String> callable = () -> {
            String result = "";
            String threadId = Thread.currentThread().getId() + "";
            if (Boolean.TRUE.equals(redisTemplate.opsForValue().setIfAbsent(key, threadId, 10, TimeUnit.SECONDS))) {
                result = threadId;
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (redisTemplate.opsForValue().get(key).equals(threadId)) {
                        redisTemplate.delete(key);
                    }
                }
            }
            return result;
        };
        List<Future<String>> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Future<String> future = executorService.submit(callable);
            list.add(future);
        }
        for (Future<String> future : list) {
            log.info(future.get());
        }

        return "请求成功";
    }


    @RequestMapping(value = "test2", method = RequestMethod.GET)
    public String test2() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        String key = "test2-lock";

        Callable<String> callable = () -> {
            String result = "";
            String threadId = Thread.currentThread().getId() + "";
            if (Boolean.TRUE.equals(concurrentLockUtil.lock(key, threadId, 10))) {
                result = threadId;
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    concurrentLockUtil.unlock(key, threadId);
                }
            }
            return result;
        };
        List<Future<String>> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Future<String> future = executorService.submit(callable);
            list.add(future);
        }
        for (Future<String> future : list) {
            log.info(future.get());
        }

        return "请求成功";
    }
}
