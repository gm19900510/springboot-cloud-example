package com.gm.lock.config;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import java.util.Collections;
import java.util.List;

@Component
public class ConcurrentLockUtil {

    private static final String LOCK_LUA = "if redis.call('setnx', KEYS[1], ARGV[1]) == 1 then redis.call('expire', KEYS[1], ARGV[2]) return 'true' else return 'false' end";
    private static final String UNLOCK_LUA = "if redis.call('get', KEYS[1]) == ARGV[1] then redis.call('del', KEYS[1]) end return 'true' ";

    private final RedisScript lockRedisScript;
    private final RedisScript unLockRedisScript;
    private final RedisSerializer<String> argsSerializer;
    private final RedisSerializer<String> resultSerializer;
    private RedisTemplate redisTemplate;


    public ConcurrentLockUtil(RedisTemplate redisTemplate) {

        this.argsSerializer = new StringRedisSerializer();
        this.resultSerializer = new StringRedisSerializer();
        this.lockRedisScript = RedisScript.of(LOCK_LUA, String.class);
        this.unLockRedisScript = RedisScript.of(UNLOCK_LUA, String.class);
        this.redisTemplate = redisTemplate;
    }

    /**
     * 分布式锁
     *
     * @param lockKey
     * @param value
     * @param time
     * @return
     */
    public boolean lock(String lockKey, String value, long time) {
        List<String> keys = Collections.singletonList(lockKey);
        String flag = (String) redisTemplate.execute(lockRedisScript, argsSerializer, resultSerializer, keys, value, String.valueOf(time));
        return Boolean.valueOf(flag);
    }

    /**
     * 删除锁
     *
     * @param lock
     * @param val
     */
    public void unlock(String lock, String val) {
        List<String> keys = Collections.singletonList(lock);
        redisTemplate.execute(unLockRedisScript, argsSerializer, resultSerializer, keys, val);
    }
}
