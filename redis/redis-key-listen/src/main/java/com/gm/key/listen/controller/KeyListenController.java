package com.gm.key.listen.controller;

import com.gm.key.listen.config.LocalCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.Subscription;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("redis")
public class KeyListenController {

    @Autowired
    RedisTemplate<String, Object> redisTemplate;
    @Autowired
    LocalCache localCache;

    @RequestMapping("/test")
    public String test(@RequestParam String key, @RequestParam String value, @RequestParam int timeout) {
        redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
        String setValue = (String) redisTemplate.opsForValue().get(key);
        return setValue;
    }

    @RequestMapping("/test2")
    public String test2(@RequestParam String key, @RequestParam String hashKey, @RequestParam String hashValue) {
        redisTemplate.opsForHash().put(key, hashKey, hashValue);
        String setValue = (String) redisTemplate.opsForHash().get(key, hashKey);
        return setValue;
    }

    @RequestMapping("/test3")
    public String test3(@RequestParam String key, @RequestParam String value) throws Exception {
        localCache.put(key, Optional.ofNullable(value));
        return localCache.get(key).get();
    }

    @RequestMapping("/test4")
    public String test4(@RequestParam String key) throws Exception {
        return localCache.get(key).get();
    }

    @RequestMapping("/test5")
    public String test5(@RequestParam String key, @RequestParam String value) {
        redisTemplate.opsForValue().set("status:" + key, value);
        String setValue = (String) redisTemplate.opsForValue().get("status:" + key);
        return setValue;
    }

    @Autowired
    RedisMessageListenerContainer container;

    final Map<String, RedisConnection> map = new HashMap<>();

    @RequestMapping("/dosub")
    public String doSubscribe(String channel) {
        RedisConnection connection = container.getConnectionFactory().getConnection();
        map.put(channel, connection);
        connection.subscribe(new MessageListener() {
            @Override
            public void onMessage(Message message, byte[] pattern) {
                log.info("接收到消息");
                System.out.println(new String(message.getBody()));
            }
        }, channel.getBytes(StandardCharsets.UTF_8));
        return "订阅" + channel;
    }

    @RequestMapping("/dopsub")
    public String dopSubscribe(String channel) {
        RedisConnection connection = container.getConnectionFactory().getConnection();
        map.put(channel, connection);
        connection.pSubscribe(new MessageListener() {
            @Override
            public void onMessage(Message message, byte[] pattern) {
                log.info("接收到消息：{}",new String(message.getBody()));
                RedisSerializer<?> serializer = redisTemplate.getKeySerializer();
                String channel = String.valueOf(serializer.deserialize(message.getChannel()));
                log.info("接收到channel：{}",channel);
            }
        }, channel.getBytes(StandardCharsets.UTF_8));
        return "订阅" + channel;
    }

    @RequestMapping("/getsub")
    public Subscription getSubscription(String channel) {
        RedisConnection connection = map.get(channel);

        Subscription subscription = connection.getSubscription();
        log.info("是否存在订阅状态：{}", subscription);
        return subscription;
    }

    @RequestMapping("/dounsub")
    public String doUnSubscribe(String channel) {
        RedisConnection connection = map.get(channel);
        if (connection.isSubscribed()) {
            Subscription subscription = connection.getSubscription();
            log.info("是否存在订阅状态：{}", subscription);
            subscription.subscribe(channel.getBytes(StandardCharsets.UTF_8));

        }
        return "取消订阅" + channel;
    }
}
