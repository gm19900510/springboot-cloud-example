package com.gm.key.listen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisKeyListenApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisKeyListenApplication.class, args);
    }
}
