package com.gm.key.listen.config;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class LocalCache {

    private final LoadingCache<String, Optional<String>> cache;


    public LocalCache() {
        /**
         * 创建本地缓存，当本地缓存不命中时，调用load方法，返回结果，再缓存结果, 指定秒自动过期
         */
        cache = CacheBuilder.newBuilder().refreshAfterWrite(10, TimeUnit.SECONDS).removalListener(notification -> { // 设置过期监听
            log.info("remove key[" + notification.getKey() + "],value[" + notification.getValue() + "],remove reason[" + notification.getCause() + "]");
        }).build(new CacheLoader<String, Optional<String>>() {
            public Optional<String> load(String key) throws Exception {
                log.info("加载key[{}]数据", key);
                return Optional.ofNullable("");
            }
        });
    }

    public Optional<String> get(String key) throws Exception {
        return cache.get(key);
    }

    public void remove(String key) {
        cache.invalidate(key);
    }

    public void put(String key, Optional<String> value) {
        cache.put(key, value);
    }
}
