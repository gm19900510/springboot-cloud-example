package com.gm.key.listen.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.Subscription;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.KeyspaceEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RedisKeyAllEventListener extends KeyspaceEventMessageListener {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public RedisKeyAllEventListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
        super.setKeyspaceNotificationsConfigParameter("KEA");
    }

    @Override
    protected void doHandleMessage(Message message) {
        String key = message.toString();
        RedisSerializer<?> serializer = this.redisTemplate.getKeySerializer();
        String channel = String.valueOf(serializer.deserialize(message.getChannel()));

        log.info("redis key: {} , channel: {}", key, channel);
    }
}
