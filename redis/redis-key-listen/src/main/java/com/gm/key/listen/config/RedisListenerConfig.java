package com.gm.key.listen.config;

import com.gm.key.listen.listener.RedisListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.Topic;

@Configuration
public class RedisListenerConfig {
    @Autowired
    RedisListener redisListener;
    private static final Topic TOPIC_EXPIRED_KEYEVENTS = new PatternTopic("__keyevent@*__:expired");
    private static final Topic TOPIC_ALL_KEYEVENTS = new PatternTopic("__keyevent@1__:set");

    private static final Topic TOPIC_ALL_KEYSPACE = new PatternTopic("__keyspace@*__:*");


    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory factory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(factory);
        container.addMessageListener(redisListener, TOPIC_ALL_KEYSPACE);
        return container;
    }
}
